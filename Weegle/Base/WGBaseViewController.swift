//
//  WGBaseViewController.swift
//  Weegle
//
//  Created by 이인재 on 04/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import Toaster

class WGBaseViewController: MPBaseViewController {
	
	override var confirmTitle: String {
		return "confirm".localized
	}
	
	func toast(message: String, duration: TimeInterval, delay: TimeInterval = 0, bottomOffsetPortrait: CGFloat = 30) {
		let toast = Toast(text: message, delay: delay, duration: duration)
		toast.view.font = UIFont.systemFont(ofSize: 15)
		toast.view.bottomOffsetPortrait = bottomOffsetPortrait
		toast.show()
	}
}
