//
//  WGTabBarController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import CleanroomLogger

class WGTabBarController: UITabBarController {

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)

		NotificationCenter.default.addObserver(self, selector: #selector(openChat(notification:)), name: WGChatService.openChatNotification.name, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(enterChat(notification:)), name: WGChatService.enterChatNotification.name, object: nil)
	}
	
	@objc func openChat(notification: Notification) {
		guard let friendIds = notification.userInfo?["id"] as? [String] else { return }
		
		selectedIndex = 1
		
		WGChatService.open(friendIds: friendIds) { (chatId) in
			if let naviController = self.viewControllers?[1] as? UINavigationController {
				if let viewController = naviController.viewControllers.first as? WGChatListViewController {
					naviController.popToRootViewController(animated: false)
					
					viewController.enter(chatId: chatId)
				}
			}
		}
	}
	
	@objc func enterChat(notification: Notification) {
		guard let chatId = notification.userInfo?["chatId"] as? String else { return }
		guard WGChatService.isOpen(chatId: chatId) == false else { return }
		
		selectedIndex = 1
		
		if let naviController = viewControllers?[1] as? UINavigationController {
			if let viewController = naviController.viewControllers.first as? WGChatListViewController {
				naviController.popToRootViewController(animated: false)

				viewController.enter(chatId: chatId)
			}
		}
	}
}
