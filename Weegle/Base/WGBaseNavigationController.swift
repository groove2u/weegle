//
//  WGBaseNavigationController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.11.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGBaseNavigationController: UINavigationController {

	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		navigationBar.isTranslucent = false
		navigationBar.backgroundColor = WGColor.baseColor(for: 9)
		navigationBar.barTintColor = WGColor.baseColor(for: 9)
		navigationBar.tintColor = .white
		navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 17), NSAttributedStringKey.foregroundColor: UIColor.white]
	}
}
