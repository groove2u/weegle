//
//  WGFriendListEditViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.16.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import RealmSwift
import PKHUD
import CleanroomLogger

class WGFriendListEditViewController: WGBaseViewController {

	@IBOutlet weak private var tableView: UITableView!
	@IBOutlet weak private var noFriendNoticeView: UIView!
	@IBOutlet weak private var searchBar: UISearchBar!
	@IBOutlet weak private var blockButton: WGRedButton!
	@IBOutlet weak private var hideButton: WGMainSmallTitleButton!
	
	fileprivate var models: Results<WGUser>?
	
	private var notificationToken: NotificationToken?
	
	override func prepareUI() {
		super.prepareUI()
		
		models = WGUserService.current?.friends?.sorted(byKeyPath: "memberName")
		notificationToken = models?.addNotificationBlock { [weak self] (_) in
			self?.updateDataUI()
		}
		
		tableView.tableFooterView = UIView(frame: .zero)
		
		NotificationCenter.default.addObserver(self, selector: #selector(fetch), name: WGFriendService.addedNotification.name, object: nil)
		
		if WGUserService.isLogin {
			fetch()
		}
		
		updateStatus()
	}
	
	override func updateUI() {
		super.updateUI()
	}
	
	deinit {
		notificationToken?.stop()
	}
	
	@objc override func fetch() {
		WGFriendService.update()
	}
	
	override func updateDataUI() {
		tableView.reloadData()
	}
	
	// Action
	
	@IBAction func updateRelation(sender: UIButton) {
		guard let models = models else { return }
		guard let indexPaths = tableView.indexPathsForSelectedRows, indexPaths.isEmpty == false else { return }
		
		let type = sender == blockButton ? Relation.block : Relation.hidden
		let message = type == .block ? "C2".localized : "C3".localized
		var objectIdx = [String]()
		
		indexPaths.forEach { (indexPath) in
			let model: WGUser
			
			if let text = searchBar.text, text.isEmpty == false {
				model = models.filter("memberName CONTAINS %@", text)[indexPath.item]
			} else {
				model = models[indexPath.item]
			}
			
			objectIdx.append(model.objectId)
		}
		
		let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(title: "confirm".localized, style: .destructive, handler: { (_) in
			WGFriendService.updateRelation(to: type, objectIdx: objectIdx, success: {
				self.navigationController?.popViewController(animated: true)
			}) { (error) in
				Log.error?.value(error)
			}
		}))
		alert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: nil))
		
		present(alert, animated: true)
	}
}

extension WGFriendListEditViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let models = models else { noFriendNoticeView.isHidden = false; return 0 }
		
		let result: Int
		
		if let text = searchBar.text, text.isEmpty == false {
			result = models.filter("memberName CONTAINS %@", text).count
		} else {
			result = models.count
		}
		
		noFriendNoticeView.isHidden = result > 0
		
		return result
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let models = models else { return UITableViewCell() }
		guard let cell = tableView.dequeueReusableCell(withIdentifier: WGFriendTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGFriendTableViewCell,
			indexPath.item < models.count else {
				return UITableViewCell()
		}
		
		let model: WGUser
		
		if let text = searchBar.text, text.isEmpty == false {
			model = models.filter("memberName CONTAINS %@", text)[indexPath.item]
		} else {
			model = models[indexPath.item]
		}
		
		cell.selectButton?.isSelected = cell.isSelected
		cell.nameLabel.text = model.memberName
		cell.profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: model.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
		cell.promotionLabel?.text = model.profileComment
		cell.promotionView?.alpha = model.profileComment.isEmpty ? 0 : 1
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		updateStatus()
	}
	
	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		updateStatus()
	}
	
	private func updateStatus() {
		blockButton.isEnabled = tableView.indexPathsForSelectedRows?.isEmpty == false
		hideButton.isEnabled = tableView.indexPathsForSelectedRows?.isEmpty == false
	}
}

extension WGFriendListEditViewController: UISearchBarDelegate {
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		updateDataUI()
	}
}
