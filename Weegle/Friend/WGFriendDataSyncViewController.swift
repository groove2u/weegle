//
//  WGFriendDataSyncViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.17.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGFriendDataSyncViewController: WGBaseViewController {

	@IBOutlet weak private var progressWidthRatioContraint: NSLayoutConstraint!
	
	// MARK: - Action
	
	@IBAction func dismiss() {
		dismiss(animated: false, completion: nil)
	}
}
