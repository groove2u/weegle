//
//  WGFriendTableViewCell.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGFriendTableViewCell: UITableViewCell {

	@IBOutlet weak var profileImageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var promotionLabel: UILabel?
	@IBOutlet weak var selectButton: UIButton?
	@IBOutlet weak var promotionView: UIView!
	@IBOutlet weak var addButton: MPBaseButton!
	
	weak var delegate: ListItemInteractionProtocol?
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		
		selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
		
		selectButton?.isSelected = selected
    }
	
	override func setHighlighted(_ highlighted: Bool, animated: Bool) {
		super.setHighlighted(highlighted, animated: animated)
		
		backgroundColor = .white
	}

	// MARK: - Action
	
	@IBAction func addFriend() {
		delegate?.listItem(cell: self, didTouched: .add)
	}
	
	@IBAction func detail() {
		delegate?.listItem(cell: self, didTouched: .detail)
	}
}
