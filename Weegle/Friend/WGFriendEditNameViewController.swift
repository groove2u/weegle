//
//  WGFriendEditNameViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.16.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGFriendEditNameViewController: WGBaseViewController {

	@IBOutlet weak private var userNameTextField: UITextField!
	
	var userID: String! {
		didSet {
//			user = WGUser(id: userID)
		}
	}
	
	private var user: WGUser!
	
	override func prepareUI() {
		super.prepareUI()
		
		navigationController?.setNavigationBarHidden(false, animated: true)
	}
	
	override func updateUI() {
		super.updateUI()
		
		userNameTextField.becomeFirstResponder()
	}
}
