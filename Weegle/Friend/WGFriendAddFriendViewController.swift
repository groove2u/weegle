//
//  WGFriendAddFriendViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGFriendAddFriendViewController: WGBaseViewController {
	
	override func updateUI() {
		super.updateUI()
		
		navigationController?.setNavigationBarHidden(true, animated: true)
	}
}
