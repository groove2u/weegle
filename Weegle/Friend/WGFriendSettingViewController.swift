//
//  WGFriendSettingViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.16.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import PKHUD

class WGFriendSettingViewController: UITableViewController {
	
	@IBOutlet weak private var syncSwitch: UISwitch!
	@IBOutlet weak private var lastSyncDateLabel: UILabel!
	
	private let formatter: DateFormatter = {
		let fmt = DateFormatter()
		fmt.dateFormat = "최종 추가시간 M월 d일 a h:m"
		
		return fmt
	}()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if let current = WGUserService.current {
			syncSwitch.isOn = current.isSyncEnabled
			
			if let date = current.lastSyncDate {
				lastSyncDateLabel.text = formatter.string(from: date)
			} else {
				lastSyncDateLabel.text = "-"
			}
		}
	}
	
	// MARK: - Action
	
	@IBAction func toggleSync(_ sender: UISwitch) {
		WGUserService.current?.isSyncEnabled = sender.isOn
		
		if sender.isOn {
			sync()
		}
	}
	
	@IBAction func sync() {
		HUD.flash(.progress)
		
		WGFriendService.sync(success: {
			HUD.hide()
		}) {
			HUD.hide()
		}
	}
	
	@IBAction func dismiss(_ sender: Any) {
		navigationController?.popViewController(animated: true)
	}
}
