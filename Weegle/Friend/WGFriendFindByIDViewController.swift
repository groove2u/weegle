//
//  WGFriendFindByIDViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.16.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import Kingfisher
import CleanroomLogger

class WGFriendFindByIDViewController: WGBaseViewController {

	@IBOutlet weak private var noticeLabel: UILabel!
	@IBOutlet weak private var noResultView: UIView!
	@IBOutlet weak private var idTextField: WGSearchField!
	@IBOutlet weak private var tableView: UITableView!
	
	fileprivate var models: [WGUser] = []
	
	override func prepareUI() {
		super.prepareUI()
		
		navigationController?.setNavigationBarHidden(false, animated: true)
		
		idTextField.becomeFirstResponder()
	}
	
	// MARK: - TextField
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if let id = idTextField.text, id.isEmpty == false {
			noticeLabel.isHidden = true
			
			WGFriendService.search(memberId: id, success: { [weak self] (user) in
				if let user = user {
					self?.models = [user]
				
					if self?.models.isEmpty == true {
						self?.noResultView.isHidden = false
					} else {
						self?.noResultView.isHidden = true
					}

					self?.tableView.reloadData()
				}
			}, failure: { (_) in
				self.noResultView.isHidden = false
				self.tableView.reloadData()
			})
		}
		
		return true
	}
}

extension WGFriendFindByIDViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//		noResultView.isHidden = models.isEmpty == false
		tableView.isHidden = models.isEmpty
		
		return models.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: WGFriendTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGFriendTableViewCell else {
			return UITableViewCell()
		}
		
		let user = models[indexPath.item]
		
		cell.profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: user.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
		cell.nameLabel.text = user.memberName
		cell.delegate = self
		
		return cell
	}
}

extension WGFriendFindByIDViewController: ListItemInteractionProtocol {
	
	func listItem(cell: UITableViewCell, didTouched: ListItemInteractionEvent) {
		guard let indexPath = tableView.indexPath(for: cell) else { return }
		
		let user = models[indexPath.item]
		
		WGFriendService.add(objectId: user.objectId, success: {
			self.alert(title: nil, message: "T19", confirmTitle: "confirm", confirm: { (_) in
				self.performSegue(withIdentifier: "DismissSegue", sender: nil)
			})
		}) { (error) in
			if let message = error?.userInfo["message"] as? String {
				self.alert(title: nil, message: message, confirmTitle: "confirm", confirm: nil)
			}
		}
	}
}
