//
//  WGQRCodeShareViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.16.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import Kingfisher
import Toaster

class WGQRCodeShareViewController: WGBaseViewController {

	@IBOutlet weak private var qrView: UIView!
	@IBOutlet weak private var profileImageView: UIImageView!
	@IBOutlet weak private var userNameLabel: UILabel!
	@IBOutlet weak private var qrImageView: UIImageView!
	@IBOutlet weak private var noticeLabel: UILabel!
	@IBOutlet weak private var bottomView: UIView!
	
	var user: WGUser!
	
	private let noticeInfo = [["title": "내 QR코드", "notice": "내 QR코드를 저장하거나 공유할 수 있습니다."],
	                          ["title": "친구 QR코드", "notice": "QR코드를 스캔하여 손쉽게 친구를 추가할 수 있습니다."]]
	
	override func prepareUI() {
		super.prepareUI()
		
		navigationController?.setNavigationBarHidden(false, animated: true)
		
		// 이 상황에서 없으면 본인 프로필
		if user == nil {
			user = WGUserService.current
		}
		
		let info = noticeInfo[user.isMe ? 0 : 1]
		
		title = info["title"]
		noticeLabel.text = info["notice"]
		
		updateDataUI()
	}
	
	override func updateDataUI() {
		userNameLabel.text = user.memberName
		profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: user.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
		qrImageView.image = user.qrCodeImage
	}
	
	// MARK: - Action
	
	@IBAction func saveToAlbum() {
		if let screenshot = qrView.screenshot {
			UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil)
			
			toast(message: "이미지를 저장했습니다.", duration: 2, bottomOffsetPortrait: bottomView.bounds.height + 10)
		}
	}
	
	@IBAction func share() {
		if let screenshot = view.screenshot {
			//swiftlint:disable ignore line_length
			let activity = UIActivityViewController(activityItems: [screenshot, "친구들과 영상이나 방송을 시청하며 대화해 보세요!\n신개념 일체형 애니메이션 메신저 'Weegle'\n- 안드로이드: https://play.google.com/store/apps/details?id=com.ojworld.weegle\n- iOS: https://itunes.apple.com/app/id1266689031"],
			                                        applicationActivities: nil)
			
			present(activity, animated: true, completion: nil)
		}
	}
}
