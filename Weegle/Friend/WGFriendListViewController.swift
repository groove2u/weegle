//
//  WGFriendListViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.10.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import PKHUD
import CleanroomLogger

class WGFriendListViewController: WGBaseViewController {

	@IBOutlet weak private var tableView: UITableView!
	@IBOutlet weak private var addFriendButton: UIButton!
	@IBOutlet weak private var noFriendNoticeView: UIView!
	@IBOutlet weak private var searchBar: UISearchBar!
	
	fileprivate var models: Results<WGUser>?
	private var notificationToken: NotificationToken?
	
	override func prepareUI() {
		super.prepareUI()
		
		models = WGUserService.current?.friends?.sorted(byKeyPath: "memberName")
		notificationToken = models?.addNotificationBlock { [weak self] (_) in
			self?.updateDataUI()
		}
	
		tableView.tableFooterView = UIView(frame: .zero)
		tableView.estimatedRowHeight = 80
		tableView.rowHeight = UITableViewAutomaticDimension
		
		NotificationCenter.default.addObserver(self, selector: #selector(fetch), name: WGFriendService.addedNotification.name, object: nil)
		
		if WGUserService.isLogin {
			fetch()
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if WGUserService.current?.isSyncEnabled == true {
			if WGUserService.current?.lastSyncDate == nil {
				syncFriends()
			} else if let lastSyncDateTimeInterval = WGUserService.current?.lastSyncDate?.timeIntervalSinceNow, lastSyncDateTimeInterval < -(60 * 5) {
				syncFriends()
			}
		}
		
		addFriendButton.isHidden = false
	}
	
	private func syncFriends() {
		HUD.flash(.progress)
		
		WGFriendService.sync(success: {
			HUD.hide()
		}, failure: {
			HUD.hide()
		})
	}
	
	deinit {
		notificationToken?.stop()
	}
	
	@objc override func fetch() {
		WGFriendService.update()
	}
	
	override func updateDataUI() {
		tableView.reloadData()
		
		title = "친구 (\(models?.count ?? 0)명)"
		navigationController?.tabBarItem?.title = "친구"
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let identifier = segue.identifier else { return }
		
		switch identifier {
		case "ShowAddFriendSegue":
			addFriendButton.isHidden = true
		case "ShowProfileSegue":
			if let indexPath = sender as? IndexPath,
				let naviController = segue.destination as? WGBaseNavigationController,
				let viewController = naviController.viewControllers.first as? WGFriendProfilePopupViewController {
				Log.info?.value(indexPath.item)
				
				viewController.user = models?[indexPath.item]
			}
		case "ListEditSegue": break
//			hidesBottomBarWhenPushed = true
		default: break
		}
	}
	
	// MARK: - Action
	@IBAction func addFriendViewDismissed(segue: UIStoryboardSegue) {
		dismiss(animated: false) { [weak self] in
			self?.addFriendButton.isHidden = false
		}
	}
}

extension WGFriendListViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let models = models else { noFriendNoticeView.isHidden = false; return 0 }
		
		let result: Int
		
		if let text = searchBar.text, text.isEmpty == false {
			result = models.filter("memberName CONTAINS %@", text).count
		} else {
			result = models.count
		}
		
		noFriendNoticeView.isHidden = result > 0
		
		return result
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let models = models else { return UITableViewCell() }
		guard let cell = tableView.dequeueReusableCell(withIdentifier: WGFriendTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGFriendTableViewCell,
			indexPath.item < models.count else {
			return UITableViewCell()
		}
		
		let model: WGUser
		
		if let text = searchBar.text, text.isEmpty == false {
			model = models.filter("memberName CONTAINS %@", text)[indexPath.item]
		} else {
			model = models[indexPath.item]
		}
		
		cell.nameLabel.text = model.memberName
		cell.profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: model.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
		cell.promotionLabel?.text = model.profileComment
		cell.promotionView.alpha = model.profileComment.isEmpty ? 0 : 1
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		DispatchQueue.main.async {
			self.performSegue(withIdentifier: "ShowProfileSegue", sender: indexPath)
		}
	}
}

extension WGFriendListViewController: UISearchBarDelegate {
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		updateDataUI()
	}
}
