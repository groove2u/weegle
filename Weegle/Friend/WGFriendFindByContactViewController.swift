//
//  WGFriendFindByContactViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.16.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import PKHUD
import Kingfisher
import CleanroomLogger

class WGFriendFindByContactViewController: WGBaseViewController {

	@IBOutlet weak private var noResultView: UIView!
	@IBOutlet weak private var resultView: UIView!
	@IBOutlet weak private var nationalCodeButton: WGDropdownButton!
	@IBOutlet weak private var phoneTextField: UITextField!
	@IBOutlet weak private var profileImageView: UIImageView!
	@IBOutlet weak private var nameLabel: UILabel!
	@IBOutlet weak private var searchButton: UIButton!
	
	private var user: WGUser?
	
	override func prepareUI() {
		super.prepareUI()
		
		navigationController?.setNavigationBarHidden(false, animated: true)
		
		NotificationCenter.default.addObserver(self, selector: #selector(textChanged(notification:)), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
		
		noResultView.isHidden = true
		resultView.isHidden = true
		
		validate()
		
		phoneTextField.becomeFirstResponder()
	}

	// MARK: - TextField
	
	@objc private func textChanged(notification: Notification) {
		validate()
	}
	
	private func validate() {
		searchButton.isEnabled = phoneTextField.text?.isEmpty == false
	}
	
	// MARK: - Action
	
	@IBAction func search() {
		guard let phone = phoneTextField.text, phone.count >= 10 else { return }
		
		WGFriendService.search(phone: phone, nationalCode: nationalCodeButton.title(for: .normal)!, success: { [weak self] (user) in
			self?.user = user
			
			self?.phoneTextField.resignFirstResponder()
			
			if let user = user {
				self?.profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: user.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
				self?.nameLabel.text = user.memberName
				
				self?.noResultView.isHidden = true
				self?.resultView.isHidden = false
			} else {
				self?.noResultView.isHidden = false
				self?.resultView.isHidden = true
			}
		}) { (error) in
			self.noResultView.isHidden = false
			self.resultView.isHidden = true
			
			Log.error?.value(error)
		}
	}
	
	@IBAction func addFriend() {
		guard let user = user else { return }
		
		WGFriendService.add(objectId: user.objectId, success: {
			self.alert(title: nil, message: "T19", confirmTitle: "confirm".localized, confirm: { (_) in
				self.performSegue(withIdentifier: "DismissSegue", sender: nil)
			})
		}) { (error) in
			if let message = error?.userInfo["message"] as? String {
				self.alert(title: nil, message: message, confirmTitle: "confirm".localized, confirm: nil)
			}
		}
	}
}
