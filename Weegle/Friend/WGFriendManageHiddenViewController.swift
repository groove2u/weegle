//
//  WGFriendManageHiddenViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.17.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import PKHUD
import CleanroomLogger

class WGFriendManageHiddenViewController: WGBaseViewController {

	@IBOutlet weak private var tableView: UITableView!
	@IBOutlet weak private var noResultNoticeView: UIView!
	@IBOutlet weak private var searchBar: UISearchBar!
	
	fileprivate var models = [WGUser]()
	
	override func prepareUI() {
		super.prepareUI()
		
		tableView.tableFooterView = UIView(frame: .zero)
		
		fetch()
	}
	
	override func updateUI() {
		super.updateUI()
	}
	
	@objc override func fetch() {
		WGFriendService.relationList(of: .hidden, success: { [weak self] (users) in
			self?.models = users
			
			if self?.models.isEmpty == true {
				self?.noResultNoticeView.isHidden = false
			} else {
				self?.noResultNoticeView.isHidden = true
			}
			
			self?.updateDataUI()
			}, failure: { (_) in
				self.noResultNoticeView.isHidden = false
				
				self.updateDataUI()
		})
	}
	
	override func updateDataUI() {
		tableView.reloadData()
	}
	
	// MARK: - Model
	
	private func model(at indexPath: IndexPath) -> WGUser {
		let model: WGUser
		
		if let text = searchBar.text, text.isEmpty == false {
			model = models.filter { $0.memberName.contains(text) }[indexPath.item]
		} else {
			model = models[indexPath.item]
		}
		
		return model
	}
}

extension WGFriendManageHiddenViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let result: Int
		
		if let text = searchBar.text, text.isEmpty == false {
			result = models.filter { $0.memberName.contains(text) }.count
		} else {
			result = models.count
		}
		
		noResultNoticeView.isHidden = result > 0
		
		return result
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: WGFriendTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGFriendTableViewCell,
			indexPath.item < models.count else {
				return UITableViewCell()
		}
		
		let user = model(at: indexPath)
		
		cell.selectButton?.isSelected = cell.isSelected
		cell.nameLabel.text = user.memberName
		cell.profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: user.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
		cell.promotionLabel?.text = user.profileComment
		cell.promotionView?.alpha = user.profileComment.isEmpty ? 0 : 1
		cell.delegate = self
		
		return cell
	}
}

extension WGFriendManageHiddenViewController: UISearchBarDelegate {
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		updateDataUI()
	}
}

extension WGFriendManageHiddenViewController: ListItemInteractionProtocol {
	
	func listItem(cell: UITableViewCell, didTouched: ListItemInteractionEvent) {
		guard let indexPath = tableView.indexPath(for: cell) else { return }
		
		let user = model(at: indexPath)
		
		let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		
		sheet.addAction(UIAlertAction(title: "친구 목록으로 복귀", style: .default, handler: { (_) in
			WGFriendService.updateRelation(to: .unhidden, objectIdx: [user.objectId], success: {
				self.fetch()

				self.toast(message: "친구 목록으로 복귀 완료", duration: 2)
			}, failure: { (error) in
				Log.error?.value(error)
			})
		}))
		sheet.addAction(UIAlertAction(title: "차단하기", style: .default, handler: { (_) in
			let alert = UIAlertController(title: nil, message: "C2".localized, preferredStyle: .alert)
			
			alert.addAction(UIAlertAction(title: "confirm".localized, style: .destructive, handler: { (_) in
				WGFriendService.updateRelation(to: .block, objectIdx: [user.objectId], success: {
					self.fetch()
					
					self.toast(message: "차단 완료", duration: 2)
				}) { (error) in
					Log.error?.value(error)
				}
			}))
			alert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: nil))
			
			self.present(alert, animated: true)
		}))
		sheet.addAction(UIAlertAction(title: "삭제하기", style: .destructive, handler: { (_) in
			let alert = UIAlertController(title: nil, message: "C4".localized, preferredStyle: .alert)
			
			alert.addAction(UIAlertAction(title: "confirm".localized, style: .destructive, handler: { (_) in
				WGFriendService.delete(objectId: user.objectId, success: {
					self.fetch()
					
					self.toast(message: "삭제 완료", duration: 2)
				}) { (error) in
					Log.error?.value(error)
				}
			}))
			alert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: nil))
			
			self.present(alert, animated: true)
		}))
		sheet.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler: nil))
		
		present(sheet, animated: true)
	}
}
