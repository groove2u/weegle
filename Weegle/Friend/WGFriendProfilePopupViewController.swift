//
//  WGFriendProfilePopupViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import CleanroomLogger

class WGFriendProfilePopupViewController: WGBaseViewController {

	@IBOutlet weak private var profileImageView: UIImageView!
	@IBOutlet weak private var userNameLabel: UILabel!
	@IBOutlet weak private var profileCommentLabel: UILabel!
	@IBOutlet weak private var chatStackView: UIStackView!
	@IBOutlet weak private var QRStackView: UIStackView!
	@IBOutlet weak private var addFriendStackView: UIStackView!
	@IBOutlet weak private var blockStackView: UIStackView!
	
	var user: WGUser!
	
	override func prepareUI() {
		super.prepareUI()
		
		guard user != nil else { return }
		
		if user.isFriend {
			addFriendStackView.removeFromSuperview()
			blockStackView.removeFromSuperview()
		} else if user.isBlocked {
			chatStackView.removeFromSuperview()
			QRStackView.removeFromSuperview()
			addFriendStackView.removeFromSuperview()
			
			if let titleLabel = blockStackView.viewWithTag(10) as? UILabel {
				titleLabel.text = "차단해제"
			}
		} else {
			chatStackView.removeFromSuperview()
			QRStackView.removeFromSuperview()
		}
		
		updateDataUI()
	}
	
	override func updateUI() {
		super.updateUI()
		
		navigationController?.setNavigationBarHidden(true, animated: true)
	}
	
	override func updateDataUI() {
		userNameLabel.text = user.memberName
		profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: user.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
		profileCommentLabel.text = user.profileComment
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let viewController = segue.destination as? WGQRCodeShareViewController {
			viewController.user = user
		}
	}
	
	// MARK: - Action
	
	@IBAction func chat() {
		dismiss(animated: false) {
			let id = self.user.objectId
			
			NotificationCenter.default.post(name: WGChatService.openChatNotification.name, object: nil, userInfo: ["id": [id]])
		}
	}
	
	@IBAction func dismiss() {
		dismiss(animated: false, completion: nil)
	}
	
	@IBAction func friendNameChanged(segue: UIStoryboardSegue) {
		Log.info?.value(segue)
	}
}
