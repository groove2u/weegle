//
//  WGFriendFindByQRViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.16.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import AVFoundation
import CleanroomLogger
import MobileCoreServices
import CoreImage

class WGFriendFindByQRViewController: WGBaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

	@IBOutlet weak private var previewView: UIView!
	@IBOutlet weak private var maskView: UIView!
	@IBOutlet weak private var scanView: UIView!
	
	private let maskLayer = CAShapeLayer()
	
	fileprivate let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
	fileprivate var captureSession: AVCaptureSession! = AVCaptureSession()
	fileprivate var previewLayer: AVCaptureVideoPreviewLayer?
	fileprivate var captureMetaOutput = AVCaptureMetadataOutput()
	
	private var rectPath: UIBezierPath!
	private var scanPath: UIBezierPath!
	
	override func prepareUI() {
		super.prepareUI()
		
		navigationController?.setNavigationBarHidden(false, animated: true)
		
		view.layoutIfNeeded()
		
		beginCapture()
	}
	
	override func updateUI() {
		super.updateUI()
		
		if captureSession?.isRunning == false {
			beginCapture()
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		if captureSession?.isRunning == true {
			captureSession?.stopRunning()
		}
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		rectPath = UIBezierPath(rect: maskView.frame)
		scanPath = UIBezierPath(rect: scanView.frame)
		
		rectPath.append(scanPath)
		rectPath.usesEvenOddFillRule = true
		
		maskLayer.path = rectPath.cgPath
		maskLayer.fillRule = kCAFillRuleEvenOdd
		maskLayer.fillColor = UIColor(hex: 0, alpha: 0.88).cgColor
		
		maskView.layer.addSublayer(maskLayer)
		
		Log.info?.value(scanView.frame)
	}
	
	@IBAction func scanFromAlbum() {
		let picker = UIImagePickerController()
		picker.mediaTypes = [kUTTypeImage as String]
		picker.allowsEditing = true
		picker.delegate = self
		picker.sourceType = .photoLibrary
		
		captureSession?.stopRunning()
		
		present(picker, animated: true)
	}
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		guard let image = info[UIImagePickerControllerEditedImage] as? UIImage,
			let ciImage = CIImage(image: image) else { return }
		
		let detector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy: CIDetectorAccuracyHigh])
		if let features = detector?.features(in: ciImage) as? [CIQRCodeFeature] {
			for feature in features {
				if let memberId = feature.messageString, memberId.isEmpty == false {
					picker.dismiss(animated: true, completion: {
						self.addFriend(memberId: memberId)
					})
					
					break
				}
			}
		}
	}
	
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		picker.dismiss(animated: true) {
			
		}
	}
	
	private func addFriend(memberId: String) {
		WGFriendService.info(memberId: memberId, success: { (user) in
			if let user = user {
				WGFriendService.add(objectId: user.objectId, success: { [weak self] in
					self?.alert(title: nil, message: "친구가 추가되었습니다", confirm: { (_) in
						self?.performSegue(withIdentifier: "DismissSegue", sender: nil)
					})
					
					}, failure: { [weak self] (error) in
						if let message = error?.userInfo["message"] as? String {
							self?.alert(title: nil, message: message) { (_) in
								self?.beginCapture()
							}
						}
				})
			} else {
				Log.error?.trace()
			}
		}, failure: { (error) in
			Log.error?.value(error?.localizedDescription)
		})
	}
}

// MARK: - QR

extension WGFriendFindByQRViewController: AVCaptureMetadataOutputObjectsDelegate {
	
	fileprivate func beginCapture() {
		let input = try? AVCaptureDeviceInput(device: captureDevice!)
		
		if captureSession?.canAddInput(input!) == true {
			captureSession = AVCaptureSession()
			captureSession?.addInput(input!)
			
			captureSession?.addOutput(captureMetaOutput)
			captureMetaOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
			captureMetaOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
			
			previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
			previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
			previewLayer?.frame = previewView.layer.bounds
			previewView.layer.addSublayer(previewLayer!)
			
			NotificationCenter.default.addObserver(self,
			                                       selector: #selector(modifyScanRect(notification:)),
			                                       name: NSNotification.Name.AVCaptureInputPortFormatDescriptionDidChange,
			                                       object: nil)
			
			captureSession?.startRunning()
		} else {
			if captureSession?.inputs.isEmpty == false, captureSession?.outputs.isEmpty == false {
				captureSession?.startRunning()
			} else {
				let alert = UIAlertController(title: nil,
				                              message: "설정 > \(appName) > 카메라 사용을 허용 해주세요",
					preferredStyle: .alert)
				
				alert.addAction(UIAlertAction(title: "설정", style: .default, handler: { (_) in
					if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
						UIApplication.shared.openURL(settingsURL)
					}
				}))
				
				alert.addAction(UIAlertAction(title: "취소", style: .cancel, handler: { (_) in
					self.dismiss(animated: true, completion: nil)
				}))
				
				present(alert, animated: true, completion: nil)
			}
		}
	}
	
	@objc private func modifyScanRect(notification: Notification) {
		captureMetaOutput.rectOfInterest = previewLayer!.metadataOutputRectConverted(fromLayerRect: scanView.frame )
	}
	
	func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
		if metadataObjects.isEmpty {
			Log.info?.message("QR코드 없음")
			
			return
		}
		
		if let metaObject = metadataObjects.first as? AVMetadataMachineReadableCodeObject,
			metaObject.type == AVMetadataObject.ObjectType.qr,
			let memberId = metaObject.stringValue?.split(separator: "/").last {
			
			Log.info?.value(memberId)
			
			captureSession?.stopRunning()
			
			addFriend(memberId: String(memberId))
		}
	}
}
