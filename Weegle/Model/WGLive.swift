//
//  WGLive.swift
//  Weegle
//
//  Created by 이인재 on 15/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import RealmSwift
import Realm
import SwiftyJSON

class WGLive {

	/*
	{
	"id": "596724db0a20b51810d3681f",
	"date": "1499931867858",
	"category": "596626ab0a20b52340f45c94",
	"publicType": 1,
	"thumbPath": "\\2017\\07\\13/chats/1499931867857.PNG",
	"tags": [
		"공개방송",
		"테스트",
		"설명"
	],
	"categoryName": "일상",
	"publicTitle": "진우,민수",
	"secret": 0,
	"livecnt": 1,
	"type": 1,
	"memberObjectId": "5951a3205168e54a9c77d179",
	"owner": {
		"id": "5951a3205168e54a9c77d179",
		"device": "android",
		"memberId": "minsoo",
		"memberName": "민수",
		"nationalCode": "+82",
		"phoneNumber": "01029530247",
		"profileComment": "안녕하세요",
		"profileImage": "\\2017\\07\\061499307101877.jpg",
		"qrCode": "1498522400775.jpg",
		"regId": "eAPF-8XpvBg:APA91bEBKnykMn2jenUkYXGOfb-ZGoBzbRB-GurQI28nwiAbpOaeUVJX5sjYjOtQ2bqpvP1bHBjFn9xRrvMJyeWg31NdGe8Fa9Bb52UhEA6xpGiLPuGvtnR_eEXOj_7kyXdCpGa5Qcai",
		"updateTime": "1499840133988",
		"objectId": "5951a3205168e54a9c77d179"
	},
	"common": {
		"staticDomain": "static.weegle.kr"
	}
	}
	*/
	
	@objc dynamic var id: String = ""
	@objc dynamic var date: String = ""
	@objc dynamic var category: String = ""
	@objc dynamic var publicType: Int = 0
	@objc dynamic var thumbPath: String = ""
	@objc dynamic var tags: [String] = []
	@objc dynamic var categoryName: String = ""
	@objc dynamic var publicTitle: String = ""
	@objc dynamic var secrect: Int = 0
	@objc dynamic var type: Int = 0
	@objc dynamic var memberObjectId: String = ""
	@objc dynamic var common: [String: String] = [:]
	@objc dynamic var owner: WGUser?
	
//	override static func primaryKey() -> String? {
//		return "id"
//	}
}
