//
//  WGChat.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.23.
//  Copyright © 2017 OJ World. All rights reserved.
//

import Foundation
import RealmSwift

enum AttachmentType: Int {
	case photo
	case movie
	case location
	case contact
	case none
}

class WGChat: Object {

	/*
	{
	chatId : String,
	title : String,
	lastDate : Long,
	chatMemberInfo : [
		{
		memberObjectId : String,
		joinDate : Long,
		lastDate : Long,
		enter : Int
		}
	]
	}
	*/

	@objc dynamic var chatId: String = ""
	@objc dynamic var title: String = ""
	@objc dynamic var lastDate: String = ""
	@objc dynamic var enter: Int = 0
	let items = List<WGChatItem>()
	let chatMemberInfo = List<WGChatMember>()

	var others: Results<WGChatMember>? {
		guard let current = WGUserService.current else { return nil }
		
		return chatMemberInfo.filter("memberObjectId != %@", current.objectId)
	}
	
	var otherNames: String {
		if let otherUsers = others {
			var buffer = [String]()
			
			for other in otherUsers {
				if let usr = WGUser.user(objectId: other.memberObjectId) {
					buffer.append(usr.memberName)
				}
			}
			
			return buffer.joined(separator: ", ")
		} else {
			return title
		}
	}
	
	var unreadCount: Int {
		guard let current = WGUserService.current else { return 0 }
		
		var count = 0
		
		items.forEach {
			if $0.writerObjectId != current.objectId {
				count += $0.unreadCount
			}
		}
		
		return count
	}
	
	var lastDateString: String? {
		return WGDateConverter.string(fromTimeintervalString: lastDate, formatter: WGDateConverter.dateFormatter)
	}
	
	var shortTimeString: String? {
		return WGDateConverter.string(fromTimeintervalString: lastDate, formatter: WGDateConverter.shortTimeFormatter)
	}
	
	override static func primaryKey() -> String? {
		return "chatId"
	}
	
	override static func indexedProperties() -> [String] {
		return ["lastDate"]
	}
}
