//
//  WGColor.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.13.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGColor {
	
	private static let baseColors = [UIColor(hex: 0x444444), UIColor(hex: 0x666666), UIColor(hex: 0x777777), UIColor(hex: 0x999999), UIColor(hex: 0xefefef),
	                                 UIColor(hex: 0xbbbbbb), UIColor(hex: 0xdddddd), UIColor(hex: 0xffffff), UIColor(hex: 0x014257), UIColor(hex: 0x63727f),
	                                 UIColor(hex: 0x01333f), UIColor(hex: 0x525d66), UIColor(hex: 0x697985), UIColor(hex: 0xee4169), UIColor(hex: 0x99c63c),
	                                 UIColor(hex: 0x7ca025), UIColor(hex: 0xf15a23), UIColor(hex: 0xd7dee1), UIColor(hex: 0xd24627), UIColor(hex: 0x07b703),
	                                 UIColor(hex: 0xd628c4)]

	private init() {}
	
	/// 가이드 컬러
	///
	/// - Parameter index: 가이드 색상 인덱스. 1부터 시작, max 21.
	/// - Returns: 해당 색상, 인덱스 벗어나면 .clear 리턴
	static func baseColor(for index: Int) -> UIColor {
		guard index > 0, index <= baseColors.count else { return .clear }
		
		return baseColors[index - 1]
	}
}
