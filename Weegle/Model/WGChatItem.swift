//
//  WGChatItem.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.29.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import RealmSwift
import Realm
import SwiftyJSON
import CleanroomLogger

enum ChatExternType: Int {
	case normal = 1
	case image, emoji, video, slide, voice, file, location, live, contact
	case invite = 12
	case enterLive = 14
	case endLive = 16
	case leaveLive = 17
	case leave = 18
	
	var icon: UIImage? {
		switch self {
		case .image:
			return #imageLiteral(resourceName: "chatAttachPhotoIcon")
		case .video:
			return #imageLiteral(resourceName: "chatAttachMovieIcon")
		case .location:
			return #imageLiteral(resourceName: "chatAttachMapIcon")
		case .live:
			return #imageLiteral(resourceName: "chatAttachLiveIcon")
		case .contact:
			return #imageLiteral(resourceName: "chatAttachContactIcon")
		default:
			return nil
		}
	}
}

class WGChatItem: Object {

	/*
	[ {
	messageId : String,
	messageTempId : Long,
	writerObjectId : String,
	message : String,
	extern : String,
	externType : Int,
	date : Long,
	type : Int
	} ]
	*/

	@objc dynamic var messageId: String = ""
	@objc dynamic var messageTempId: String = ""
	@objc dynamic var writerObjectId: String = ""
	@objc dynamic var message: String = ""
	@objc dynamic var extern: String = ""
	@objc dynamic var externType: Int = 0
	@objc dynamic var date: String = ""
	@objc dynamic var type: Int = 0
	@objc dynamic var chatId: String = ""
	
	var chat: WGChat? {
		return WGChat.chat(id: chatId)
	}
	
	var timeString: String? {
		return WGDateConverter.string(fromTimeintervalString: date, formatter: WGDateConverter.shortTimeFormatter)
	}
	
	var unreadCount: Int {
		guard let chat = chat else { return 0 }
		guard let dateInt = Int64(date) else { return 0 }
		
		var count = chat.chatMemberInfo.count
		
		for memberInfo in chat.chatMemberInfo {
			if let memberDateInt = Int64(memberInfo.lastDate) {
				if memberDateInt >= dateInt {
					count -= 1
				} else if memberInfo.enter == "1" {
					count -= 1
				}
			}
		}
		
		return count
	}
	
	var user: WGUser? {
		let found = WGUser.user(objectId: writerObjectId)
		
		return found
	}
	
	override static func primaryKey() -> String? {
		return "messageId"
	}
	
	override static func indexedProperties() -> [String] {
		return ["date"]
	}
}
