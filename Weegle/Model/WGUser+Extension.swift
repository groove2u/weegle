//
//  WGUser+Extension.swift
//  Weegle
//
//  Created by 이인재 on 02/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import RealmSwift
import CleanroomLogger

extension WGUser {

	var friends: Results<WGUser>? {
		guard let realm = WGUserService.realm else { return nil }
		guard let current = WGUserService.current else { return nil }
		
		return realm.objects(WGUser.self).filter("objectId != %@", current.objectId)
	}
	
	var isSyncEnabled: Bool {
		get {
			if UserDefaults.standard.object(forKey: "\(objectId).sync.enabled") == nil {
				UserDefaults.standard.set(true, forKey: "\(objectId).sync.enabled")
				UserDefaults.standard.synchronize()
			}
			
			return UserDefaults.standard.bool(forKey: "\(objectId).sync.enabled")
		}
		
		set {
			UserDefaults.standard.set(newValue, forKey: "\(objectId).sync.enabled")
			UserDefaults.standard.synchronize()
		}
	}
	
	var lastSyncDate: Date? {
		get {
			return UserDefaults.standard.object(forKey: "\(objectId).sync.date") as? Date
		}
		
		set {
			UserDefaults.standard.set(newValue, forKey: "\(objectId).sync.date")
			UserDefaults.standard.synchronize()
		}
	}
	
	static func QRCodeImage(message: String) -> UIImage? {
		guard let data = message.data(using: .isoLatin1, allowLossyConversion: false),
			let filter = CIFilter(name: "CIQRCodeGenerator") else { return nil }
		
		filter.setValue(data, forKey: "inputMessage")
		filter.setValue("Q", forKey: "inputCorrectionLevel")
		
		guard let qrImageSource = filter.outputImage else { return nil }
		
		let cgImage = CIContext().createCGImage(qrImageSource, from: qrImageSource.extent)!
		let scale: CGFloat = 8
		
		//Scale the image usign CoreGraphics
		UIGraphicsBeginImageContext(CGSize(width: qrImageSource.extent.size.width * scale, height: qrImageSource.extent.size.width * scale))
		let context = UIGraphicsGetCurrentContext()!
		context.interpolationQuality = .none
		context.draw(cgImage, in: context.boundingBoxOfClipPath)
		let preImage = UIGraphicsGetImageFromCurrentImageContext()!
		
		//Cleaning up .
		UIGraphicsEndImageContext()
		
		// Rotate the image
		let qrImage = UIImage(cgImage: preImage.cgImage!, scale: preImage.scale, orientation: .downMirrored)
		
		return qrImage
	}
	
	static func clear(includingCurrentUser: Bool = false) {
		guard let realm = WGUserService.realm else { return }
		
		do {
			try realm.write {
				if includingCurrentUser {
					realm.delete(realm.objects(WGUser.self))
				} else {
					if let friends = WGUserService.current?.friends {
						realm.delete(friends)
					}
				}
			}
		} catch {
			Log.error?.value(error)
		}
	}
	
	static func user(id: String) -> WGUser? {
		Log.info?.message("realm: \(WGUserService.realm! == WGUserService.realm!)")
		
		return WGUserService.realm?.object(ofType: WGUser.self, forPrimaryKey: id)
	}
	
	static func user(objectId: String) -> WGUser? {
		return WGUserService.realm?.objects(WGUser.self).filter("objectId = %@", objectId).first
	}
	
	func save(value: Any? = nil, update: Bool = true) {
		guard let realm = WGUserService.realm else { return }
		
		do {
			try realm.write {
				Log.info?.trace()
				
				if let value = value {
					realm.create(WGUser.self, value: value, update: update)
				} else {
					realm.add(self, update: update)
				}
			}
		} catch {
			Log.error?.value(error)
		}
	}
	
	func remove() {
		do {
			try realm?.write {
				realm?.delete(self)
			}
		} catch {
			Log.error?.value(error)
		}
	}
}
