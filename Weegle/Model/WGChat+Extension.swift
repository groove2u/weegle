//
//  WGChat+Extension.swift
//  Weegle
//
//  Created by 이인재 on 02/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift
import CleanroomLogger

extension WGChat {
	
	static func chat(id: String) -> WGChat? {
		return WGUserService.realm?.object(ofType: WGChat.self, forPrimaryKey: id)
	}

	static var list: Results<WGChat>? {
		return WGUserService.realm?.objects(WGChat.self)
	}
	
	func save(value: Any? = nil, update: Bool = true) {
		guard let realm = WGUserService.realm else { return }
		
		do {
			try realm.write {
				if let value = value {
					realm.create(WGChat.self, value: value, update: update)
				} else {
					realm.add(self, update: update)
				}
			}
		} catch {
			Log.error?.value(error)
		}
	}
	
	func add(items: [JSON]) {
		guard let realm = WGUserService.realm else { return }
		
		do {
			try realm.write {
				for var item in items {
					if item["messageInfo"].exists() {
						item = item["messageInfo"]
					}
					
					item["date"].stringValue = item["date"].stringValue
					item["extern"].string = item["extern"].rawString()
					item["messageTempId"].string = ""
					
					let addedItem = realm.create(WGChatItem.self, value: item.object, update: true)
					
					if self.items.contains(addedItem) == false {
						self.items.append(addedItem)
					}
				}
			}
		} catch {
			Log.error?.value(error)
		}
	}
	
	func remove() {
		do {
			try realm?.write {
				realm?.delete(self)
			}
		} catch {
			Log.error?.value(error)
		}
	}
}
