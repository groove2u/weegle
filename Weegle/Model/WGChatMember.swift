//
//  WGChatMember.swift
//  Weegle
//
//  Created by 이인재 on 12/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import Foundation
import RealmSwift

class WGChatMember: Object {

	@objc dynamic var memberObjectId: String = ""
	@objc dynamic var joinDate: String = ""
	@objc dynamic var lastDate: String = ""
	@objc dynamic var enter: String = ""
	
	override static func primaryKey() -> String? {
		return "memberObjectId"
	}
	
	override static func indexedProperties() -> [String] {
		return ["lastDate", "enter"]
	}
}
