//
//  WGUser.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftKeychainWrapper
import SwiftyJSON

class WGUser: Object {

	/*
	"objectId": "5951a3205168e54a9c77d179",
	"memberId": "minsoo",
	"memberName": "민수",
	"nationalCode": "+82",
	"phoneNumber": "01011111117",
	"memberImage": "1498791064173.jpg",
	"memberComment": "상태 메세지\n",
	"updateTime": "1498815954762",
	"qrCode": "1498522400775.jpg",
	"isFriend" :true or false
	*/
	
	@objc dynamic var id: String = ""
	@objc dynamic var objectId: String = ""
	@objc dynamic var memberName: String = ""
	@objc dynamic var memberId: String = ""
	@objc dynamic var profileImage: String = ""
	@objc dynamic var profileComment: String = ""
	@objc dynamic var password: String = ""
	@objc dynamic var nationalCode: String = ""
	@objc dynamic var phoneNumber: String = ""
	@objc dynamic var memberImage: String = ""
	@objc dynamic var updateTime: String = ""
	@objc dynamic var qrCode: String = ""
	@objc dynamic var friendCount: Int = 0
	@objc dynamic var isFriend: Bool = true
	@objc dynamic var isBlocked: Bool = false
	var isMe: Bool {
		return WGUserService.current?.objectId == objectId
	}
	var qrCodeImage: UIImage? {
		return WGUser.QRCodeImage(message: "http://api.weegle.kr/api/v1/getMember/\(memberId)")
	}
	var qrCodeImageData: Data? {
		guard let image = qrCodeImage else { return nil }
		guard let imageData = UIImageJPEGRepresentation(image, 0.85) else { return nil }
		
		return imageData
	}
	
	override static func primaryKey() -> String? {
		return "objectId"
	}
}
