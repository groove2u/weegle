//
//  ListItemInteractionProtocol.swift
//  Weegle
//
//  Created by 이인재 on 24/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

enum ListItemInteractionEvent {
	case select
	case add
	case delete
	case detail
	case more
	case custom(info: [String: Any])
}

protocol ListItemInteractionProtocol: class {
	
	func listItem(cell: UITableViewCell, didTouched event: ListItemInteractionEvent)
}

protocol CollectionItemInteractionProtocol: class {
	
	func listItem(cell: UICollectionViewCell, didTouched event: ListItemInteractionEvent)
}
