//
//  WGDetailListTableViewCell.swift
//  Weegle
//
//  Created by 이인재 on 06/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGDetailListTableViewCell: UITableViewCell {

	@IBOutlet weak var mainTitleLabel: UILabel!
	@IBOutlet weak var contentsLabel: UILabel!
	@IBOutlet weak var contentsView: UIView!
}
