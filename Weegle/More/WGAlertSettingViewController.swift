//
//  WGAlertSettingViewController.swift
//  Weegle
//
//  Created by 이인재 on 17/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import UserNotifications

class WGAlertSettingViewController: WGBaseViewController {

	@IBOutlet weak var alertSwitch: UISwitch!
	
	override func prepareUI() {
		super.prepareUI()
		
		WGDeviceService.remoteNotificationStatus { (isEnabled) in
			self.alertSwitch.isOn = isEnabled
		}
	}
	
	// MARK: - Action
	
	@IBAction func alertSettingChanged(_ sender: UISwitch) {
		WGDeviceService.remoteNotificationStatus { (isEnabled) in
			if isEnabled {
				if sender.isOn {
					WGDeviceService.registerRemotePush()
				} else {
					WGDeviceService.unregisterRemotePush()
				}
			} else {
				if sender.isOn {
					sender.isOn = false
					
					let alert = UIAlertController(title: nil, message: "설정 > \(appName) > 알림 설정을 허용 해주세요", preferredStyle: .alert)
					
					alert.addAction(UIAlertAction(title: "설정", style: .cancel, handler: { (_) in
						if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
							UIApplication.shared.openURL(settingsURL)
						}
					}))
					
					alert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: { (_) in
						
					}))
					
					self.present(alert, animated: true, completion: nil)
				} else {
					WGDeviceService.unregisterRemotePush()
				}
			}
		}
	}
}
