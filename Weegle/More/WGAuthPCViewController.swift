//
//  WGAuthPCViewController.swift
//  Weegle
//
//  Created by 이인재 on 14/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGAuthPCViewController: WGBaseViewController {

	@IBOutlet weak private var authStackView: UIStackView!
	@IBOutlet weak private var authCodeLabel: UILabel!
	@IBOutlet weak private var timerLabel: UILabel!
	@IBOutlet weak private var noDataStackView: UIStackView!
}
