//
//  WGMoreViewController.swift
//  Weegle
//
//  Created by 이인재 on 13/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import MobileCoreServices
import SwiftyJSON
import CleanroomLogger

class WGMoreViewController: WGBaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	
	@IBOutlet weak private var profileImageView: UIImageView!
	@IBOutlet weak private var userNameLabel: UILabel!
	@IBOutlet weak private var userIDLabel: UILabel!
	
	override func prepareUI() {
		super.prepareUI()
		
		NotificationCenter.default.addObserver(self, selector: #selector(profileChanged(notification:)), name: NSNotification.Name.UserProfileChanged, object: nil)
		
		updateDataUI()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "NoticeSegue", let viewController = segue.destination as? WGDetailListViewController, let json = sender as? [JSON] {
			viewController.title = "공지사항"
			viewController.items = json
		}
	}
	
	override func updateDataUI() {
		if let current = WGUserService.current {
			userNameLabel.text = current.memberName
			userIDLabel.text = current.memberId
			profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: current.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
		}
	}
	
	@objc private func profileChanged(notification: Notification) {
		updateDataUI()
	}
	
	@IBAction func notice() {
		WGAppService.notice(success: { (json) in
			self.performSegue(withIdentifier: "NoticeSegue", sender: json)
		}) { (error) in
			Log.error?.value(error)
		}
	}
}
