//
//  WGAskViewController.swift
//  Weegle
//
//  Created by 이인재 on 04/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import MobileCoreServices
import CleanroomLogger

class WGAskViewController: WGBaseViewController {

	@IBOutlet weak private var noticeLabel: UILabel!
	@IBOutlet weak private var contentsTextView: UITextView!
	@IBOutlet weak private var emailTextField: UITextField!
	@IBOutlet weak private var attachmentFileName: UILabel!
	@IBOutlet weak private var agreeButton: UIButton!
	
	private var image: UIImage?
	
	// MARK: - Action
	
	@IBAction func addAttachment() {
		let picker = UIImagePickerController()
		picker.mediaTypes = [kUTTypeImage as String]
		picker.allowsEditing = false
		picker.delegate = self
		
		let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		
		sheet.addAction(UIAlertAction(title: "카메라", style: .default, handler: { (_) in
			picker.sourceType = .camera
			
			self.present(picker, animated: true)
		}))
		sheet.addAction(UIAlertAction(title: "앨범", style: .default, handler: { (_) in
			picker.sourceType = .photoLibrary
			
			self.present(picker, animated: true)
		}))
		sheet.addAction(UIAlertAction(title: "cancel".localized, style: .cancel))
		
		present(sheet, animated: true)
	}
	
	@IBAction func toggleAgree(_ sender: UIButton) {
		sender.isSelected = !sender.isSelected
	}
	
	@IBAction func ask() {
		guard let contents = contentsTextView.text, contents.isEmpty == false else {
			self.alert(title: nil, message: "T35", confirmTitle: "confirm", confirm: { (_) in
				self.contentsTextView.becomeFirstResponder()
			})
			
			return
		}
		
		guard emailTextField.text?.isEmpty == false else {
			self.alert(title: nil, message: "T36", confirmTitle: "confirm", confirm: { (_) in
				self.emailTextField.becomeFirstResponder()
			})
			
			return
		}
		
		guard let email = emailTextField.text, email.isEmail else {
			self.alert(title: nil, message: "T37", confirmTitle: "confirm", confirm: { (_) in
				self.emailTextField.becomeFirstResponder()
			})
			
			return
		}
		
		guard agreeButton.isSelected else {
			self.alert(title: nil, message: "T38", confirmTitle: "confirm", confirm: { (_) in
				
			})
			
			return
		}
		
		WGUserService.ask(contents: contents, email: email, image: image, success: {
			self.alert(title: nil, message: "T4", confirmTitle: "confirm", confirm: { (_) in
				self.navigationController?.popViewController(animated: true)
			})
		}, progress: { (_) in
			
		}) { (error) in
			Log.error?.value(error)
		}
	}
}

// MARK: - UIImagePickerControllerDelegate

extension WGAskViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
			Log.info?.value(image)
			
			self.image = image
			
			picker.dismiss(animated: true, completion: {
				self.attachmentFileName.text = "이미지"
			})
		}
	}
	
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		picker.dismiss(animated: true, completion: nil)
	}
}

// MARK: - UITextViewDelegate

extension WGAskViewController {
	
	func textViewDidChange(_ textView: UITextView) {
		Log.info?.value(textView.text!)
		
		noticeLabel.isHidden = textView.text?.isEmpty == false
	}
}
