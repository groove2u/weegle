//
//  WGWithdrawViewController.swift
//  Weegle
//
//  Created by 이인재 on 19/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import CleanroomLogger

class WGWithdrawViewController: WGBaseViewController {

	@IBOutlet weak private var withdrawButton: WGButton!
	
	@IBAction func toggleAgree(_ sender: UIButton) {
		sender.isSelected = !sender.isSelected
		
		withdrawButton.isEnabled = sender.isSelected
	}
	
	@IBAction func withdraw() {
		let alert = UIAlertController(title: nil, message: "C17".localized, preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(title: "confirm".localized, style: .destructive, handler: { (_) in
			WGUserService.withdraw(success: {
				
			}) { (error) in
				Log.error?.value(error)
			}
		}))
		alert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: nil))
		
		present(alert, animated: true, completion: nil)
	}
}
