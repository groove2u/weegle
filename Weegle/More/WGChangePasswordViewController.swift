//
//  WGChangePasswordViewController.swift
//  Weegle
//
//  Created by 이인재 on 17/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGChangePasswordViewController: WGBaseViewController {

	@IBOutlet weak private var currentPasswordTextField: UITextField!
	@IBOutlet weak private var newPasswordTextField: UITextField!
	@IBOutlet weak private var confirmNewPasswordTextField: UITextField!
	
	// MARK: - Action
	
	@IBAction func confirm() {
		guard let currentPassword = currentPasswordTextField.validate(title: nil, message: "T27".localized, confirmTitle: "confirm".localized) else { return }
		guard let newPassword = newPasswordTextField.validate(title: nil, message: "T27".localized, confirmTitle: "confirm".localized) else { return }
		guard let passwordCheck = confirmNewPasswordTextField.validate(title: nil, message: "T27".localized, confirmTitle: "confirm".localized) else { return }
		guard newPassword == passwordCheck else { alert(message: "T29".localized) { [weak self] (_) in self?.confirmNewPasswordTextField.becomeFirstResponder() }; return }
		guard newPassword.count >= 8, newPassword.count <= 30 else { alert(message: "T28"); return }
		
		WGUserService.updatePassword(current: currentPassword, to: newPassword,
		                             success: {
										self.alert(title: nil, message: "T3", confirmTitle: "confirm", confirm: { (_) in
											self.navigationController?.popViewController(animated: true)
										})
		}) { (error) in
			if let error = error {
				self.alert(title: nil, message: error, confirmTitle: "confirm")
			}
		}
	}
}
