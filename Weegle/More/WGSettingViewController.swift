//
//  WGSettingViewController.swift
//  Weegle
//
//  Created by 이인재 on 06/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import MobileCoreServices
import SwiftyJSON
import CleanroomLogger

class WGSettingViewController: WGBaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

	@IBOutlet weak private var versionLabel: UILabel!
	@IBOutlet weak private var passwordView: UIView!
	@IBOutlet weak var alertStatusLabel: UILabel!
	
	override func prepareUI() {
		super.prepareUI()
		
		versionLabel.text = UIApplication.appVersion
		
		passwordView.isHidden = WGUserService.loginType != .default
		
		NotificationCenter.default.addObserver(self, selector: #selector(applicationWillForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let viewController = segue.destination as? WGWebViewController {
			if segue.identifier == "TermsSegue" {
				viewController.title = "이용약관"
				viewController.fileURL = Bundle.main.url(forResource: "Terms", withExtension: "rtf")
			} else if segue.identifier == "PrivacySegue" {
				viewController.title = "개인정보 취급방침"
				viewController.fileURL = Bundle.main.url(forResource: "Privacy", withExtension: "rtf")
			}
		} else if let viewController = segue.destination as? WGDetailListViewController {
			if segue.identifier == "HelpSegue", let json = sender as? [JSON] {
				viewController.title = "도움말"
				viewController.items = json
			}
		}
	}
	
	// MARK: - Notification
	
	@objc private func applicationWillForeground(_ notification: Notification) {
		WGDeviceService.remoteNotificationStatus { (isEnabled) in
			self.alertStatusLabel.text = isEnabled ? "켜짐" : "꺼짐"
		}
	}
	
	// MARK: - Action
	
	@IBAction func showHelp() {
		WGAppService.help(success: { (json) in
			self.performSegue(withIdentifier: "HelpSegue", sender: json)
		}, failure: { (error) in
			Log.error?.value(error)
		})
	}
	
	@IBAction func logout() {
		WGUserService.logout()
	}
}
