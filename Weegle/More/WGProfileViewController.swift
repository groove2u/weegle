//
//  WGProfileViewController.swift
//  Weegle
//
//  Created by 이인재 on 17/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import MobileCoreServices
import PKHUD
import CleanroomLogger

class WGProfileViewController: WGBaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

	@IBOutlet weak private var profileImageView: UIImageView!
	@IBOutlet weak private var userNameLabel: UILabel!
	@IBOutlet weak private var userIDLabel: UILabel!
	@IBOutlet weak private var profileCommentTextField: UITextField!
	
	override func prepareUI() {
		super.prepareUI()
		
		if let current = WGUserService.current {
			userNameLabel.text = current.memberName
			userIDLabel.text = "ID : \(current.memberId)"
			profileCommentTextField.text = current.profileComment
			profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: current.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
		}
	}
	
	@IBAction func selectProfileImage() {
		let picker = UIImagePickerController()
		picker.mediaTypes = [kUTTypeImage as String]
		picker.allowsEditing = true
		picker.delegate = self
		
		let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		
		sheet.addAction(UIAlertAction(title: "카메라", style: .default, handler: { (_) in
			picker.sourceType = .camera
		
			self.present(picker, animated: true)
		}))
		sheet.addAction(UIAlertAction(title: "앨범", style: .default, handler: { (_) in
			picker.sourceType = .photoLibrary
			
			self.present(picker, animated: true)
		}))
		sheet.addAction(UIAlertAction(title: "기본이미지로 변경", style: .default, handler: { (_) in
			WGUserService.deleteProfileImage(success: {
				self.profileImageView.image = #imageLiteral(resourceName: "commonProfileImage")
			}, failure: { (_) in
				
			})
		}))
		sheet.addAction(UIAlertAction(title: "cancel".localized, style: .cancel))
		
		present(sheet, animated: true)
	}
	
	@IBAction func updateProfileInfo() {
		guard let current = WGUserService.current else { return }
		guard let comment = profileCommentTextField.text else { return }

		WGUserService.updateProfileInfo(name: current.memberName, comment: comment, success: {
			Log.info?.message("프로필 저장 완료")
			
			super.dismiss()
		}) { (_) in

		}
	}
	
	// MARK: - UIImagePickerControllerDelegate
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else { return }
		
		WGUserService.uploadProfileImage(image: image, success: {
			self.profileImageView.image = image
			
			picker.dismiss(animated: true)
			
			HUD.hide()
		}, progress: { progress in
			HUD.flash(.progress)
		}, failure: { (_) in
			picker.dismiss(animated: true)
		})
	}
	
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		picker.dismiss(animated: true)
	}
	
	// MARK: - Override
	override func dismiss(_ sender: AnyObject?) {
		view.endEditing(true)
		
		let alert = UIAlertController(title: nil, message: "C8".localized, preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(title: "confirm".localized, style: .cancel, handler: { (_) in
			self.updateProfileInfo()
		}))
		alert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: { (_) in
			super.dismiss()
		}))
		
		present(alert, animated: true)
	}
}
