//
//  WGVersionViewController.swift
//  Weegle
//
//  Created by 이인재 on 17/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import CleanroomLogger

class WGVersionViewController: WGBaseViewController {

	@IBOutlet weak private var currentVersionLabel: UILabel!
	@IBOutlet weak private var lastestVersionLabel: UILabel!
	@IBOutlet weak private var updateButton: UIButton!
	
	override func prepareUI() {
		super.prepareUI()
		
		currentVersionLabel.text = "현재 버전 \(UIApplication.appVersion)"
		
		WGAppService.version(success: { (json) in
			self.lastestVersionLabel.text = "최신 버전 \(json["iphone"].stringValue)"
			
			if let appVersion = Int(UIApplication.appVersion.replacingOccurrences(of: ".", with: "")),
				let lastestVersion = Int(json["iphone"].stringValue.replacingOccurrences(of: ".", with: "")) {
				let isUpdateAvailable = appVersion < lastestVersion
				
				self.updateButton.isEnabled = isUpdateAvailable
				self.updateButton.setTitle(isUpdateAvailable ? "최신 버전 업데이트 하기" : "최신 버전입니다", for: .normal)
				
				if isUpdateAvailable {
					self.updateButton.backgroundColor = UIColor(hex: 0x365358)
					self.updateButton.borderWidth = 0
					self.updateButton.setTitleColor(.white, for: .normal)
				}
			}
		}) { (error) in
			Log.error?.value(error)
		}
	}
	
	// MARK: - Action
	
	@IBAction func update() {
		if let url = URL(string: "https://itunes.apple.com/app/id1266689031") {
			UIApplication.shared.openURL(url)
		}
	}
}
