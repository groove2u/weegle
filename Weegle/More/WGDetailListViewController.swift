//
//  WGDetailListViewController.swift
//  Weegle
//
//  Created by 이인재 on 06/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import SwiftyJSON
import CleanroomLogger

class WGDetailListViewController: UITableViewController {

	var items = [JSON]()
	
	private var indexPathsForSelectedRows = [IndexPath]()
	
    override func viewDidLoad() {
        super.viewDidLoad()

		prepareUI()
    }

	// MARK: - Custom
	
	private func prepareUI() {
		tableView.tableFooterView = UIView(frame: .zero)
		
		updateDataUI()
	}
	
	private func updateDataUI() {
		tableView.reloadData()
	}

	@IBAction func pop(_ sender: Any) {
		navigationController?.popViewController(animated: true)
	}
	
	// MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: WGDetailListTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGDetailListTableViewCell else {
			return UITableViewCell()
		}
		
		let item = items[indexPath.item]
		
		cell.mainTitleLabel.text = item["title"].string
		cell.contentsLabel?.text = item["content"].string
		cell.contentsView.isHidden = indexPathsForSelectedRows.contains(indexPath) == false
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if let idx = indexPathsForSelectedRows.index(of: indexPath) {
			indexPathsForSelectedRows.remove(at: idx)
		} else {
			indexPathsForSelectedRows.append(indexPath)
		}
		
		tableView.reloadRows(at: [indexPath], with: .none)
	}
}
