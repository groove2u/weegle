//
//  WGManagePCViewController.swift
//  Weegle
//
//  Created by 이인재 on 14/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGManagePCViewController: WGBaseViewController {

	@IBOutlet weak private var stackView: UIStackView!
	
	private var items: [String] = ["PC1", "PC2", "PC3"]
	
	override func prepareUI() {
		super.prepareUI()
		
		for item in items {
			if let view = Bundle.main.loadNibNamed(String(describing: WGPCCellView.self), owner: nil, options: nil)?.first as? WGPCCellView {
				view.nameLabel.text = item
				
				stackView.addArrangedSubview(view)
				stackView.addSubview(view)
			}
		}
	}
}
