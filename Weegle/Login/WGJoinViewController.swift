//
//  WGJoinViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import PKHUD
import CleanroomLogger

class WGJoinViewController: WGBaseViewController {

	@IBOutlet weak private var userNameTextField: WGTextField!
	@IBOutlet weak private var idTextField: WGTextField!
	@IBOutlet weak private var passwordTextField: WGTextField!
	@IBOutlet weak private var passwordCheckTextField: WGTextField!
	@IBOutlet weak private var duplicationCheckButton: WGSubMiniButton!
	@IBOutlet weak private var phoneNumberTextField: WGTextField!
	@IBOutlet weak private var nationalNumberButton: WGDropdownButton!
	@IBOutlet weak private var authCodeTextField: WGTextField!
	@IBOutlet weak private var privacyButton: UIButton!
	@IBOutlet weak private var termButton: UIButton!
	@IBOutlet var normalJoinViews: [UIView]!
	@IBOutlet weak private var completeJoinButton: WGMainButton!
	
	var type: WGLoginType = .default {
		didSet {
			switch type {
			case .default:
				title = "회원가입"
			case .facebook:
				title = "페이스북 회원가입"
			case .google:
				title = "구글 회원가입"
			}
		}
	}
	var token: String?
	var userID: String?
	
	private var authId: String?
	private var isIdDuplicationChecked = false
	
	// Prepare
	
	override func prepareUI() {
		super.prepareUI()
		
		if type != .default {
			normalJoinViews.forEach {
				$0.removeFromSuperview()
				
				if let stackView = $0.superview as? UIStackView {
					stackView.removeArrangedSubview($0)
				}
			}
			
			isIdDuplicationChecked = true
		}
		
		navigationController?.setNavigationBarHidden(false, animated: true)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let viewController = segue.destination as? WGWebViewController {
			if segue.identifier == "TermsSegue" {
				viewController.title = "이용약관"
				viewController.fileURL = Bundle.main.url(forResource: "Terms", withExtension: "rtf")
			} else if segue.identifier == "PrivacySegue" {
				viewController.title = "개인정보 취급방침"
				viewController.fileURL = Bundle.main.url(forResource: "Privacy", withExtension: "rtf")
			}
		}
	}
	
	// MARK: - Custom
	private func validate() -> WGUser? {
		guard let name = userNameTextField.validate(title: nil, message: "T24".localized, confirmTitle: "confirm".localized) else { return nil }
		guard name.characters.count >= 2, name.characters.count <= 30 else { alert(message: "T24"); return nil }
		guard let phoneNumber = phoneNumberTextField.validate(title: nil, message: "T30".localized, confirmTitle: "confirm".localized) else { return nil }
		
		let user = WGUser()
		user.memberName = name
		user.phoneNumber = phoneNumber
		user.nationalCode = nationalNumberButton.title(for: .normal)!

		switch type {
		case .default:
			guard let nickName = idTextField.validate(title: nil, message: "T25".localized, confirmTitle: "confirm".localized) else { return nil }
			guard nickName.characters.count >= 6, nickName.characters.count <= 30 else { alert(message: "T28"); return nil }
			guard isIdDuplicationChecked else { alert(message: "T26"); return nil }
			guard let password = passwordTextField.validate(title: nil, message: "T27".localized, confirmTitle: "confirm".localized) else { return nil }
			guard let passwordCheck = passwordCheckTextField.validate(title: nil, message: "T27".localized, confirmTitle: "confirm".localized) else { return nil }
			guard password == passwordCheck else { alert(message: "T29".localized) { [weak self] (_) in self?.passwordCheckTextField.becomeFirstResponder() }; return nil }
			guard password.characters.count >= 8, password.characters.count <= 30 else { alert(message: "T28"); return nil }
			
			user.memberId = nickName
			user.password = password
		default:
			guard let userID = userID else { return nil }
			user.id = userID
		}
		
		return user
	}
	
	// MARK: - Action
	
	@IBAction func checkTerms(_ sender: UIButton) {
		sender.isSelected = !sender.isSelected
	}
	
	@IBAction func checkIdDuplication() {
		guard isIdDuplicationChecked == false else { return }
		guard let nickName = idTextField.validate(title: nil, message: "T25".localized, confirmTitle: "confirm".localized) else { return }
		
		WGUserService.checkDuplication(nickName: nickName, success: { [weak self] (isIdUnique)in
			if isIdUnique {
				let alert = UIAlertController(title: nil, message: "T20".localized, preferredStyle: .alert)
				alert.addAction(UIAlertAction(title: "confirm".localized, style: .default, handler: { (_) in
					self?.isIdDuplicationChecked = true
					self?.idTextField.isEnabled = false
					self?.passwordTextField.becomeFirstResponder()
				}))
				alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler: { (_) in
					self?.isIdDuplicationChecked = false
				}))
				self?.present(alert, animated: true, completion: nil)
			} else {
				let alert = UIAlertController(title: nil, message: "T21".localized, confirmTitle: "confirm".localized, confirm: nil)
				self?.present(alert, animated: true, completion: nil)
			}
		}) { (error) in
			Log.error?.value(error)
		}
	}
	
	@IBAction func showNationalCodes(_ sender: Any) {
		
	}
	
	@IBAction func requestAutCode() {
		guard let phoneNumber = phoneNumberTextField.validate(title: nil, message: "T23".localized, confirmTitle: "confirm".localized) else { return }
		
		phoneNumberTextField.resignFirstResponder()
		
		WGUserService.phoneNumberAuthCode(nationalCode: nationalNumberButton.title(for: .normal)!, phoneNumber: phoneNumber, success: { [weak self] (id) in
			let alert = UIAlertController(title: nil, message: "T22".localized, confirmTitle: "confirm".localized, confirm: { (_) in
				self?.phoneNumberTextField.isEnabled = false
				self?.nationalNumberButton.isEnabled = false
				
				self?.authId = id
				
				self?.authCodeTextField.becomeFirstResponder()
			})
			self?.present(alert, animated: true, completion: nil)
		}) { (error) in
			Log.error?.value(error)
		}
	}
	
	@IBAction func completeJoin() {
		guard let authId = authId else { alert(message: "T31"); return }
		guard let authCode = authCodeTextField.validate(title: nil, message: "T31".localized, confirmTitle: "confirm".localized) else { return }
		guard let user = validate() else { return }
		guard privacyButton.isSelected else { alert(message: "T32"); return }
		guard termButton.isSelected else { alert(message: "T33"); return }
		
		func resetAuthCode() {
			self.authId = nil
			
			nationalNumberButton.isEnabled = true
			phoneNumberTextField.isEnabled = true
		}

		if type == .default {
			// 인증코드 확인
			WGUserService.verifyAuthCode(authId: authId, code: authCode, success: { [weak self] in
				// 가입
				WGUserService.join(user: user, success: {
					// 로그인
					WGUserService.login(id: user.memberId, password: user.password, success: {
						self?.alert(message: "T41") { (_) in
							self?.performSegue(withIdentifier: "LoginCompleteSegue", sender: nil)
						}
					}, failure: { (error) in
						if let message = error?.userInfo["message"] as? String {
							self?.alert(title: nil, message: message)
						}
						
						resetAuthCode()
						
						Log.error?.value(error)
					})
				}, failure: { (error) in
					if let message = error?.userInfo["message"] as? String {
						self?.alert(title: nil, message: message)
					}
					
					resetAuthCode()
					
					Log.error?.value(error)
				})
			}) { [weak self] (error) in
				if let message = error?.userInfo["message"] as? String {
					self?.alert(title: nil, message: message) { (_) in
						resetAuthCode()
					}
				}
				
				Log.error?.value(error)
			}
		} else {
			guard let token = token else {
				Log.error?.trace()
				
				return
			}
			
			WGUserService.join(user: user, type: type, token: token, authCode: authCode, success: {
				WGUserService.login(type: self.type, snsToken: token, success: {
					self.alert(message: "T41") { (_) in
						self.performSegue(withIdentifier: "LoginCompleteSegue", sender: nil)
					}
				}, failure: { (error) in
					if let message = error?.userInfo["message"] as? String {
						self.alert(title: nil, message: message)
					}
					
					resetAuthCode()
					
					Log.error?.value(error)
				})
			}, failure: { (error) in
				if let message = error?.userInfo["message"] as? String {
					self.alert(title: nil, message: message)
				}
				
				resetAuthCode()
				
				Log.error?.value(error)
			})
		}
	}
}
