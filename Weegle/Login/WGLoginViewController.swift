//
//  WGLoginViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.11.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import PKHUD
import CleanroomLogger

class WGLoginViewController: WGBaseViewController, GIDSignInDelegate, GIDSignInUIDelegate {

	@IBOutlet weak private var idTextField: WGTextField!
	@IBOutlet weak private var passwordTextField: WGTextField!
	@IBOutlet weak private var facebookButton: WGOptionButton!
	@IBOutlet weak private var googleButton: WGOptionButton!
	
	override func updateUI() {
		super.updateUI()
		
		navigationController?.setNavigationBarHidden(true, animated: true)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let identifier = segue.identifier {
			if let viewController = segue.destination as? WGJoinViewController {
				switch identifier {
				case "FacebookSegue":
					if let info = sender as? [String: String],
						let token = info["token"],
						let userID = info["userID"] {
						viewController.token = token
						viewController.userID = userID
						viewController.type = .facebook
					}
				case "GoogleSegue":
					if let info = sender as? [String: String],
						let token = info["token"],
						let userID = info["userID"] {
						viewController.token = token
						viewController.userID = userID
						viewController.type = .google
					}
				default:
					viewController.type = .default
				}
			}
		}
	}
	
	private func facebookLogin() {
		FBSDKLoginManager().logOut()
		
		let manager = FBSDKLoginManager()
		manager.logIn(withReadPermissions: ["public_profile"], from: self, handler: { (_, error) in
			guard error == nil else { Log.error?.value(error); return }
			
			FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "name"]).start { (_, result, error) in
				if error == nil {
					HUD.flash(.progress)
					
					WGUserService.login(type: .facebook, snsToken: FBSDKAccessToken.current().tokenString, success: {
						HUD.hide()
						
						self.dismiss()
					}, failure: { (error) in
						HUD.hide()
						
						Log.error?.value(error)
						
						self.performSegue(withIdentifier: "FacebookSegue", sender: ["userID": FBSDKAccessToken.current().userID, "token": FBSDKAccessToken.current().tokenString])
					})
					
					Log.info?.value(result)
				} else {
					Log.error?.value(error)
				}
			}
		})
	}
	
	// MARK: - Google sign in
	
	private func googleLogin() {
		GIDSignIn.sharedInstance().delegate = self
		GIDSignIn.sharedInstance().uiDelegate = self
		
		GIDSignIn.sharedInstance().signIn()
	}
	
	// MARK: - Google Sign in
	
	func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
		viewController.modalPresentationStyle = .overFullScreen
		
		present(viewController, animated: true, completion: nil)
	}
	
	func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
		viewController.dismiss(animated: true, completion: nil)
	}
	
	func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
		if error == nil {
			HUD.flash(.progress)
			
			WGUserService.login(type: .google, snsToken: user.authentication.accessToken, success: {
				HUD.hide()
				
				self.dismiss()
			}, failure: { (error) in
				HUD.hide()
				
				self.performSegue(withIdentifier: "GoogleSegue", sender: ["userID": user.userID, "token": user.authentication.accessToken])
				
				Log.error?.value(error)
			})
		} else {
			Log.error?.value(error)
		}
	}
	
	func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!, withError error: Error!) {
		GIDSignIn.sharedInstance().signOut()
	}
	
	// MARK: - Action
	@IBAction func socialLogin(_ sender: UIButton) {
		if sender == facebookButton {
			facebookLogin()
		} else {
			googleLogin()
		}
	}
	
	@IBAction func login() {
		guard let nickName = idTextField.validate(title: nil, message: "T25".localized, confirmTitle: "confirm".localized) else { return }
		guard nickName.characters.count >= 6, nickName.characters.count <= 30 else { alert(message: "T28"); return }
		guard let password = passwordTextField.validate(title: nil, message: "T27".localized, confirmTitle: "confirm".localized) else { return }
		guard password.characters.count >= 8, password.characters.count <= 30 else { alert(message: "T28"); return }
		
		WGUserService.login(id: nickName, password: password, success: { [weak self] in
			self?.dismiss(animated: true, completion: nil)
		}) { [weak self] (error) in
			if let message = error?.userInfo["message"] as? String {
				self?.alert(title: nil, message: message, confirmTitle: "confirm".localized)
			}
		}
	}
	
	@IBAction func loginComplete(segue: UIStoryboardSegue) {
		dismiss(animated: true, completion: nil)
	}
}
