//
//  WGFindPasswordViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.13.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGFindPasswordViewController: WGBaseViewController {

	override func prepareUI() {
		super.prepareUI()
		
		navigationController?.setNavigationBarHidden(false, animated: true)
	}
}
