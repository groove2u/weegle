//
//  WGWebViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import CleanroomLogger

class WGWebViewController: WGBaseViewController {

	@IBOutlet weak private var titleLabel: UILabel!
	@IBOutlet weak private var webView: UIWebView!
	
	var url: String?
	var fileURL: URL?
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	override func prepareUI() {
		super.prepareUI()
		
		if let url = url, let targetURL = URL(string: url) {
			webView.loadRequest(URLRequest(url: targetURL))
		} else if let url = fileURL {
			do {
				let data = try Data(contentsOf: url)
				
				webView.load(data, mimeType: "application/rtf", textEncodingName: "utf8", baseURL: url)
			} catch {
				Log.error?.value(error)
			}
		}
		
		titleLabel.text = title
	}
}

extension WGWebViewController: UIWebViewDelegate {
	
}
