//
//  MPUtil.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.16.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

var appName: String {
	return (Bundle.main.infoDictionary![kCFBundleNameKey as String] as? String) ?? ""
}
