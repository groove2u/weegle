//
//  MPBaseViewController.swift
//  MPFramework
//
//  Created by 이인재 on 2015.12.16.
//  Copyright © 2015년 magentapink. All rights reserved.
//

import UIKit

open class MPBaseViewController: UIViewController, MPViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIGestureRecognizerDelegate {

	@IBOutlet open var scrollView: UIScrollView?
	
	open var confirmTitle: String {
		return "확인"
	}
	open var currentTextObject: UIResponder?
    open static var storyboardId: String {
        return String(describing: self)
    }
    
	override open var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	override open func viewDidLoad() {
		super.viewDidLoad()
		
		let gestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer()
		gestureRecognizer.addTarget(self, action: #selector(viewTouched(_:)))
		gestureRecognizer.cancelsTouchesInView = false
		gestureRecognizer.delegate = self
		view.addGestureRecognizer(gestureRecognizer)
		
		prepareUI()
	}
	
	override open func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		registerKeyboardNotifications()
		
		updateUI()
	}
	
	override open func viewWillDisappear(_ animated: Bool) {
		clearKeyboardNotifications()
		
		super.viewWillDisappear(animated)
	}

	// MARK: - MPViewDelegate
	open func prepareUI() {}
	open func updateUI() {}
	open func updateDataUI() {}
	open func fetch() {}

	// MARK: - Getter
	open func viewForScrollView() -> UIScrollView? { return nil }
	
	// MARK: - Keyboard
	open func registerKeyboardNotifications() {
		NotificationCenter.default.addObserver(self,
		                                       selector: #selector(keyboardWillShown(_:)),
		                                       name: NSNotification.Name.UIKeyboardWillShow,
		                                       object: nil)
		NotificationCenter.default.addObserver(self,
		                                       selector: #selector(keyboardWillBeHidden(_:)),
		                                       name: NSNotification.Name.UIKeyboardWillHide,
		                                       object: nil)
	}
	
	open func clearKeyboardNotifications() {
		NotificationCenter.default.removeObserver(self,
		                                          name: NSNotification.Name.UIKeyboardDidShow,
		                                          object: nil)
		NotificationCenter.default.removeObserver(self,
		                                          name: NSNotification.Name.UIKeyboardWillHide,
		                                          object: nil)
	}
	
	@objc open func keyboardWillShown(_ notification: Notification) {
		if let info = (notification as NSNotification).userInfo {
			guard let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
			guard let curve = (info[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber)?.intValue else { return }
			guard let duration = (info[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue else { return }
			
			let inset: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.size.height + 10, right: 0)
			
			UIView.animate(withDuration: duration,
				delay: 0.0,
				options: UIViewAnimationOptions(rawValue: UInt(curve) << 16),
				animations: {
					self.scrollView?.contentInset = inset
					self.scrollView?.scrollIndicatorInsets = inset
				},
				completion: nil)
		} else {
			// no userInfo dictionary present
		}
	}
	
	@objc open func keyboardWillBeHidden(_ notification: Notification) {
		let inset = UIEdgeInsets.zero
		
		scrollView?.contentInset = inset
		scrollView?.scrollIndicatorInsets = inset
	}

	open func textFieldDidBeginEditing(_ textField: UITextField) {
		currentTextObject = textField
	}
	
	open func textViewDidBeginEditing(_ textView: UITextView) {
		currentTextObject = textView
	}
	
	open func makeTableViewZeroMargin(tableView: UITableView, cell: UITableViewCell) {
		tableView.separatorInset = UIEdgeInsets.zero
		tableView.layoutMargins = UIEdgeInsets.zero
		cell.layoutMargins = UIEdgeInsets.zero
	}
	
	open func alert(title: String? = nil, message: String? = nil, confirmTitle: String? = nil, confirm: ((UIAlertAction) -> Void)? = nil) {
		let alert = UIAlertController(title: title?.localized, message: message?.localized, confirmTitle: confirmTitle?.localized ?? self.confirmTitle.localized, confirm: confirm)
		present(alert, animated: true, completion: nil)
	}
	
	@IBAction open func dismiss(_ sender:AnyObject? = nil) {
		if let navigationController = navigationController {
			if navigationController.viewControllers.count == 1 {
				navigationController.dismiss(animated: true, completion: nil)
			} else {
				navigationController.popViewController(animated: true)
			}
		} else {
			dismiss(animated: true, completion: nil)
		}
	}
	
	@IBAction open func dismissWithoutAnimation(_ sender:AnyObject? = nil) {
		if let navigationController = navigationController {
			if navigationController.viewControllers.count == 1 {
				navigationController.dismiss(animated: false, completion: nil)
			} else {
				navigationController.popViewController(animated: false)
			}
		} else {
			dismiss(animated: false, completion: nil)
		}
	}
	
	@objc open func viewTouched(_ sender: UITapGestureRecognizer) {
		view.endEditing(false)
	}
	
	// MARK: - Gesture
	
	open func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
		return (touch.view is UIControl) == false
	}
}
