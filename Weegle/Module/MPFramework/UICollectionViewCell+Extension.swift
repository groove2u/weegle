//
//  UICollectionViewCell+Extension.swift
//  Weegle
//
//  Created by 이인재 on 08/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
	
	static var defaultReuseIdentifier: String {
		return String(describing: self)
	}
}
