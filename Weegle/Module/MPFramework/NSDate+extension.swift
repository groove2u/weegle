//
//  NSDate+extension.swift
//  MPFramework
//
//  Created by 이인재 on 2015.12.16.
//  Copyright © 2015년 magentapink. All rights reserved.
//

import Foundation

extension Date {
	
	@nonobjc public var closedWeekdays: [Date] {
		var days = [Date](repeating: Date(), count: 7)
		
		let daysInWeek = 7
		let oneDayTimeInterval: TimeInterval = 60 * 60 * 24
		let baseDate = addingTimeInterval((-timeIntervalSince1970).truncatingRemainder(dividingBy: 60))
		let calendar = Calendar.current
		let weekdayValue = (calendar as NSCalendar).component(NSCalendar.Unit.weekday, from: baseDate) - 1
		
		for i in 0..<daysInWeek {
			let internalIndex = (baseDate.timeIntervalSince1970 > Date().timeIntervalSince1970 ? 0 : 1) + i
			let date = baseDate.addingTimeInterval(oneDayTimeInterval * TimeInterval(internalIndex))
			var index = weekdayValue + internalIndex
			
			if index >= daysInWeek {
				index -= daysInWeek
			}
			
			days[index] = date
		}
		
//		for i in 0..<daysInWeek {
//			print("### day[\(Weekday(rawValue: i)?.description)]:\(days[i])")
//		}
		
		return days
	}
}
