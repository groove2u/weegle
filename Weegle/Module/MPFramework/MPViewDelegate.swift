//
//  MPViewDelegate.swift
//  MPFramework
//
//  Created by 이인재 on 2015.12.16.
//  Copyright © 2015년 magentapink. All rights reserved.
//

public protocol MPViewDelegate: class {
	
	/// viewDidLoad() 에서 호출, super.prepareUI() 꼭 호출해야 함.
	func prepareUI()
	/// viewWillAppear() 에서 호출, super.updateUI() 꼭 호출해야 함.
	func updateUI()
	/// 데이터 업데이트 후 UI 갱신용.
	func updateDataUI()
	/// 데이터 호출 용.
	func fetch()
}
