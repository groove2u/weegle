//
//  String+extension.swift
//  MPFramework
//
//  Created by 이인재 on 2015.12.16.
//  Copyright © 2015년 magentapink. All rights reserved.
//

import UIKit

extension String {
	
	public var isNumber: Bool {
		return NumberFormatter().number(from: self) != nil
	}
	
	public var localized: String {
		return NSLocalizedString(self, comment: "")
	}
	
	var isBlank: Bool {
		return trimmingCharacters(in: CharacterSet.whitespaces).isEmpty
	}
	
	var isEmail: Bool {
		do {
			let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)

			return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSRange(location: 0, length: self.characters.count)) != nil
		} catch {
			return false
		}
	}
	
	var isAlphanumeric: Bool {
		return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
	}
	
	//validate Password
	var isValidPassword: Bool {
		do {
			let regex = try NSRegularExpression(pattern: "^[a-zA-Z_0-9\\-_,;.:#+*?=!§$%&/()@]+$", options: .caseInsensitive)
			if (regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSRange(location: 0, length: self.characters.count)) != nil) {
				if (self.characters.count >= 6 && self.characters.count <= 20) {
					return true
				} else {
					return false
				}
			} else {
				return false
			}
		} catch {
			return false
		}
	}
	
	public func substringToIndex(_ index: Int) -> String {
		if index > characters.count {
			return self
		} else {
			return String(prefix(upTo: characters.index(characters.startIndex, offsetBy: index)))
		}
	}
	
	public func substringFromIndex(_ index: Int) -> String {
		if index > characters.count {
			return self
		} else {
			return String(suffix(from: characters.index(characters.startIndex, offsetBy: index)))
		}
	}
	
//	public func substringWithRange(_ start: Int, end: Int) -> String {
//		if end - start > characters.count {
//			return self
//		} else {
//			return String(self[start..<end])
//			return substring(with: Range<String.Index>(characters.index(characters.startIndex, offsetBy: start)..<characters.index(characters.startIndex, offsetBy: end)))
//		}
//	}
	
	func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
		let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
		let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
		
		return boundingBox.height
	}
	
	func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
		let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
		let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
		
		return boundingBox.width
	}
}

extension NSAttributedString {
	
	func height(withConstrainedWidth width: CGFloat) -> CGFloat {
		let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
		let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
		
		return boundingBox.height
	}
	
	func width(withConstrainedHeight height: CGFloat) -> CGFloat {
		let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
		let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
		
		return boundingBox.width
	}
}
