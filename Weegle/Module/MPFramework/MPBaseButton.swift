//
//  MPBaseButton.swift
//  MPFramework
//
//  Created by 이인재 on 2017.07.11.
//  Copyright © 2017 magentapink. All rights reserved.
//

import UIKit

open class MPBaseButton: UIButton {

	@IBInspectable open var selectedBackgroundColor: UIColor?
	@IBInspectable open var highlightedBackgroundColor: UIColor?
	@IBInspectable open var disabledBackgroundColor: UIColor?
	@IBInspectable open var selectedBorderColor: UIColor?
	@IBInspectable open var highlightedBorderColor: UIColor?
	@IBInspectable open var disabledBorderColor: UIColor?
	
	private var backgroundColorBuffer: UIColor!
	private var borderColorBuffer: UIColor!
	
	open override var isSelected: Bool {
		didSet {
			prepre()
			
			if selectedBorderColor != nil {
				backgroundColor = isSelected ? selectedBackgroundColor : backgroundColorBuffer
			}
			
			if selectedBorderColor != nil {
				borderColor = isSelected ? selectedBorderColor : borderColorBuffer
			}
		}
	}
	
	open override var isHighlighted: Bool {
		didSet {
			prepre()
			
			if highlightedBackgroundColor != nil {
				backgroundColor = isHighlighted ? highlightedBackgroundColor : backgroundColorBuffer
			}
			
			if highlightedBorderColor != nil {
				borderColor = isHighlighted ? highlightedBorderColor : borderColorBuffer
			}
		}
	}
	
	open override var isEnabled: Bool {
		didSet {
			prepre()
			
			if disabledBackgroundColor != nil {
				backgroundColor = isEnabled ? backgroundColorBuffer : disabledBackgroundColor
			}
			
			if disabledBorderColor != nil {
				borderColor = isEnabled ? borderColorBuffer : disabledBorderColor
			}
		}
	}
	
	open override func awakeFromNib() {
		super.awakeFromNib()
		
		let val = isEnabled
		isEnabled = val
	}
	
	private func prepre() {
		if backgroundColorBuffer == nil {
			backgroundColorBuffer = backgroundColor ?? .white
		}
		
		if borderColorBuffer == nil {
			borderColorBuffer = borderColor
		}
	}
}
