//
//  TextValidator.swift
//  MPFramework
//
//  Created by 이인재 on 2016.11.03.
//  Copyright © 2016 magentapink. All rights reserved.
//

import UIKit

open class TextValidator {
	
	private init() {}
	
	@nonobjc open static func validate(textField: UITextField, title: String?, message: String?, confirmTitle: String) -> String? {
		guard let text = textField.text, text.isEmpty == false else {
			let alert = UIAlertController(title: title, message: message, confirmTitle: confirmTitle, confirm: { (_) in
				textField.becomeFirstResponder()
			})
			
			if let topViewController = UIApplication.topViewController() {
				topViewController.present(alert, animated: true, completion: nil)
			}
			
			return nil
		}
		
		return text
	}
}
