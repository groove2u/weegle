//
//  UIAlertController+extension.swift
//  MPFramework
//
//  Created by 이인재 on 2016.11.03.
//  Copyright © 2016 magentapink. All rights reserved.
//

import UIKit

extension UIAlertController {
	
	@nonobjc public convenience init(title: String?, message: String?, confirmTitle: String, confirm: ((UIAlertAction) -> Void)?) {
		self.init(title: title, message: message, preferredStyle: .alert)
		let confirmAction = UIAlertAction(title: confirmTitle, style: .default, handler: confirm)
		addAction(confirmAction)
	}
}
