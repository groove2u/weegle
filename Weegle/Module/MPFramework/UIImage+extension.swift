//
//  UIImage+extension.swift
//  MPFramework
//
//  Created by 이인재 on 2015.12.19.
//  Copyright © 2015년 magentapink. All rights reserved.
//

import UIKit

extension UIImage {
	
	/**
	이미지 리사이징
	
	- parameter ratio:    변환 비율
	- parameter scale:    해상도 비율 (ex: retina = 2.0), 기본값 0 -> 현재 기기 비율
	- parameter hasAlpha: 투명도 유무
	
	- returns: 리사이즈 된 이미지
	*/
	@nonobjc open func resizedImage(_ ratio: CGFloat, scale: CGFloat = 0, hasAlpha: Bool = false) -> UIImage {
		let scaledSize = size.applying(CGAffineTransform(scaleX: ratio, y: ratio))
		let hasAlpha = false
		
		UIGraphicsBeginImageContextWithOptions(scaledSize, !hasAlpha, scale)
		draw(in: CGRect(origin: CGPoint.zero, size: scaledSize))
		
		let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return scaledImage!
	}

	@nonobjc open func resizedImage(targetWidth: CGFloat, scale: CGFloat = 0, hasAlpha: Bool = false) -> UIImage {
		let ratio = targetWidth / size.width
   		let scaledSize = size.applying(CGAffineTransform(scaleX: ratio, y: ratio))
    	let hasAlpha = false
    	
    	UIGraphicsBeginImageContextWithOptions(scaledSize, !hasAlpha, scale)
    	draw(in: CGRect(origin: CGPoint.zero, size: scaledSize))
    	
    	let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
    	UIGraphicsEndImageContext()
    	
    	return scaledImage!
	}
	
	@nonobjc open func resizedImage(targetHeight: CGFloat, scale: CGFloat = 0, hasAlpha: Bool = false) -> UIImage {
		let ratio = targetHeight / size.height
  		let scaledSize = size.applying(CGAffineTransform(scaleX: ratio, y: ratio))
    	let hasAlpha = false
		
    	UIGraphicsBeginImageContextWithOptions(scaledSize, !hasAlpha, scale)
    	draw(in: CGRect(origin: CGPoint.zero, size: scaledSize))
    	
    	let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
    	UIGraphicsEndImageContext()
    	
    	return scaledImage!
	}
}
