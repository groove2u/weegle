//
//  UIView+extension.swift
//  MPFramework
//
//  Created by 이인재 on 2016.11.03.
//  Copyright © 2016 magentapink. All rights reserved.
//

import UIKit

extension UIView {

	@IBInspectable open var cornerRadius: CGFloat {
		set {
			layer.cornerRadius = newValue
			layer.masksToBounds = newValue > 0
		}
		
		get {
			return layer.cornerRadius
		}
	}
	
	@IBInspectable open var borderColor: UIColor? {
		set {
			layer.borderColor = newValue?.cgColor
		}
		
		get {
			return UIColor(cgColor: layer.borderColor!)
		}
	}
	
	@IBInspectable open var borderWidth: CGFloat {
		set {
			layer.borderWidth = newValue
		}
		
		get {
			return layer.borderWidth
		}
	}
	
	var screenshot: UIImage? {
		UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
		drawHierarchy(in: self.bounds, afterScreenUpdates: true)
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return image
	}
	
	func screenshot(rect: CGRect) -> UIImage? {
		UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
		drawHierarchy(in: rect, afterScreenUpdates: true)
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return image
	}
}
