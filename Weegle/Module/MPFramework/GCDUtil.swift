//
//  GCDUtil.swift
//  MPFramework
//
//  Created by 이인재 on 2015.12.16.
//  Copyright © 2015년 magentapink. All rights reserved.
//

import Foundation

/**
    GCD after second wrapper
    - parameter second : delay second
*/
public func dispatch_after_second(_ second: Double, complete: @escaping () -> Void) {
	let afterQueue = DispatchQueue(label: "after queue", attributes: [])
	let delay = second * Double(NSEC_PER_SEC)
	afterQueue.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC), execute: complete)
}

public func mainAsync(_ complete: @escaping () -> Void) {
	mainQueue.async(execute: complete)
}

public var mainQueue: DispatchQueue {
	return DispatchQueue.main
}

public var userInteractiveQueue: DispatchQueue {
	return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
}

public var userInitiatedQueue: DispatchQueue {
	return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated)
}

public var utilityQueue: DispatchQueue {
	return DispatchQueue.global(qos: DispatchQoS.QoSClass.utility)
}

public var backgroundQueue: DispatchQueue {
	return DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
}
