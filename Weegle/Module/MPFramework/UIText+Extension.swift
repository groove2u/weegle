//
//  UIText+Extension.swift
//  Weegle
//
//  Created by 이인재 on 03/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

extension UITextField {

	@nonobjc func validate(title: String?, message: String?, confirmTitle: String) -> String? {
		guard let text = self.text, text.isEmpty == false else {
			let alert = UIAlertController(title: title, message: message, confirmTitle: confirmTitle, confirm: { [weak self] (_) in
				self?.becomeFirstResponder()
			})
			
			if let topViewController = UIApplication.topViewController() {
				topViewController.present(alert, animated: true, completion: nil)
			}
			
			return nil
		}
		
		return text
	}

}
