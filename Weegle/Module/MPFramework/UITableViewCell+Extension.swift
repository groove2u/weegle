//
//  UITableViewCell+Extension.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

extension UITableViewCell {

	@nonobjc static var defaultReuseIdentifier: String {
		return String(describing: self)
	}
}
