#import <UIKit/UIKit.h>

//! Project version number for TvsLib.
FOUNDATION_EXPORT double TvsLibVersionNumber;

//! Project version string for TvsLib.
FOUNDATION_EXPORT const unsigned char TvsLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TvsLib/PublicHeader.h>

#import	"tvsmodule_bridge.h"
