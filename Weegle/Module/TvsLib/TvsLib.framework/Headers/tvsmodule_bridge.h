#ifndef tvsmodule_bridge_h
#define tvsmodule_bridge_h

#include	<stdint.h>
#include	<CoreMedia/CMSampleBuffer.h>
#include	<CoreVideo/CoreVideo.h>

#ifdef	__cplusplus
extern "C" {
#endif
	static const int	cMaxRemotes = 8;
	static const int	cMaxCams = 2;
	static const int	cCamLayer = cMaxRemotes;
	static const int	cMikeChannel = 256;
	
	static const int	cInfoNoreStart = 1, cInfoNoreStop = 2;

	static const int	SendAudio = 1, SendVideo = 2;
	static const uint32_t	DJVideo = 0x0001, DJAudio = 0x0002, EmitorVideo = 0x0004, EmitorAudio = 0x0008, OwnerVideo = 0x0010, OwnerAudio = 0x0020,
				UserVideo = 0x0040, UserAudio = 0x0080, SendClient = 0x1000, SendMobile = 0x2000, SendPublic = 0x4000, SendMyself = 0x8000,
				HiStream = 0x00010000, LoStream = 0x00020000, SecondVideoStream = 0x00040000, BoardVideoStream = 0x00080000, CaptionVideoStream = 0x00100000;

	static const int eLine0 = 1, eLine1 = 2, eTitle = 3, eWriter = 4, eComposer = 5, eSinger = 6, eMaxNoreLines = 7;

	typedef struct NoreLineBitmap {
		const wchar_t	*m_str;			// in render string
		const int 	m_arrlen;			// in
		const int		*m_indexarr;	// in
		float				*m_posarr;		// out
		int					m_width, m_height, m_stride;	// out
		uint8_t			*m_bitmap;		// out
	} NoreLineBitmap;
	typedef	void	(NoreLineBitmapCallback)(NoreLineBitmap *nlbitmap);
	typedef	void	(InfoCallback)(const void *obj, int cmd, int result, const char *desc);
	typedef	void	(RecorderCallback)(const void *obj, CMSampleBufferRef sample, int options);
	typedef	void	(LayerCallback)(const void *obj, int layer, int width, int height);
	
	typedef struct TVSCamSize {
		int		width, height;
	} TVSCamSize;
	
	extern int	sDefaultSamplerate;
	
	void*	nCreateModule();		// May be add callback interface for text bitmap generation
	void	nSetModuleCallbacks(void *module, const void *obj, NoreLineBitmapCallback noreCallback, InfoCallback infoCallback, RecorderCallback recCallback, LayerCallback layerCallback);
	void	nDestroyModule(void *module);
	void	nSetGlSurface(void *module, CVEAGLContext eaglcontext);

	// Camera management
	//void*	nCamGet(void *module, int orientaion, int place);	// place -1 - any
	//void	nCamDestroy(void *cam);
	//void	nCamStart(void *cam);
	//void	nCamStop(void *cam);
	//void	nCamSetSize(void *cam, int width, int height);
	//TVSCamSize	nCamGetSize(void *cam);
	//int		nCamGetAvailableSizes(void *cam, TVSCamSize sizes[], int arrsize);
	//int		nCamGetState(void *cam);
	void		nDrawCam(void *module, int place, CMSampleBufferRef sample);
	void		nProcessCam(void *module, int place, CMSampleBufferRef sample);

	
	// Stream server connection
	void	nSetStreamServer(void *module, const char *serverIp, int serverPort);
	void	nLogout(void *module);
	void	nLogin(void *module, const char *userId);
	void	nJoin(void *module, uint64_t roomId, uint32_t roomMask, int subscribers, uint64_t userIds[], uint32_t userMasks[]);
	void	nAssignChannelMask(void *module, uint64_t userId, uint32_t userMask, int channel);
	void	nClearChannelMask(void *module, uint64_t userId, uint32_t userMask);
	void	nClearChannelPos(void *module, int channel);
	void	nClearChannels(void *module);
	void	nSetSendMask(void *module, uint32_t videomask, uint32_t audiomask);
	void	nDoSend(void *module, int mode);
	
	// Media management
	//void	nSetVideoEncoderSize(void *module, int width, int height);
	void	nSetSendBitrate(void *module, int vbitrate, int vmode, int abitrate, int amode);
	void	nSetMicVolume(void *module, float volume);
	void	nSetSongVolume(void *module, float volume);
	void	nSetSpeakerVolume(void *module, float volume);
	void	nStartAudioCapture(void *module);
	void	nStopAudioCapture(void *module);
	void	nEnablePlayChannel(void *module, int channel, bool enabled);
	void	nSetPitchTempo(void *module, int pitch, float tempo);
	void	nSetEchoVolume(void *module, float volume);
	
	// Drawing
 	void	nDoGlDraw(void *module, CGRect prect);

	void	nSetAnimationTime(void *module, int timeMs);
	//void	nSetCamRotation(void *module, int rotation);
	void	nSetLayerMode(void *module, int layer, int mode);
	void	nSetStretchMode(void *module, int layer, int mode);
	void	nSetMirrorMode(void *module, int layer, int mode);
	//void	nSetLayerBitmap(void *module, int layer, ???);
	void	nSetRemoteLayer(void *module, int layer, float x, float y, float z, float width, float height, float rotation);
	void	nSetLocalLayer(void *module, float x, float y, float z, float width, float height, float rotation);
	void	nSetLayerImage(void *module, int layer, CVPixelBufferRef imageRef);
	
	// Karaoke
	void	nNorePlaySong(void *module, const void *songData, int songSize, int songType, const void *lyricData, int lyricSize, int lyricType);
	void	nNoreStop(void *module);
	void	nSetLyricPos(void *module, int target, float x, float y, float dx, float dy);
	void	nNoreSetLyricColor(void *module, float r, float g, float b, float a);
	
	// Recording
	void	nSetRecordingPath(void *module, const char *path);
	void	nStartRecording(void *module, const char *filename, int mode);
	void	nStopRecording(void *module);

	// Test callbacks
	void	nTestCallback(void *module, void (callback)(const char *a));

	// Helper functions
	int		nConnectSocket(const char *addr, int port);
#ifdef	__cplusplus
}
#endif


#endif /* tvsmodule_bridge_h */
