//
//  WGSearchField.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.16.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGSearchField: WGTextField {

	private let leftPaddingView: UIView
	private let findIconImageView: UIImageView
	
	required init?(coder aDecoder: NSCoder) {
		leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 33, height: 1))
		findIconImageView = UIImageView(image: #imageLiteral(resourceName: "findFriendIcon"))
		
		super.init(coder: aDecoder)
		
		leftPaddingView.addSubview(findIconImageView)
		
		leftView = leftPaddingView
		leftViewMode = .always
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		leftPaddingView.frame = CGRect(x: 0, y: 0, width: 33, height: bounds.height)
		
		var frame = findIconImageView.frame
		frame.origin.x = (leftPaddingView.frame.width - frame.width) / 2.0
		frame.origin.y = (leftPaddingView.frame.height - frame.height) / 2.0
		findIconImageView.frame = frame
	}
}
