//
//  WGRedButton.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.16.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGRedButton: WGButton {

	override func prepareUI() {
		backgroundColor = WGColor.baseColor(for: 17)
		highlightedBackgroundColor = WGColor.baseColor(for: 19)
		titleLabel?.font = UIFont.systemFont(ofSize: 15)
		setTitleColor(WGColor.baseColor(for: 8), for: .normal)
		setTitleColor(WGColor.baseColor(for: 8), for: .highlighted)
	}
}
