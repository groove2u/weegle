//
//  WGPCCellView.swift
//  Weegle
//
//  Created by 이인재 on 14/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGPCCellView: UIView {

	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var logoutButton: MPBaseButton!
}
