//
//  WGSubButton.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGSubButton: WGButton {

	override func prepareUI() {
		backgroundColor = WGColor.baseColor(for: 10)
		highlightedBackgroundColor = WGColor.baseColor(for: 12)
		titleLabel?.font = UIFont.systemFont(ofSize: 17)
		setTitleColor(WGColor.baseColor(for: 8), for: .normal)
		setTitleColor(WGColor.baseColor(for: 8), for: .highlighted)
	}
}
