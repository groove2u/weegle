//
//  WGSubMiniButton.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGSubMiniButton: WGSubButton {

	override func prepareUI() {
		super.prepareUI()
		
		titleLabel?.font = UIFont.systemFont(ofSize: 15)
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		cornerRadius = 4
	}
}
