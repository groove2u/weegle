//
//  WGDropdownButton.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGDropdownButton: WGOptionButton {

	let iconImageView = UIImageView(image: #imageLiteral(resourceName: "commonDropdownIcon"))
	
	override func prepareUI() {
		super.prepareUI()
		
		titleLabel?.font = UIFont.systemFont(ofSize: 15)
		contentEdgeInsets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 25)
		
		addSubview(iconImageView)
		
		iconImageView.translatesAutoresizingMaskIntoConstraints = false
		iconImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
		iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		cornerRadius = 4
	}
}
