//
//  WGRoundView.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.29.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGRoundView: UIView {

	override func layoutSubviews() {
		super.layoutSubviews()

		cornerRadius = floor(bounds.height / 2.0)
	}
}
