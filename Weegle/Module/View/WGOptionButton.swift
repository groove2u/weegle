//
//  WGOptionButton.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGOptionButton: WGButton {

	override func prepareUI() {
		backgroundColor = WGColor.baseColor(for: 8)
		highlightedBackgroundColor = WGColor.baseColor(for: 5)
		titleLabel?.font = UIFont.systemFont(ofSize: 17)
		setTitleColor(WGColor.baseColor(for: 10), for: .normal)
		setTitleColor(WGColor.baseColor(for: 10), for: .highlighted)
		borderColor = WGColor.baseColor(for: 10)
		borderWidth = 1
	}
}
