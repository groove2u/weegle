//
//  WGButton.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

enum WGButtonType: String {
	case main
	case sub1
	case sub2
}

class WGButton: MPBaseButton {
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		prepareUI()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		prepareUI()
	}
	
	func prepareUI() {
		
	}
	
	override func layoutSubviews() {
		cornerRadius = bounds.height / 2.0
		
		super.layoutSubviews()
	}
}
