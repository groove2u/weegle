//
//  WGProfileImageView.swift
//  Weegle
//
//  Created by 이인재 on 23/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGProfileImageView: UIImageView {

	override func layoutSubviews() {
		super.layoutSubviews()
		
		cornerRadius = bounds.width / 2.0
		borderWidth = 0.5
		borderColor = UIColor(hex: 0xe0e0e0)
	}
}
