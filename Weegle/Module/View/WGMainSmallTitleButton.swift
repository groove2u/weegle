//
//  WGMainSmallTitleButton.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.16.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGMainSmallTitleButton: WGMainButton {

	override func prepareUI() {
		super.prepareUI()

		titleLabel?.font = UIFont.systemFont(ofSize: 15)
	}
}
