//
//  WGChatBubbleView.swift
//  Weegle
//
//  Created by 이인재 on 01/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGChatBubbleView: UIView {
	
	var chatType: ChatExternType = .normal
	
	override func layoutSubviews() {
		super.layoutSubviews()

		let cRadius = floor(bounds.height / 2.0)
		
		if chatType == .normal {
			if cRadius >= 22 {
				cornerRadius = 22
			} else {
				cornerRadius = cRadius
			}
		} else {
			cornerRadius = 5
		}
	}
}
