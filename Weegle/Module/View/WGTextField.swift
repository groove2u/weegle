//
//  WGTextField.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.13.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGTextField: UITextField {

	private let defaultBorderColor = WGColor.baseColor(for: 4)
	private let highlightedBorderColor = WGColor.baseColor(for: 1)
	private let disabledBackgroundColor = WGColor.baseColor(for: 5)

	override var isEnabled: Bool {
		didSet {
			backgroundColor = isEnabled ? .white : disabledBackgroundColor
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 1))
		let rightPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 1))
		
		leftView = leftPaddingView
		rightView = rightPaddingView
		leftViewMode = .always
		rightViewMode = .always
		
		cornerRadius = 4
		borderColor = defaultBorderColor
		borderWidth = 1
		
		textColor = WGColor.baseColor(for: 1)
	}

	@discardableResult
	override func becomeFirstResponder() -> Bool {
		borderColor = highlightedBorderColor
		
		return super.becomeFirstResponder()
	}
	
	@discardableResult
	override func resignFirstResponder() -> Bool {
		borderColor = defaultBorderColor
		
		return super.resignFirstResponder()
	}
}
