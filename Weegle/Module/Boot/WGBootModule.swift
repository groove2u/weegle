//
//  WGBootModule.swift
//  Weegle
//
//  Created by 이인재 on 12/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

protocol WGBootProtocol {
	
	static func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)
}

class WGBootModule: WGBootProtocol {

	static private var modules: [WGBootProtocol.Type] {
		return [WGLoggerBootModule.self, WGRemotePushBootModule.self, WGFacebookBootModule.self, WGGoogleBootModule.self, WGTVSoriBootModule.self]
	}
	
	private init() {}

	static func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
		for module in modules {
			module.application(application, didFinishLaunchingWithOptions: launchOptions)
		}
	}
}
