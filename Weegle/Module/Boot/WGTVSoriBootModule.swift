//
//  WGTVSoriBootModule.swift
//  Weegle
//
//  Created by 이인재 on 09/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGTVSoriBootModule: WGBootProtocol {

	static func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
		TvsoriPreviewController.initTvsoriSDK(baseLiveSocketPath, baseLiveSocketPort, 2048000, 64000)
	}
}
