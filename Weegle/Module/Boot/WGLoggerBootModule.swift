//
//  WGLoggerBootModule.swift
//  Weegle
//
//  Created by 이인재 on 12/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import CleanroomLogger

class WGLoggerBootModule: WGBootProtocol {

	static func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
		#if DEBUG
			enableLogger()
		#else
			Log.neverEnable()
		#endif
	}
	
	static private func enableLogger() {
		let formatter = FieldBasedLogFormatter(fields: [
			.timestamp(.custom("HH:mm:ss.SSS")),
			.literal(" | "),
			.callSite,
			.literal(" "),
			.severity(SeverityStyle.custom(textRepresentation: .colorCoded, truncateAtWidth: nil, padToWidth: 1, rightAlign: true)),
			.literal(" : "),
			.payload
			])
		let recorder = StandardStreamsLogRecorder(formatters: [formatter])
		
		Log.enable(configuration: BasicLogConfiguration(recorders: [recorder]))
	}
}
