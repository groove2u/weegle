//
//  WGRemotePushBootModule.swift
//  Weegle
//
//  Created by 이인재 on 12/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import UserNotifications
import CleanroomLogger

class WGRemotePushBootModule: WGBootProtocol {
	
	static func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
		WGDeviceService.registerRemotePush()
	}
}
