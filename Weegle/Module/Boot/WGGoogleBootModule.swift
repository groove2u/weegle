//
//  WGGoogleBootModule.swift
//  Weegle
//
//  Created by 이인재 on 12/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import GoogleSignIn
import GoogleMaps

class WGGoogleBootModule: WGBootProtocol {

	static func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
		GIDSignIn.sharedInstance().clientID = "540186143401-d3hapo9682aqoj72p3dkejcdqs1sb6i7.apps.googleusercontent.com"
		GMSServices.provideAPIKey("AIzaSyA0FHQVVTX-i_vi24XYwpgt54WqDLgU1IU")
	}
}
