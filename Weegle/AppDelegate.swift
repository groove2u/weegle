//
//  AppDelegate.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.10.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import GoogleSignIn
import UserNotifications
import TvsLib
import CleanroomLogger

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	static var sTvsClient : ITvsClient?
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		WGBootModule.application(application, didFinishLaunchingWithOptions: launchOptions)
		
		WGDeviceService.checkFirstRun()
		registerNotifications()
		FirebaseApp.configure()
		
		checkUpdate()
		
		return true
	}
	
	func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
		let facebookHandled = FBSDKApplicationDelegate.sharedInstance().application(app,
		                                                                            open: url,
		                                                                            sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
		                                                                            annotation: options[UIApplicationOpenURLOptionsKey.annotation])

		let googleHandled = GIDSignIn.sharedInstance().handle(url,
		                                                      sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
		                                                      annotation: options[UIApplicationOpenURLOptionsKey.annotation])
		
		return facebookHandled || googleHandled
	}
	
	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		WGChatService.leaveCurrent()
		WGChatService.disconnect()
		TvsoriPreviewController.handleDidEnterBackground()
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
		WGDeviceService.updatePushToken()
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		WGChatService.connect()
		FBSDKAppEvents.activateApp()
		TvsoriPreviewController.handleDidBecomeActive()
	}

	func applicationWillTerminate(_ application: UIApplication) {
		TvsoriPreviewController.destroyTvsoriSDK()
	}
}

// MARK: - Prepare

extension AppDelegate {
	
	/// 로그인/아웃 Notification
	fileprivate func registerNotifications() {
		NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotificaiton), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(didLogIn), name: WGUserService.loginCompleteNotification.name, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(didLogOut), name: WGUserService.logoutCompleteNotification.name, object: nil)
	}
	
	/// 버전 체크
	private func checkUpdate() {
		WGAppService.version(success: { (json) in
			if let appVersion = Int(UIApplication.appVersion.replacingOccurrences(of: ".", with: "")),
				let lastestVersion = Int(json["iphone"].stringValue.replacingOccurrences(of: ".", with: "")) {
				let isUpdateAvailable = appVersion < lastestVersion
				var isForceUpdate = json["iphoneFlag"].boolValue

				// FIXME: 테스트용 강제 업데이트 방지
				isForceUpdate = false
				
				if isUpdateAvailable {
					let alert = UIAlertController(title: nil, message: "새로운 버전이 출시되었습니다.\n업데이트 하시겠습니까?", preferredStyle: .alert)
					
					alert.addAction(UIAlertAction(title: "업데이트", style: .cancel, handler: { (_) in
						if let url = URL(string: "https://itunes.apple.com/app/id1266689031") {
							UIApplication.shared.openURL(url)
						}
					}))
					alert.addAction(UIAlertAction(title: isForceUpdate ? "종료" : "cancel".localized, style: isForceUpdate ? .destructive : .default, handler: { (_) in
						if isForceUpdate {
							exit(0)
						} else {
							if InstanceID.instanceID().token() != nil {
								self.checkLoginStatus()
							}
						}
					}))
					
					self.window?.rootViewController?.present(alert, animated: true)
				} else {
					if InstanceID.instanceID().token() != nil {
						self.checkLoginStatus()
					}
				}
			} else {
				self.checkLoginStatus()
			}
		}) { (error) in
			if InstanceID.instanceID().token() != nil {
				self.checkLoginStatus()
			}
			
			Log.error?.value(error)
		}
	}
	
	/// 로그인 체크
	fileprivate func checkLoginStatus() {
		if WGUserService.isLogin {
			WGUserService.refreshToken(success: {
				Log.info?.trace()
				WGDeviceService.updatePushToken()
			}, failure: { [weak self] (error) in
				if let msg = error?.userInfo["message"] as? String {
					Log.error?.value(msg)
					
					self?.didLoginStatusChanged(status: .logOut)
				}
			})
		} else {
			didLoginStatusChanged(status: .logOut)
		}
	}
}

// MARK: - Login

extension AppDelegate: WGLoginDelegate {
	
	@objc fileprivate func didLogIn() {
		WGChatService.connect()
		
		WGDeviceService.updatePushToken()
		
		didLoginStatusChanged(status: .logIn)
	}
	
	@objc fileprivate func didLogOut() {
		WGChatService.disconnect()
		
		didLoginStatusChanged(status: .logOut)
	}
	
	func didLoginStatusChanged(status: WGLoginStatus) {
		window?.rootViewController = UIStoryboard(name: status == .logIn ? "Main" : "Login", bundle: nil).instantiateInitialViewController()
	}
}

// MARK: - APNS

extension AppDelegate: UNUserNotificationCenterDelegate {

	@objc fileprivate func tokenRefreshNotificaiton(notification: Notification) {
		checkLoginStatus()
	}
	
	func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
		Log.info?.message("### PUSH received\n\(userInfo)")
		
		if let aps = userInfo["aps"] as? [String : String], let chatId = userInfo["gcm.notification.chatId"] as? String {
			if let msg = aps["alert"] {
				let confirm = UIAlertAction(title: "확인", style: .default) { (_) in
					NotificationCenter.default.post(name: WGChatService.enterChatNotification.name, object: nil, userInfo: ["chatId": chatId])
				}
				
				let alert = UIAlertController(title: nil,
				                              message: msg,
				                              preferredStyle: .alert)
				
				alert.addAction(confirm)
				
				window?.rootViewController?.present(alert, animated: true, completion: nil)
			}
		}
	}
}
