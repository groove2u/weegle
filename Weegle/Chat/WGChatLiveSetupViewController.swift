//
//  WGChatLiveSetupViewController.swift
//  Weegle
//
//  Created by 이인재 on 22/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import SwiftyJSON
import CleanroomLogger

class WGChatLiveSetupViewController: WGBaseViewController {

	@IBOutlet weak private var backgroundImageView: UIImageView!
	@IBOutlet weak private var beginLiveButton: UIButton!
	@IBOutlet weak private var noticeStackView: UIStackView!
	@IBOutlet weak private var countNoticeLabel: UILabel!
	@IBOutlet weak private var countImageView: UIImageView!
	@IBOutlet weak private var countLabel: UILabel!
	
	var backgroundImage: UIImage?
	var invitedFriendObjectIds = [String]()
	var chatId: String!
	
	private var timer: Timer?
	
	override var prefersStatusBarHidden: Bool {
		return true
	}
	
	override func prepareUI() {
		super.prepareUI()
		
		navigationController?.setNavigationBarHidden(true, animated: true)
		
		backgroundImageView.image = backgroundImage
		
		updateStatus(isCount: false)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

	}
	
	// MARK: - Action
	
	private func updateStatus(isCount: Bool) {
		countNoticeLabel.isHidden = isCount == false
		countLabel.isHidden = isCount == false
		countImageView.isHidden = isCount == false
		
		beginLiveButton.isHidden = isCount
		noticeStackView.isHidden = isCount
	}
	
	@IBAction func complete() {
		updateStatus(isCount: true)
		
		countLabel.text = "3"
		
		timer?.invalidate()
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
			self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer(timer:)), userInfo: nil, repeats: true)
		}
	}
	
	@objc private func updateTimer(timer: Timer) {
//		guard let image = backgroundImage else { return }
		
		if var time = Int(countLabel.text!) {
			if time > 0 {
				time -= 1
				
				countLabel.text = "\(time)"
			} else {
				self.timer?.invalidate()
				
				WGChatService.openLive(chatId: chatId, type: .live, success: { (json) in
					if let viewController = self.storyboard?.instantiateViewController(withIdentifier: String(describing: BroadcastViewController.self)) as? BroadcastViewController {
						viewController.mRoomId = json["roomId"].int64Value
						viewController.mUserId = WGUserService.current?.memberId
						viewController.mMainUserId = WGUserService.current?.memberId
						viewController.chatInfo = json
						viewController.isBJ = true
						viewController.isPublic = true
						
						self.dismiss(animated: false, completion: { [weak presenter = self.presentingViewController] in
							DispatchQueue.main.async {
								self.updateStatus(isCount: false)
								self.countLabel.text = "3"
								
								presenter?.present(viewController, animated: true)
							}
						})
					}
				}, failure: { (_) in
					
				})
			}
		}
	}
	
	override func dismiss(_ sender: AnyObject?) {
		timer?.invalidate()
		
		super.dismiss()
	}
}
