//
//  WGChatViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.23.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import RealmSwift
import MobileCoreServices
import SnapKit
import SwiftyJSON
import AVKit
import ContactsUI
import CleanroomLogger

class WGChatViewController: WGBaseViewController, UITableViewDelegate, UITableViewDataSource,
ListItemInteractionProtocol, CNContactViewControllerDelegate, DisplayContentProtocol {

	@IBOutlet weak private var tableView: UITableView!
	@IBOutlet weak private var noConversationNoticeView: UIView!
	@IBOutlet weak private var contentInputBottomConstraint: NSLayoutConstraint!
	@IBOutlet weak private var conversationTextView: UITextView!
	@IBOutlet weak private var conversationHeightConstraint: NSLayoutConstraint!
	@IBOutlet weak private var externViewBottomConstraint: NSLayoutConstraint!
	@IBOutlet weak private var keyboardTypeButton: UIButton!
	
	var chatId: String!
	
	private let externViewHiddenY: CGFloat = -226
	
	private var chat: WGChat!
	private var notificationToken: NotificationToken?
	private var isKeyboardShown: Bool {
		return contentInputBottomConstraint.constant != 0 || isExternalKeyboardShown
	}
	private var isExternalKeyboardShown: Bool {
		return externViewBottomConstraint.constant == 0
	}
	
	override func prepareUI() {
		super.prepareUI()
		
		NotificationCenter.default.addObserver(self, selector: #selector(enter), name: WGChatService.connectedNotification.name, object: nil)
		
		conversationTextView.textContainerInset = UIEdgeInsets(top: 8, left: 5, bottom: 8, right: 5)
		
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.estimatedRowHeight = 32
		tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
		
		let style = NSMutableParagraphStyle()
		conversationTextView.attributedText = NSAttributedString(string: "", attributes: [NSAttributedStringKey.paragraphStyle: style])
		
		chat = WGChat.chat(id: chatId)
		
		WGChatService.messageList(chatId: chatId, lastDate: chat.items.isEmpty ? "0" : chat.lastDate)
		
		notificationToken = chat.addNotificationBlock({ [weak self] (changes) in
			Log.info?.message("change: \(changes)")
			
			self?.updateDataUI()
		})
		
		if chat.title.isEmpty {
			title = chat.otherNames
		} else {
			title = chat.title
		}
	}
	
	deinit {
		notificationToken?.stop()
	}
	
	override func updateUI() {
		super.updateUI()
		
		if chat.title.isEmpty {
			title = chat.otherNames + " \(chat.chatMemberInfo.count)"
		} else {
			title = chat.title + " \(chat.chatMemberInfo.count)"
		}
		
		enter()
	}
	
	override func updateDataUI() {
		guard chat.isInvalidated == false else { return }
		
		tableView.reloadData()
		
		if chat.items.isEmpty == false {
			tableView.scrollToRow(at: IndexPath(item: chat.items.count - 1, section: 1), at: .bottom, animated: false)
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "ShowSideMenu", let viewController = segue.destination as? WGChatSideViewController {
			viewController.chatId = chatId
			viewController.delegate = self
			
			view.endEditing(false)
		} else if segue.identifier == "ImageSegue", let viewController = segue.destination as? WGImageViewController {
			if let url = sender as? URL {
				viewController.imageURL = url
			}
		} else if segue.identifier == "VideoSegue", let viewController = segue.destination as? AVPlayerViewController {
			if let url = sender as? URL {
				viewController.player = AVPlayer(url: url)
				viewController.navigationController?.setNavigationBarHidden(false, animated: true)
			}
		} else if segue.identifier == "EditChatTitleSegue", let viewController = segue.destination as? WGChatEditTitleViewController {
			viewController.chatId = chatId
		}
	}
	
	@objc func enter() {
		WGChatService.enter(chatId: chatId, complete: {
			
		})
	}

	// MARK: - DisplayContentProtocol
	
	func showContent(_ image: UIImage) {
		if let viewController = storyboard?.instantiateViewController(withIdentifier: String(describing: WGImageViewController.self)) as? WGImageViewController {
			viewController.image = image
			
			navigationController?.pushViewController(viewController, animated: true)
		}
	}
	
	func showContent(_ videoURL: URL) {
		
	}
	
	// MARK: - UITableViewDelegate, dataSource
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 2
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard chat.isInvalidated == false else { return 0 }
		
		if section == 0 { return 1 }

		return chat.items.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if indexPath.section == 0 {
			guard let cell = tableView.dequeueReusableCell(withIdentifier: WGChatStatusTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGChatStatusTableViewCell else {
				return UITableViewCell()
			}

			cell.contentLabel.text = chat.lastDateString
			
			return cell
		}
		
		guard indexPath.item < chat.items.count else { return UITableViewCell() }
		let chatItem = chat.items[indexPath.item]
		
		if chatItem.externType == ChatExternType.image.rawValue || chatItem.externType == ChatExternType.video.rawValue {
			guard let cell = tableView.dequeueReusableCell(withIdentifier: chatItem.user?.isMe == true ?
				WGChatImageTableViewCell.defaultReuseIdentifier : "WGChatOtherImageTableViewCell", for: indexPath) as? WGChatImageTableViewCell else {
				return UITableViewCell()
			}
			
			cell.delegate = self
			cell.present(chatItem: chatItem)
			
			return cell
		} else if chatItem.externType == ChatExternType.contact.rawValue {
			guard let cell = tableView.dequeueReusableCell(withIdentifier: chatItem.user?.isMe == true ?
				WGChatContactTableViewCell.defaultReuseIdentifier : "WGChatOtherContactTableViewCell", for: indexPath) as? WGChatContactTableViewCell else {
					return UITableViewCell()
			}
			
			cell.delegate = self
			cell.present(chatItem: chatItem)
			
			return cell
		} else if chatItem.externType == ChatExternType.live.rawValue {
			guard let cell = tableView.dequeueReusableCell(withIdentifier: WGChatLiveTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGChatLiveTableViewCell else {
					return UITableViewCell()
			}
			
			cell.delegate = self
			cell.present(chatItem: chatItem)
			
			return cell
		} else if chatItem.externType == ChatExternType.location.rawValue {
			guard let cell = tableView.dequeueReusableCell(withIdentifier: chatItem.user?.isMe == true ?
				WGChatMapTableViewCell.defaultReuseIdentifier : "WGChatOtherMapTableViewCell", for: indexPath) as? WGChatMapTableViewCell else {
					return UITableViewCell()
			}
			
			cell.delegate = self
			cell.present(chatItem: chatItem)
			
			return cell
		} else if chatItem.externType == ChatExternType.invite.rawValue {
			guard let cell = tableView.dequeueReusableCell(withIdentifier: WGChatStatusTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGChatStatusTableViewCell else {
				return UITableViewCell()
			}
			guard let current = WGUserService.current else { return UITableViewCell() }
			
			let json = JSON(data: chatItem.message.data(using: .utf8, allowLossyConversion: false)!)
			let users = json.arrayValue.map { WGUser.user(objectId: $0.stringValue)?.memberName ?? "" }.joined(separator: ", ")
			let buffer = "\(chat.shortTimeString ?? "")\n\(current.memberName)님이 \(users)님을 초대했습니다."
			
			cell.contentLabel.text = buffer
			
			return cell
		} else {
			guard let cell = tableView.dequeueReusableCell(withIdentifier: chatItem.user?.isMe == true ?
				WGChatTextTableViewCell.defaultReuseIdentifier : "WGChatOtherTextTableViewCell", for: indexPath) as? WGChatTextTableViewCell else {
				return UITableViewCell()
			}
			
			cell.present(chatItem: chatItem)
			
			return cell
		}
	}

	// MARK: - ListItemInteractionProtocol
	
	func listItem(cell: UITableViewCell, didTouched: ListItemInteractionEvent) {
		guard let indexPath = tableView.indexPath(for: cell) else { return }
		guard indexPath.item < chat.items.count else { return }
		
		let chatItem = chat.items[indexPath.item]
		
		view.endEditing(true)
		
		switch chatItem.externType {
		case ChatExternType.image.rawValue:
			if let data = chatItem.extern.data(using: .utf8) {
				let externJSON = JSON(data: data)
				let urlString = externJSON["imagePath"].stringValue
				
				performSegue(withIdentifier: "ImageSegue", sender: WGNetworkService.resourceURL(uri: urlString))
			}
		case ChatExternType.video.rawValue:
			if let data = chatItem.extern.data(using: .utf8) {
				let externJSON = JSON(data: data)
				let urlString = externJSON["videoPath"].stringValue
				
				performSegue(withIdentifier: "VideoSegue", sender: WGNetworkService.resourceURL(uri: urlString))
			}
		case ChatExternType.contact.rawValue:
			if let data = chatItem.extern.data(using: .utf8) {
				let externJSON = JSON(data: data)
				let name = externJSON["name"].stringValue
				let phone = externJSON["number"].stringValue
				
				let contact = CNMutableContact()
				let phoneValue = CNLabeledValue(label: CNLabelPhoneNumberMobile, value: CNPhoneNumber(stringValue: phone))
				contact.phoneNumbers = [phoneValue]
				contact.givenName = name
				
				let controller = chatItem.user?.isMe == true ? CNContactViewController(for: contact) : CNContactViewController(forNewContact: contact)
				controller.contactStore = CNContactStore()
				controller.delegate = self
				controller.view.backgroundColor = WGColor.baseColor(for: 9)
				
				navigationController?.setNavigationBarHidden(false, animated: true)
				navigationController?.pushViewController(controller, animated: true)
			}
		default: break
		}
	}
	
	func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
		navigationController?.popViewController(animated: true)
	}
	
	// MARK: - Action
	
	@IBAction func send() {
		guard let message = conversationTextView.text, message.isEmpty == false else { return }
		guard let currentUser = WGUserService.current else { return }
		
		conversationTextView.text = nil
		conversationHeightConstraint.constant = 36
		
		view.layoutIfNeeded()
		
		let chatItem = WGChatItem()
		chatItem.message = message
		chatItem.writerObjectId = currentUser.objectId
		chatItem.messageTempId = UUID().uuidString
		chatItem.extern = ""
		chatItem.externType = 1

		WGChatService.send(chatId: chatId, chatItem: chatItem)
	}
	
	@IBAction func sendExtern(_ sender: UIButton) {
		switch sender.tag {
		case 0:	//이미지
			selectImage(mediaType: kUTTypeImage as String)
		case 1:	//동영상
			selectImage(mediaType: kUTTypeMovie as String)
		case 2:	//카메라
			camera()
		case 3:	//지도
			performSegue(withIdentifier: "MapSegue", sender: nil)
		case 4:	//방송하기
			live()
		case 5:	//연락처 공유
			contact()
		default: break
		}
	}
	
	@IBAction func toggleKeyboard(_ sender: UIButton) {
		sender.isSelected = !sender.isSelected
		
		toggleExternalKeyboard(shouldShow: sender.isSelected)
	}
	
	private func toggleExternalKeyboard(shouldShow: Bool) {
		if isKeyboardShown {
			if shouldShow {
				UIView.setAnimationsEnabled(false)
				view.endEditing(true)
				UIView.setAnimationsEnabled(true)
				
				externViewBottomConstraint.constant = 0
				view.layoutIfNeeded()
			} else {
				UIView.setAnimationsEnabled(false)
				conversationTextView.becomeFirstResponder()
				UIView.setAnimationsEnabled(true)
				
				externViewBottomConstraint.constant = externViewHiddenY
				view.layoutIfNeeded()
			}
		} else {
			if shouldShow {
				UIView.animate(withDuration: WGDeviceService.keyboardAnimationDuration,
				               delay: 0,
				               options: UIViewAnimationOptions(rawValue: UInt(WGDeviceService.keyboardAnimationCurve) << 16),
				               animations: {
								self.externViewBottomConstraint.constant = 0
								self.view.layoutIfNeeded()
				}, completion: nil)
			} else {
				conversationTextView.becomeFirstResponder()
			}
		}
	}
	
	// MARK: - UITextView

	func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
		keyboardTypeButton.isSelected = false
		
		if isExternalKeyboardShown {
			externViewBottomConstraint.constant = externViewHiddenY
			view.layoutIfNeeded()

			UIView.setAnimationsEnabled(false)
		}
		
		return true
	}
	
	override func textViewDidBeginEditing(_ textView: UITextView) {
		UIView.setAnimationsEnabled(true)
		
		super.textViewDidBeginEditing(textView)
	}
	
	func textViewDidChange(_ textView: UITextView) {
		if let text = textView.text {
			let height = floor(text.height(withConstrainedWidth: textView.bounds.width - (textView.textContainerInset.left + textView.textContainerInset.right), font: textView.font!))
				+ textView.textContainerInset.top + textView.textContainerInset.bottom
			
			if height <= 97 {
				conversationHeightConstraint.constant = height
				
				view.layoutIfNeeded()

//				Log.info?.value(conversationHeightConstraint.constant)
				conversationTextView.setContentOffset(.zero, animated: true)
			}
		}
	}
	
	// MARK: - Keyboard
	
	override func keyboardWillShown(_ notification: Notification) {
		guard let info = (notification as NSNotification).userInfo else { return }
		guard let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
		guard let curve = (info[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber)?.intValue else { return }
		guard let duration = (info[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue else { return }
		
		UIView.animate(withDuration: duration,
		               delay: 0.0,
		               options: UIViewAnimationOptions(rawValue: UInt(curve) << 16),
		               animations: { [weak self] in
						self?.contentInputBottomConstraint.constant = keyboardFrame.size.height
						self?.view.layoutIfNeeded()
			},
		               completion: nil)
	}
	
	open override func keyboardWillBeHidden(_ notification: Notification) {
		guard let info = (notification as NSNotification).userInfo else { return }
		guard let curve = (info[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber)?.intValue else { return }
		guard let duration = (info[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue else { return }
		
		UIView.animate(withDuration: duration,
		               delay: 0.0,
		               options: UIViewAnimationOptions(rawValue: UInt(curve) << 16),
		               animations: { [weak self] in
						self?.contentInputBottomConstraint.constant = 0
						self?.view.layoutIfNeeded()
			},
		               completion: nil)
	}
	
	func hideExternalKeyboard() {
		UIView.animate(withDuration: WGDeviceService.keyboardAnimationDuration,
		               delay: 0,
		               options: UIViewAnimationOptions(rawValue: UInt(WGDeviceService.keyboardAnimationCurve) << 16),
		               animations: {
						self.externViewBottomConstraint.constant = self.externViewHiddenY
						self.view.layoutIfNeeded()
		}, completion: nil)
	}
	
	// MARK: - Override
	
	override func viewTouched(_ sender: UITapGestureRecognizer) {
		super.viewTouched(sender)

		if isKeyboardShown {
			hideExternalKeyboard()
		}
	}
	
	override func dismiss(_ sender: AnyObject?) {
		if let chat = WGChat.chat(id: chatId) {
			if chat.items.isEmpty {
				notificationToken?.stop()
				chat.remove()
				
				navigationController?.popViewController(animated: true)
			} else {
				WGChatService.leave(chatId: chatId)
				
				super.dismiss()
			}
		}
	}
	
	// MARK: - Segue
	
	@IBAction func editChatTitle(segue: UIStoryboardSegue) {
		segue.source.dismiss(animated: false) {
			DispatchQueue.main.async {
				self.performSegue(withIdentifier: "EditChatTitleSegue", sender: nil)
			}
		}
	}
	
	@IBAction func inviteChatWithFriends(segue: UIStoryboardSegue) {
		dismiss(animated: false) {
			if let viewController = self.storyboard?.instantiateViewController(withIdentifier: String(describing: WGChatInviteViewController.self)) as? WGChatInviteViewController {
				viewController.chatId = self.chatId
				
				DispatchQueue.main.async {
					self.navigationController?.pushViewController(viewController, animated: true)
				}
			}
		}
	}
	
	@IBAction func leave(segue: UIStoryboardSegue) {
		notificationToken?.stop()
		
		dismiss(animated: true) {
			self.navigationController?.popViewController(animated: true)
		}
	}
	
	@IBAction func pop(segue: UIStoryboardSegue) {
		navigationController?.popViewController(animated: true)
	}
}
