//
//  WGChatViewController+Extension.swift
//  Weegle
//
//  Created by 이인재 on 05/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import MobileCoreServices
import ContactsUI
import AVKit
import PKHUD
import SwiftyJSON
import CleanroomLogger

extension WGChatViewController {

	func selectImage(mediaType: String) {
		guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else { return }
		
		let picker = UIImagePickerController.default
		picker.sourceType = .photoLibrary
		picker.mediaTypes = [mediaType]
		picker.delegate = self
		
		present(picker, animated: true, completion: nil)
	}
	
	func camera() {
		guard UIImagePickerController.isSourceTypeAvailable(.camera) else { return }
		
		let picker = UIImagePickerController.default
		picker.sourceType = .camera
		picker.allowsEditing = false
		picker.delegate = self
		picker.showsCameraControls = true
		picker.videoQuality = .typeMedium
		picker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
		
		present(picker, animated: true, completion: nil)
	}
	
	func live() {
		if let viewContoller = storyboard?.instantiateViewController(withIdentifier: String(describing: WGChatLiveSetupViewController.self)) as? WGChatLiveSetupViewController {
			viewContoller.chatId = chatId
			
			let chatItem = WGChatItem()
			chatItem.externType = ChatExternType.live.rawValue
			
			WGChatService.send(chatId: chatId, chatItem: chatItem)
			
			present(viewContoller, animated: false)
		}
	}
	
	func contact() {
		let contactPicker = CNContactPickerViewController()
		contactPicker.delegate = self
		contactPicker.displayedPropertyKeys = [CNContactPhoneNumbersKey, CNContactEmailAddressesKey, CNContactPostalAddressesKey]
		
		present(contactPicker, animated: true)
	}
}

// MARK: - UIImagePickerControllerDelegate

extension WGChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
			Log.info?.value(image)
			
			if let viewController = storyboard?.instantiateViewController(withIdentifier: String(describing: WGImageViewController.self)) as? WGImageViewController {
				viewController.chatId = chatId
				viewController.image = image
				
				picker.pushViewController(viewController, animated: true)
			}
		} else if let movieUrl = info[UIImagePickerControllerMediaURL] as? URL {
			Log.info?.value(movieUrl)
			
			HUD.flash(.progress)
			
			WGChatService.videoMessage(chatId: chatId, messageTempId: "-1", videoURL: movieUrl, success: { (_) in
				HUD.hide()
				
				picker.dismiss(animated: true)
			}, progress: { (progress) in
				Log.info?.value(progress)
			}, failure: { (error) in
				Log.error?.value(error?.localizedDescription)
				
				HUD.hide()
				
				picker.dismiss(animated: true)
			})
		}
	}
	
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		picker.dismiss(animated: true, completion: nil)
	}
}

// MARK: - CNContactPickerDelegate

extension WGChatViewController: CNContactPickerDelegate {
	
	func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
		sendContact(contacts: [contact])
	}
	
	func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
		sendContact(contacts: contacts)
	}
	
	fileprivate func sendContact(contacts: [CNContact]) {
		guard let current = WGUserService.current else { return }
		
		for contact in contacts {
			if let name = CNContactFormatter.string(from: contact, style: .fullName) {
				let phone = contact.phoneNumbers.filter { $0.value.stringValue.hasPrefix("010") }.first?.value.stringValue
			
				if phone != nil {
					let chatItem = WGChatItem()
					chatItem.writerObjectId = current.objectId
					chatItem.messageTempId = UUID().uuidString
					chatItem.extern = "{\"name\": \"\(name)\", \"number\": \"\(phone!)\"}"
					chatItem.externType = ChatExternType.contact.rawValue
					
					WGChatService.send(chatId: chatId, chatItem: chatItem)
				}
			}
		}
	}
}
