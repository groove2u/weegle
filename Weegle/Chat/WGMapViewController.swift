//
//  WGMapViewController.swift
//  Weegle
//
//  Created by 이인재 on 07/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class WGMapViewController: WGBaseViewController {
	
	private var locationManager = CLLocationManager()
	private var currentLocation: CLLocation?
	private var mapView: GMSMapView!
	private var placesClient: GMSPlacesClient!
	private var zoomLevel: Float = 15.0

	override func prepareUI() {
		super.prepareUI()
		
//		locationManager = CLLocationManager()
//		locationManager.desiredAccuracy = kCLLocationAccuracyBest
//		locationManager.requestAlwaysAuthorization()
//		locationManager.distanceFilter = 50
//		locationManager.startUpdatingLocation()
//		locationManager.delegate = self
//		
//		placesClient = GMSPlacesClient.shared()
	}
	
	override func loadView() {
		let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
		let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
		mapView.isMyLocationEnabled = true
		view = mapView
		
		// Creates a marker in the center of the map.
		let marker = GMSMarker()
		marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
		marker.title = "Sydney"
		marker.snippet = "Australia"
		marker.map = mapView
	}
}
