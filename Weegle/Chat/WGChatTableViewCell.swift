//
//  WGChatTableViewCell.swift
//  Weegle
//
//  Created by 이인재 on 27/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGChatTableViewCell: UITableViewCell {

	@IBOutlet weak var bubbleView: WGChatBubbleView!
	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var unreadCountLabel: UILabel!
	@IBOutlet weak var errorStackView: UIStackView!
	@IBOutlet weak var userNameLabel: UILabel!
	@IBOutlet weak var profileImageView: UIImageView!
	
	weak var delegate: ListItemInteractionProtocol?
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		let gesture = UITapGestureRecognizer()
		gesture.addTarget(self, action: #selector(tapped(gesture:)))
		gesture.numberOfTapsRequired = 1
		
		bubbleView.addGestureRecognizer(gesture)
	}
	
	// MARK: - Gesture
	
	@objc private func tapped(gesture: UITapGestureRecognizer) {
		delegate?.listItem(cell:self, didTouched: .detail)
	}
	
	func present(chatItem: WGChatItem) {
		timeLabel.text = chatItem.timeString
		
		unreadCountLabel.text = "\(chatItem.unreadCount)"
		unreadCountLabel.isHidden = chatItem.unreadCount <= 0
		
		if chatItem.user?.isMe == false {
			if let urlString = chatItem.user?.profileImage {
				profileImageView?.kf.setImage(with: WGNetworkService.resourceURL(uri: urlString), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
			}
			
			userNameLabel?.text = chatItem.user?.memberName
		}
	}
}
