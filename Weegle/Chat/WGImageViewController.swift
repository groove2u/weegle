//
//  WGImageViewController.swift
//  Weegle
//
//  Created by 이인재 on 24/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import PKHUD
import Kingfisher
import CleanroomLogger

class WGImageViewController: WGBaseViewController {

	@IBOutlet private weak var confirmButton: UIBarButtonItem!
	@IBOutlet private weak var imageView: UIImageView!
	
	var chatId: String?
	var image: UIImage!
	var imageURL: URL?
	
	override func updateUI() {
		super.updateUI()
		
		navigationController?.setNavigationBarHidden(false, animated: true)
	}
	
	override func prepareUI() {
		super.prepareUI()
		
		if let url = imageURL {
			HUD.show(.progress)
			
			imageView.kf.setImage(with: url, completionHandler: { (_, _, _, _) in
				HUD.hide()
			})
		} else {
			imageView.image = image
		}
		
		if chatId == nil {
			navigationItem.rightBarButtonItems = nil
		}
	}
	
	// MARK: - Action
	
	@IBAction private func send() {
		guard let chatId = chatId else { return }
		
		HUD.flash(.progress)
		
		WGChatService.imageMessage(chatId: chatId,
		                           messageTempId: "-1",
		                           image: image,
		                           success: { (_) in
									HUD.hide()
									
									self.dismiss(animated: true, completion: nil)
		}, progress: { (_) in
			
		}, failure: { (_) in
			HUD.hide()
			
			self.dismiss(animated: true, completion: nil)
		})
	}
	
	// MARK: - UIScrollViewDelegate
	
	func viewForZooming(in scrollView: UIScrollView) -> UIView? {
		return imageView
	}
}
