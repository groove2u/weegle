//
//  WGChatEditTitleViewController.swift
//  Weegle
//
//  Created by 이인재 on 08/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import CleanroomLogger

class WGChatEditTitleViewController: WGBaseViewController {

	@IBOutlet weak var countLabel: UILabel!
	@IBOutlet weak var titleTextField: UITextField!
	
	var chatId: String!
	
	private let maxCount = 20
	
	private var chat: WGChat?
	
	override func prepareUI() {
		super.prepareUI()
		
		chat = WGChat.chat(id: chatId)
		
		if chat?.title.isEmpty == true {
			titleTextField.text = chat?.otherNames
		} else {
			titleTextField.text = chat?.title
		}
		
		NotificationCenter.default.addObserver(self, selector: #selector(updateStatus), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
		
		updateStatus()
	}
	
	@objc private func updateStatus() {
		countLabel.text = "\(titleTextField.text?.count ?? 0)/\(maxCount)"
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		guard let text = textField.text else { return true }
		
		let result = (text as NSString).replacingCharacters(in: range, with: string)
		
		return result.count <= maxCount
	}
	
	@IBAction func save(_ sender: Any) {
		guard let title = TextValidator.validate(textField: titleTextField, title: nil, message: "대화방 이름을 입력해주세요.", confirmTitle: "confirm".localized) else {
			return
		}
		
		WGChatService.change(title: title, chatId: chatId, success: {
			do {
				try self.chat?.realm?.write {
					self.chat?.title = title
				}
			} catch {
				Log.error?.value(error)
			}
			
			self.navigationController?.popViewController(animated: true)
		}) { (error) in
			Log.error?.value(error)
		}
	}
}
