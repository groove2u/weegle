//
//  WGChatContactTableViewCell.swift
//  Weegle
//
//  Created by 이인재 on 07/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import SwiftyJSON

class WGChatContactTableViewCell: WGChatTableViewCell {

	@IBOutlet weak var contactNameLabel: UILabel!
	
	override func present(chatItem: WGChatItem) {
		super.present(chatItem: chatItem)
		
		if let data = chatItem.extern.data(using: .utf8) {
			let externJSON = JSON(data: data)
			contactNameLabel.text = externJSON["name"].string
		}
	}
}
