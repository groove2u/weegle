//
//  WGChatSideViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.30.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import CleanroomLogger

protocol DisplayContentProtocol: class {
	
	func showContent(_ image: UIImage)
	func showContent(_ videoURL: URL)
}

class WGChatSideViewController: WGBaseViewController {

	@IBOutlet weak private var leadingConstraint: NSLayoutConstraint!
	@IBOutlet weak private var contentView: UIView!
	@IBOutlet weak private var albumView: UIView!
	@IBOutlet weak var albumViewBottomContraint: NSLayoutConstraint!
	@IBOutlet weak var pastImageViewStackView: UIStackView!
	@IBOutlet weak private var tableView: UITableView!
	@IBOutlet weak private var pastImageView0: UIImageView!
	@IBOutlet weak private var pastImageView1: UIImageView!
	@IBOutlet weak private var pastImageView2: UIImageView!
	
	var chatId: String! {
		didSet {
			chat = WGChat.chat(id: chatId)
		}
	}
	weak var delegate: DisplayContentProtocol?
	
	private let maxBackgroundColor = UIColor(hex: 0x222222, alpha: 0.4)
	private let minBackgroundColor = UIColor(hex: 0x222222, alpha: 0)
	
	private var chat: WGChat?
	private var pastImageViews: [UIImageView] {
		return [pastImageView0, pastImageView1, pastImageView2]
	}
	
	override func prepareUI() {
		super.prepareUI()
		
		if let imageChats = chat?.items.filter("externType = %@", ChatExternType.image.rawValue).prefix(3) {
			if imageChats.isEmpty {
				albumViewBottomContraint.constant = 0
			} else {
				for i in 0..<imageChats.count {
					if let data = imageChats[i].extern.data(using: .utf8) {
						let externJSON = JSON(data: data)
						let urlString = externJSON["imagePath"].stringValue
						
						pastImageViews[i].kf.setImage(with: WGNetworkService.resourceURL(uri: urlString))
					}
				}
			}
		}
		
		hide(animated: false, shouldDismiss: false)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		show()
	}
	
	// MARK: - Animation
	
	private func show() {
		let contentWidth = contentView.bounds.width
		
		view.layoutIfNeeded()
		
		UIView.animate(withDuration: 0.3,
		               delay: 0,
		               options: [.curveEaseInOut, .beginFromCurrentState],
		               animations: { [weak self] in
						self?.leadingConstraint.constant = contentWidth
						self?.view.backgroundColor = self?.maxBackgroundColor
						
						self?.view.layoutIfNeeded()
		}) { (_) in
			
		}
	}
	
	private func hide(animated: Bool = true, shouldDismiss: Bool = true, complete: (() -> Void)? = nil) {
		UIView.animate(withDuration: animated ? 0.3 : 0,
		               delay: 0,
		               options: [.curveEaseInOut, .beginFromCurrentState],
		               animations: { [weak self] in
						self?.leadingConstraint.constant = 0
						self?.view.backgroundColor = self?.minBackgroundColor
						
						self?.view.layoutIfNeeded()
		}) { [weak self] (_) in
			if shouldDismiss {
				self?.dismiss(animated: false, completion: complete)
			}
		}
	}
	
	// MARK: - Action
	
	@IBAction func showImage(_ sender: UIButton) {
		guard let image = pastImageViews[sender.tag].image else { return }
		
		delegate?.showContent(image)
		
		dismissWithoutAnimation()
	}
	
	@IBAction func leave() {
		guard let chatId = chatId else { return }
		
		WGChat.chat(id: chatId)?.remove()
		WGChatService.destroy(chatId: chatId)
		
		hide(animated: false, shouldDismiss: false)
		performSegue(withIdentifier: "LeaveSegue", sender: nil)
	}
	
	@IBAction func hideRequested() {
		hide()
	}
}

// MARK: - UITableViewDelegate, dataSource

extension WGChatSideViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return chat?.chatMemberInfo.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: WGFriendTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGFriendTableViewCell else { return UITableViewCell() }

		cell.delegate = self
		
		if let objectId = chat?.chatMemberInfo[indexPath.item].memberObjectId {
			if let user = WGUser.user(objectId: objectId) {
				cell.profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: user.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
				cell.nameLabel.text = user.memberName
				cell.addButton?.isHidden = true
			} else {
				WGFriendService.info(objectId: objectId, success: { (user) in
					if let user = user {
						cell.profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: user.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
						cell.nameLabel.text = user.memberName
						cell.addButton?.isHidden = false
					}
				}, failure: { (_) in
					
				})
			}
		}
		
		return cell
	}
}

// MARK: - ListItemInteractionProtocol

extension WGChatSideViewController: ListItemInteractionProtocol {
	
	func listItem(cell: UITableViewCell, didTouched event: ListItemInteractionEvent) {
		guard let indexPath = tableView.indexPath(for: cell) else { return }
		
		if let objectId = chat?.chatMemberInfo[indexPath.item].memberObjectId {
			WGFriendService.add(objectId: objectId, success: {
				self.tableView.reloadData()
			}, failure: { (_) in
				
			})
		}
	}
}
