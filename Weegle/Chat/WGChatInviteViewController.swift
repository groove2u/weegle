//
//  WGChatInviteViewController.swift
//  Weegle
//
//  Created by 이인재 on 08/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import RealmSwift
import CleanroomLogger

class WGChatInviteViewController: WGBaseViewController {

	@IBOutlet weak private var tableView: UITableView!
	@IBOutlet weak private var collectionView: UICollectionView!
	@IBOutlet weak private var noFriendNoticeView: UIView!
	@IBOutlet weak private var searchBar: UISearchBar!
	@IBOutlet var inviteButton: UIBarButtonItem!
	@IBOutlet weak private var selectFriendsNoticeLabel: UILabel!
	@IBOutlet weak private var bottomInviteButton: WGGreenButton?
	
	var type: WGChatType = .normal
	var chatId: String?
	var selectedFriendIds: [String]?
	
	fileprivate var models: Results<WGUser>?
	
	private var notificationToken: NotificationToken?
	
	override func prepareUI() {
		super.prepareUI()
		
		models = WGUserService.current?.friends?.sorted(byKeyPath: "memberName")
		
		tableView.tableFooterView = UIView(frame: .zero)
		
		if WGUserService.isLogin {
			fetch()
		}
		
		if let selectedFriendIds = selectedFriendIds {
			for objectId in selectedFriendIds {
				if let index = models?.index(matching: "objectId = %@", objectId) {
					let indexPath = IndexPath(item: index, section: 0)
					
					self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
					self.tableView(self.tableView, didSelectRowAt: indexPath)
				}
			}
		}
		
		updateStatus()
	}
	
	deinit {
		notificationToken?.stop()
	}
	
	@objc override func fetch() {
		WGFriendService.update()
	}
	
	override func updateDataUI() {
		tableView.reloadData()
		collectionView.reloadData()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "FriendSelectedSegue",
			let viewController = segue.destination as? WGLiveAccessLevelViewController {
			guard let indexPaths = tableView.indexPathsForSelectedRows, indexPaths.isEmpty == false else { return }
			
			var objectIdx = [String]()
			
			indexPaths.forEach { (indexPath) in
				if let model = self.model(at: indexPath) {
					objectIdx.append(model.objectId)
				}
			}
			
			viewController.invitedFriendObjectIds = objectIdx
		}
	}
	
	// Model
	
	private func model(at indexPath: IndexPath) -> WGUser? {
		guard let models = models else { return nil }
		
		if let text = searchBar.text, text.isEmpty == false {
			return models.filter("memberName CONTAINS %@", text)[indexPath.item]
		}
		
		return models[indexPath.item]
	}
	
	private func updateStatus() {
		if let indexPaths = tableView.indexPathsForSelectedRows {
			if let inviteButton = inviteButton {
				navigationItem.rightBarButtonItems = [inviteButton]
				inviteButton.title = "초대 \(indexPaths.count)"
			}
			
			bottomInviteButton?.isEnabled = true
			bottomInviteButton?.setTitle("초대 \(indexPaths.count)", for: .normal)
		} else {
			navigationItem.rightBarButtonItems = nil
			
			bottomInviteButton?.isEnabled = false
			bottomInviteButton?.setTitle("초대", for: .normal)
		}
		
		collectionView.reloadData()
	}
	
	// Action
	
	@IBAction func invite(sender: UIBarButtonItem) {
		guard let indexPaths = tableView.indexPathsForSelectedRows, indexPaths.isEmpty == false else { return }
		
		var objectIdx = [String]()
		
		indexPaths.forEach { (indexPath) in
			if let model = self.model(at: indexPath) {
				objectIdx.append(model.objectId)
			}
		}
		
		if let chatId = chatId {
			WGChatService.invite(chatId: chatId, friendIds: objectIdx)
			
			navigationController?.popViewController(animated: true)
		} else {
			NotificationCenter.default.post(name: WGChatService.openChatNotification.name, object: nil, userInfo: ["id": objectIdx])
		}
	}
}

extension WGChatInviteViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let models = models else { noFriendNoticeView.isHidden = false; return 0 }
		
		let result: Int
		
		if let text = searchBar.text, text.isEmpty == false {
			result = models.filter("memberName CONTAINS %@", text).count
		} else {
			result = models.count
		}
		
		noFriendNoticeView.isHidden = result > 0
		
		return result
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let models = models else { return UITableViewCell() }
		guard let cell = tableView.dequeueReusableCell(withIdentifier: WGFriendTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGFriendTableViewCell,
			indexPath.item < models.count else {
				return UITableViewCell()
		}
		
		if let model = model(at: indexPath) {
			cell.selectButton?.isSelected = cell.isSelected
			cell.nameLabel.text = model.memberName
			cell.profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: model.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
			cell.promotionLabel?.text = model.profileComment
			cell.promotionView?.alpha = model.profileComment.isEmpty ? 0 : 1
		}
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		updateStatus()
	}
	
	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		updateStatus()
	}
}

// MARK: - UICollectionView

extension WGChatInviteViewController: UICollectionViewDelegate, UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		let result = tableView.indexPathsForSelectedRows?.count ?? 0
		
		selectFriendsNoticeLabel.isHidden = result > 0
		
		return result
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WGChatListEditFriendCollectionViewCell.defaultReuseIdentifier, for: indexPath) as?
			WGChatListEditFriendCollectionViewCell else {
			return UICollectionViewCell()
		}
		guard let indexPath = tableView.indexPathsForSelectedRows?[indexPath.item] else { return UICollectionViewCell() }
		guard let model = model(at: indexPath) else { return UICollectionViewCell() }
		
		cell.profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: model.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
		cell.nameLabel.text = model.memberName
		cell.delegate = self
		
		return cell
	}
}

// MARK: - Interaction

extension WGChatInviteViewController: CollectionItemInteractionProtocol {
	
	func listItem(cell: UICollectionViewCell, didTouched event: ListItemInteractionEvent) {
		guard let indexPath = collectionView.indexPath(for: cell) else { return }
		guard let foundIndexPath = tableView.indexPathsForSelectedRows?[indexPath.item] else { return }
		
		switch event {
		case .delete:
			tableView.deselectRow(at: foundIndexPath, animated: true)
			tableView(tableView, didDeselectRowAt: foundIndexPath)
			
			updateStatus()
		default: break
		}
	}
}

// MARK: - UISearchBarDelegate

extension WGChatInviteViewController: UISearchBarDelegate {
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		updateDataUI()
	}
}
