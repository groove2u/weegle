//
//  WGChatListViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import RealmSwift
import CleanroomLogger

class WGChatListViewController: WGBaseViewController, UITableViewDelegate, UITableViewDataSource {

	@IBOutlet var orderButton: UIButton!
	@IBOutlet weak private var tableView: UITableView!
	@IBOutlet weak private var addChatButton: UIButton!
	@IBOutlet weak private var noChatNoticeView: UIView!
	
	private var models: Results<WGChat>?
	private var notificationToken: NotificationToken?
	
	override func prepareUI() {
		super.prepareUI()
		
		navigationItem.titleView = orderButton
		
		tableView.tableFooterView = UIView()
		
		models = WGChat.list?.sorted(byKeyPath: "lastDate", ascending: false)
		notificationToken = models?.addNotificationBlock({ [weak self] (_) in
			self?.updateDataUI()
		})
	}
	
	deinit {
		notificationToken?.stop()
	}
	
	override func updateDataUI() {
		tableView.reloadData()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let identifier = segue.identifier {
			switch identifier {
			case "ShowAddChatSegue":
				addChatButton.isHidden = true
			case "ShowChatSegue":
				if let viewController = segue.destination as? WGChatViewController,
					let indexPath = sender as? IndexPath {
					viewController.chatId = models?[indexPath.item].chatId
				}
			default: break
			}
		}
	}
	
	override func updateUI() {
		super.updateUI()
	}
	
	// MARK: - Service
	
	func enter(chatId: String) {
		guard let index = models?.index(matching: "chatId = %@", chatId) else {
			Log.error?.message("Chat \(chatId) not found in realm.")
			
			WGChatService.enter(chatId: chatId, complete: {
				self.enter(chatId: chatId)
			})
			
			return
		}
		
		tableView(tableView, didSelectRowAt: IndexPath(item: index, section: 0))
	}
	
	// MARK: - UITableViewDelegate, dataSource
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let result = models?.count ?? 0
		
		noChatNoticeView.isHidden = result > 0
		
		return result
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: WGChatListTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGChatListTableViewCell else {
			return UITableViewCell()
		}
		guard let models = models else { return UITableViewCell() }
		guard indexPath.item < models.count else { return UITableViewCell() }
		
		let model = models[indexPath.item]
		
		if let otherUsers = model.others,
			let otherUserInfo = otherUsers.first,
			let user = WGUser.user(objectId: otherUserInfo.memberObjectId) {
			cell.profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: user.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
			
			if model.title.isEmpty {
				cell.titleLabel.text = model.otherNames
			} else {
				cell.titleLabel.text = model.title
			}
		}
		
		if let recent = model.items.last {
			if let icon = ChatExternType(rawValue: recent.externType)?.icon {
				cell.conversationLabel.text = nil
				cell.attachmentTypeImageView.isHidden = false
				
				cell.attachmentTypeImageView.image = icon
			} else {
				cell.attachmentTypeImageView.isHidden = true
				
				cell.conversationLabel.text = recent.message
			}
		} else {
			cell.attachmentTypeImageView.isHidden = true
			cell.conversationLabel.text = ""
		}

		cell.unreadCountLabel.text = "\(model.unreadCount)"
		cell.unreadCountView.isHidden = model.unreadCount <= 0
		cell.lastTimeLabel.text = model.shortTimeString
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		performSegue(withIdentifier: "ShowChatSegue", sender: indexPath)
	}
	
	// MARK: - Action
	
	@IBAction func orderConversation() {
		let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		
		sheet.addAction(UIAlertAction(title: "최신 순", style: .default, handler: { (_) in
			self.models = self.models?.sorted(byKeyPath: "lastDate", ascending: true)
			
			self.updateDataUI()
		}))
		sheet.addAction(UIAlertAction(title: "안 읽은 순", style: .default, handler: { (_) in

		}))
		sheet.addAction(UIAlertAction(title: "취소", style: .cancel))
		
		present(sheet, animated: true, completion: nil)
	}
	
	@IBAction func closeAddChatPopup(segue: UIStoryboardSegue) {
		dismiss(animated: false) { [weak self] in
			self?.addChatButton.isHidden = false
		}
	}
	
	@IBAction func openNormalChatWithFriends(segue: UIStoryboardSegue) {
		dismiss(animated: false) {
			self.addChatButton.isHidden = false
			
			if let viewController = self.storyboard?.instantiateViewController(withIdentifier: String(describing: WGChatInviteViewController.self)) as? WGChatInviteViewController {
				viewController.type = .normal
			
				DispatchQueue.main.async {
					self.navigationController?.pushViewController(viewController, animated: true)
				}
			}
		}
	}
	
	@IBAction func openSecretChatWithFriends(segue: UIStoryboardSegue) {
		dismiss(animated: false) {
			self.addChatButton.isHidden = false
			
			if let viewController = self.storyboard?.instantiateViewController(withIdentifier: String(describing: WGChatInviteViewController.self)) as? WGChatInviteViewController {
				viewController.type = .private
				
				DispatchQueue.main.async {
					self.navigationController?.pushViewController(viewController, animated: true)
				}
			}
		}
	}
}
