//
//  WGChatListEditViewController.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.23.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import RealmSwift
import CleanroomLogger

class WGChatListEditViewController: WGBaseViewController, UITableViewDelegate, UITableViewDataSource {

	@IBOutlet weak private var tableView: UITableView!
	@IBOutlet weak private var noChatNoticeView: UIView!
	@IBOutlet weak var leaveButton: WGRedButton!
	@IBOutlet weak var searchBar: UISearchBar!
	
	private var models: Results<WGChat>?
	
	override func prepareUI() {
		super.prepareUI()
		
		tableView.tableFooterView = UIView()
		
		models = WGChat.list?.sorted(byKeyPath: "lastDate", ascending: false)
	}
	
	override func updateDataUI() {
		tableView.reloadData()
		
		updateStatus()
	}
	
	override func updateUI() {
		super.updateUI()
	}
	
	private func model(at indexPath: IndexPath) -> WGChat? {
		guard let models = models else { return nil }
		
		if let text = searchBar.text, text.isEmpty == false {
			return models.filter("title CONTAINS %@", text)[indexPath.item]
		}
		
		return models[indexPath.item]
	}
	
	// MARK: - UITableViewDelegate, dataSource
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let models = models else { noChatNoticeView.isHidden = false; return 0 }
		
		let result: Int
		
		if let text = searchBar.text, text.isEmpty == false {
			result = models.filter("title CONTAINS %@", text).count
		} else {
			result = models.count
		}
		
		noChatNoticeView.isHidden = result > 0
		
		return result
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: WGChatListTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGChatListTableViewCell else {
			return UITableViewCell()
		}
		guard let models = models else { return UITableViewCell() }
		guard indexPath.item < models.count else { return UITableViewCell() }
		guard let model = model(at: indexPath) else { return UITableViewCell() }
		
		if let otherUsers = model.others,
			let otherUserInfo = otherUsers.first,
			let user = WGUser.user(objectId: otherUserInfo.memberObjectId) {
			cell.profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: user.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
			
			if model.title.isEmpty {
				cell.titleLabel.text = model.otherNames
			} else {
				cell.titleLabel.text = model.title
			}
		}
		
		if let recent = model.items.last {
			if let icon = ChatExternType(rawValue: recent.externType)?.icon {
				cell.conversationLabel.text = nil
				cell.attachmentTypeImageView.isHidden = false
				
				cell.attachmentTypeImageView.image = icon
			} else {
				cell.attachmentTypeImageView.isHidden = true
				
				cell.conversationLabel.text = recent.message
			}
		}
		
		cell.selectButton?.isSelected = cell.isSelected
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		updateStatus()
	}
	
	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		updateStatus()
	}
	
	private func updateStatus() {
		if let indexPathsForSelectedRows = tableView.indexPathsForSelectedRows, indexPathsForSelectedRows.isEmpty == false {
			leaveButton.isEnabled = true
		} else {
			leaveButton.isEnabled = false
		}
	}
	
	// MARK: - Action
	
	@IBAction func leaveSelectedChats() {
		guard let indexPathsForSelectedRows = tableView.indexPathsForSelectedRows else { return }
		
		let alert = UIAlertController(title: nil, message: "C6".localized, preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(title: "confirm".localized, style: .default, handler: { (_) in
			if let models = self.models {
				var buffer = [String]()
				
				for indexPath in indexPathsForSelectedRows {
					buffer.append(models[indexPath.item].chatId)
				}
				
				for chatId in buffer {
					WGChat.chat(id: chatId)?.remove()
					WGChatService.destroy(chatId: chatId)
				}
			}
			
			self.updateDataUI()
		}))
		alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel))
		
		present(alert, animated: true)
	}
}

// MARK: - UISearchBarDelegate

extension WGChatListEditViewController: UISearchBarDelegate {
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		updateDataUI()
	}
}
