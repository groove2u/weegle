//
//  WGChatImageTableViewCell.swift
//  Weegle
//
//  Created by 이인재 on 27/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import SwiftyJSON

class WGChatImageTableViewCell: WGChatTableViewCell {

	@IBOutlet weak var messageImageView: UIImageView!
	@IBOutlet weak var playView: UIView!
	
	override func present(chatItem: WGChatItem) {
		super.present(chatItem: chatItem)
		
		if let data = chatItem.extern.data(using: .utf8) {
			let externJSON = JSON(data: data)
			let urlString = chatItem.externType == ChatExternType.image.rawValue ? externJSON["thumbImagePath"].stringValue : externJSON["thumbVideoPath"].stringValue
			
			playView.isHidden = chatItem.externType == ChatExternType.image.rawValue
			
			messageImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: urlString))
		}
	}
}
