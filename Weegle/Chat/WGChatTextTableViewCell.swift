//
//  WGChatTextTableViewCell.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.29.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGChatTextTableViewCell: WGChatTableViewCell {

	@IBOutlet weak var conversationLabel: UILabel!
	
	override func present(chatItem: WGChatItem) {
		super.present(chatItem: chatItem)
		
		conversationLabel.text = chatItem.message
	}
}
