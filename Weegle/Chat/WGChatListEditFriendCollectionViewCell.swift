//
//  WGChatListEditFriendCollectionViewCell.swift
//  Weegle
//
//  Created by 이인재 on 30/07/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGChatListEditFriendCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var profileImageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	
	weak var delegate: CollectionItemInteractionProtocol?
	
	@IBAction func remove() {
		delegate?.listItem(cell: self, didTouched: .delete)
	}
}
