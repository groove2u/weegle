//
//  WGChatListTableViewCell.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.23.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGChatListTableViewCell: UITableViewCell {

	@IBOutlet weak var profileImageView: UIImageView!
	@IBOutlet weak var secretCoverImageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var conversationLabel: UILabel!
	@IBOutlet weak var lastTimeLabel: UILabel!
	@IBOutlet weak var attachmentTypeImageView: UIImageView!
	@IBOutlet weak var errorIconImageView: UIImageView!
	@IBOutlet weak var unreadCountView: UIView!
	@IBOutlet weak var unreadCountLabel: UILabel!
	@IBOutlet weak var selectButton: UIButton!
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		selectButton?.isSelected = selected
	}
}
