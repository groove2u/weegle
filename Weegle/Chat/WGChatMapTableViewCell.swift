//
//  WGChatMapTableViewCell.swift
//  Weegle
//
//  Created by 이인재 on 07/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import SwiftyJSON

class WGChatMapTableViewCell: WGChatTableViewCell {

	@IBOutlet weak var locationTitleLabel: UILabel!
	@IBOutlet weak var locationDetailLabel: UILabel!
	
	/*
	{"locationLat":37.5407625,"locationLng":127.0793428,"name":"건국대학교","title":"120 Neungdong-ro, Gwangjin-gu, Seoul, South Korea"}
	*/
	
	override func present(chatItem: WGChatItem) {
		super.present(chatItem: chatItem)
		
		if let data = chatItem.extern.data(using: .utf8) {
			let externJSON = JSON(data: data)
			locationTitleLabel.text = externJSON["name"].string
			locationDetailLabel.text = externJSON["title"].string
		}
	}
}
