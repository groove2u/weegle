//
//  WGDateConverter.swift
//  Weegle
//
//  Created by 이인재 on 02/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import Foundation

class WGDateConverter {

	static let dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy/MM/dd"
		
		return formatter
	}()
	
	static let shortTimeFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateFormat = "a hh:mm"
		
		return formatter
	}()
	
	static func string(fromTimeintervalString timeIntervalString: String, formatter: DateFormatter) -> String? {
		guard let timeInterval = TimeInterval(timeIntervalString) else { return nil }
		
		let date = Date(timeIntervalSince1970: timeInterval / 1_000)
		
		return formatter.string(from: date)
	}
}
