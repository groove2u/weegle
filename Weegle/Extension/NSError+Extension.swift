//
//  NSError+Extension.swift
//  Weegle
//
//  Created by 이인재 on 07/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

extension NSError {

	@nonobjc var message: String? {
		return userInfo["message"] as? String
	}
}
