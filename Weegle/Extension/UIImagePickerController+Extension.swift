//
//  UIImagePickerController+Extension.swift
//  Weegle
//
//  Created by 이인재 on 24/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

extension UIImagePickerController {

	static var `default`: UIImagePickerController {
		let picker = UIImagePickerController()
		picker.navigationBar.isTranslucent = false
		picker.navigationBar.backgroundColor = .white
		picker.navigationBar.barTintColor = WGColor.baseColor(for: 9)
		picker.navigationBar.tintColor = .white
		picker.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 17), NSAttributedStringKey.foregroundColor: UIColor.white]

		return picker
	}
}
