//
//  Data+Extension.swift
//  Weegle
//
//  Created by 이인재 on 01/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

extension Data {

	var hexString: String {
		return self.map { String(format: "%02hhx", $0) }.joined()
	}
}
