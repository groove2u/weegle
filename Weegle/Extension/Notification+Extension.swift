//
//  Notification+Extension.swift
//  Weegle
//
//  Created by 이인재 on 12/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

extension Notification {

	init(name: String, object: Any? = nil, userInfo: [AnyHashable : Any]? = nil) {
		self.init(name: Notification.Name("\(appName).\(name)"), object: object, userInfo: userInfo)
	}
}

extension Notification.Name {
	
	static let UserProfileChanged = Notification.Name(rawValue: "\(appName).notification.user.profile.changed")
}
