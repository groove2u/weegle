//
//  Dictionary+Extension.swift
//  Weegle
//
//  Created by 이인재 on 21/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

extension Dictionary {

	var jsonString: String? {
		do {
			let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
			
			return String(data: jsonData, encoding: .utf8)
		} catch {
			return nil
		}
	}
}
