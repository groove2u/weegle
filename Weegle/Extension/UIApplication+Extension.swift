//
//  UIApplication+Extension.swift
//  Weegle
//
//  Created by 이인재 on 04/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

extension UIApplication {

	static var appVersion: String {
		return (Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String) ?? ""
	}
	static var buildVersion: String {
		return (Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String) ?? ""
	}
}
