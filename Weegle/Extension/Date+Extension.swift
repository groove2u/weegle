//
//  Date+Extension.swift
//  Weegle
//
//  Created by 이인재 on 2017/08/29.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

extension Date {

	var timestamp: String {
		return "\(floor(timeIntervalSince1970 * 1_000))"
	}
}
