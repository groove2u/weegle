//
//  WGFriendService.swift
//  Weegle
//
//  Created by 이인재 on 06/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import SwiftyJSON
import Contacts
import CleanroomLogger

enum Relation: String {
	case block
	case hidden
	case unblock
	case unhidden
}

class WGFriendService {

	static let addedNotification = Notification(name: Notification.Name("\(appName).friend.added"))
	
	/// 친구 추가
	///
	/// - Parameters:
	///   - id: 추가할 친구의 ID
	///   - success: 성공 클로저
	///   - failure: 실패 클로저
	static func add(objectId: String, success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/friends", method: .post, parameters: ["friendId": objectId], needsAuth: true, success: { (json) in
			let code = json["code"].intValue
			let status = json["status"].stringValue
			
			if status == "normal" {
				NotificationCenter.default.post(name: addedNotification.name, object: nil, userInfo: ["id": objectId])
				
				success()
			} else {
				failure(NSError(domain: "FriendDomain", code: code, userInfo: ["message": json["message"].stringValue]))
			}
		}, failure: { (error) in
			failure(error)
		})
	}
	
	/// 친구 리스트
	///
	/// - Parameters:
	///   - success: 성공 클로저
	///   - failure: 실패 클로저
	static func update(success: (() -> Void)? = nil, failure: ((NSError?) -> Void)? = nil) {
		WGNetworkService.requestJSON(api: "/friends", needsAuth: true, success: { (json) in
			if let friendsInfo = json.array {
				WGUser.clear()
				
				for info in friendsInfo {
					WGUser(value: info.object).save()
				}
				
				success?()
			}
		}) { (error) in
			Log.error?.value(error)
			
			failure?(error)
		}
	}
	
	static func sync(success: @escaping () -> Void, failure: @escaping () -> Void) {
		var contacts = [String]()
		let request = CNContactFetchRequest(keysToFetch: [CNContactPhoneNumbersKey as CNKeyDescriptor])
		
		do {
			try CNContactStore().enumerateContacts(with: request, usingBlock: { (contact, _) in
				contacts.append(contentsOf: contact.phoneNumbers.filter { $0.value.stringValue.hasPrefix("010") }.map { $0.value.stringValue.replacingOccurrences(of: "-", with: "") })
			})
		} catch {
			Log.error?.value(error)
		}
		
		WGNetworkService.requestJSON(api: "/members/sync", method: .put, parameters: ["friends": contacts], encoding: JSONEncoding.default, needsAuth: true, success: { (json) in
			Log.info?.value(json)
			
			WGUserService.current?.lastSyncDate = Date()
		}) { (error) in
			Log.error?.value(error)
		}
	}
	
	static func updateRelation(to: Relation, objectIdx: [String], success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/friends/\(to.rawValue)", method: .post, parameters: ["friends": objectIdx], encoding: JSONEncoding.default, needsAuth: true, success: { (json) in
			Log.info?.value(json)
			
			update(success: {
				success()
			}, failure: { (error) in
				failure(error)
			})
		}) { (error) in
			Log.error?.value(error)
			failure(error)
		}
	}
	
	static func delete(objectId: String, success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/friends/\(objectId)", method: .delete, parameters: ["friendId": objectId], needsAuth: true, success: { (json) in
			Log.info?.value(json)
			
			update(success: {
				success()
			}, failure: { (error) in
				failure(error)
			})
		}) { (error) in
			Log.error?.value(error)
			failure(error)
		}
	}
	
	static func relationList(of relation: Relation, success: @escaping ([WGUser]) -> Void, failure: @escaping (NSError?) -> Void) {
		guard relation != .unblock, relation != .unhidden else { failure(nil); return }
		
		WGNetworkService.requestJSON(api: "/friends/\(relation.rawValue)", needsAuth: true, success: { (json) in
			if json["code"].isEmpty {
				var buffer = [WGUser]()
				
				for info in json.arrayValue {
					buffer.append(WGUser(value: info.object))
				}
				
				success(buffer)
			} else {
				failure(nil)
			}
		}) { (error) in
			failure(error)
		}
	}
	
	static func info(memberId: String, success: @escaping (WGUser?) -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/getMember/\(memberId)", needsAuth: true, success: { (json) in
			if json.isEmpty == false {
				success(WGUser(value: json.object))
			} else {
				failure(nil)
			}
		}) { (error) in
			failure(error)
		}
	}
	
	static func info(objectId: String, success: @escaping (WGUser?) -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/members/\(objectId)", needsAuth: true, success: { (json) in
			if json.isEmpty == false {
				success(WGUser(value: json.object))
			} else {
				failure(nil)
			}
		}) { (error) in
			failure(error)
		}
	}
	
	static func search(memberId: String, success: @escaping (WGUser?) -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/memberIdSearch", method: .post, parameters: ["memberId": memberId], needsAuth: true, success: { (json) in
			if json.isEmpty == false, json["code"].isEmpty {
				success(WGUser(value: json.object))
			} else {
				failure(nil)
			}
		}) { (error) in
			failure(error)
		}
	}
	
	static func search(phone: String, nationalCode: String, success: @escaping (WGUser?) -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "member/phone/\(phone)", parameters: ["nationalCode": nationalCode], needsAuth: true, success: { (json) in
			if json.isEmpty == false {
				success(WGUser(value: json.object))
			} else {
				failure(nil)
			}
		}) { (error) in
			failure(error)
		}
	}
}
