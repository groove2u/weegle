//
//  WGNetworkService.swift
//  Weegle
//
//  Created by 이인재 on 03/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CleanroomLogger

#if DEBUG
let baseAPIURLString = "tapi.weegle.kr"
let baseChatSocketURLString = "http://tchat.weegle.kr:8865"
let baseFileURLString = "http://tstatic.weegle.kr"
#else
let baseAPIURLString = "api.weegle.kr"
let baseChatSocketURLString = "http://chat.weegle.kr:8865"
let baseFileURLString = "http://static.weegle.kr"
#endif

let baseLiveSocketPath = "218.145.161.140"
let baseLiveSocketPort = 8301
let applicationDomain = Bundle.main.bundleIdentifier ?? "kr.ojworld.apps.iOS.Weegle"

class WGNetworkService {

	static var authHeaders: HTTPHeaders? {
		guard let token = WGUserService.token else { return nil }
		
		return ["Authorization": "Bearer \(token)"]
	}
	
	static func resourceURL(uri: String, needsAuth: Bool = false) -> URL? {
		return URL(string: baseFileURLString + uri)
	}
	
	static func error(api: String, json: JSON) -> NSError {
		return NSError(domain: "\(applicationDomain).error.\(api)", code: json["code"].intValue, userInfo: ["message": json["message"].stringValue])
	}
	
	static func createAPIPath(api: String, version: Int = 1, isSecureScheme: Bool = false) -> String {
		return (isSecureScheme ? "https://" : "http://") + "\(baseAPIURLString)/api/v\(version)\(api.hasPrefix("/") ? api : "/" + api)"
	}
	
	@discardableResult
	static func requestJSON(api: String, method: HTTPMethod = .get, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default,
	                        needsAuth: Bool = false, isSecureScheme: Bool = false, success: @escaping (JSON) -> Void, failure: @escaping (NSError?) -> Void) -> DataRequest {
		return Alamofire.request(createAPIPath(api: api, isSecureScheme: isSecureScheme), method: method, parameters: parameters, encoding: encoding, headers: needsAuth ? authHeaders : nil)
			.responseJSON { (response) in
				switch response.result {
				case .success(let value):
					let json = JSON(value)
					
					Log.info?.message("➡️ \(response.request!)")
					Log.info?.message("⬅️ \(response.response!)")
					Log.info?.value(json)
					
					success(json)
				case .failure(let error):
					Log.error?.value(error)
					failure(nil)
				}
		}
	}
}
