//
//  WGDeviceService.swift
//  Weegle
//
//  Created by 이인재 on 05/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import UserNotifications
import FirebaseInstanceID
import CleanroomLogger

class WGDeviceService {

	static let device = UIDevice.current.model
	static let identifier = "WeegleIOS"
	static let keyboardAnimationCurve = 7
	static let keyboardAnimationDuration = 0.25
	
	static var pushToken: String? {
		return InstanceID.instanceID().token()
	}
	
	private init() {}
	
	static func checkFirstRun() {
		let userDefaults = UserDefaults.standard
		let key = "\(appName).device.hasRunBefore"
		
		if userDefaults.bool(forKey: key) == false {
			
			WGUserService.logout()
			
			userDefaults.set(true, forKey: key)
			userDefaults.synchronize()
		}
	}
	
	static func remoteNotificationStatus(_ complete: @escaping (_ isEnabled: Bool) -> Void) {
		if #available(iOS 10.0, *) {
			UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { (settings) in
				DispatchQueue.main.async {
					complete(settings.authorizationStatus == .authorized)
				}
			})
		} else {
			complete(UIApplication.shared.currentUserNotificationSettings?.types.rawValue != 0)
		}
	}
	
	static func registerRemotePush() {
		let application = UIApplication.shared
		
		if #available(iOS 10.0, *) {
			let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
			UNUserNotificationCenter.current().requestAuthorization(options: authOptions,completionHandler: {_, _ in })
		} else {
			let userNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
			application.registerUserNotificationSettings(userNotificationSettings)
		}
		
		application.registerForRemoteNotifications()
	}
	
	static func unregisterRemotePush() {
		UIApplication.shared.unregisterForRemoteNotifications()
	}
	
	// 토큰 갱신
	static func updatePushToken() {
		guard WGUserService.token != nil else { return }
		
		Log.info?.message("Push token: \(WGDeviceService.pushToken ?? "")")
		
		WGNetworkService.requestJSON(api: "/members/regId", method: .put,
		                             parameters: ["regId": WGDeviceService.pushToken ?? "", "device": "ios"],
		                             needsAuth: true, success: { (_) in
			
		}) { (_) in
			
		}
	}
}
