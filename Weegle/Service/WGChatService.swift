//
//  WGChatService.swift
//  Weegle
//
//  Created by 이인재 on 06/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import SocketIO
import SwiftyJSON
import RealmSwift
import Alamofire
import CleanroomLogger

enum WGChatType: Int {
	case normal
	case `private`
}

enum WGChatLiveType: String {
	case back
	case see
	case live
}

class WGChatService {
	
	static let connectedNotification = Notification(name: "\(appName).socket.connected.notification")
	static let openChatNotification = Notification(name: "\(appName).chat.open.notification")
	static let enterChatNotification = Notification(name: "\(appName).chat.enter.notification")
	
	private(set) static var socket: SocketIOClient? = SocketIOClient(socketURL: URL(string: baseChatSocketURLString)!, config: [.log(true), .compress])
	private static var currentChatId: String?
	
	static func connect() {
		guard WGUserService.isLogin else { Log.error?.message("try connect socket after login"); return }
		
		socket?.on(clientEvent: .connect) { _,_  in
			NotificationCenter.default.post(connectedNotification)
			
			socket?.on("onChatInfo", callback: { (data, _) in
				if let first = data.first {
					let json = JSON(first)["onChatInfo"]

					let chat = WGChat(value: json.object)
					chat.save(value: json.object)
				}
			})
			
			socket?.on("onMessageInfo", callback: { (data, _) in
				if let first = data.first {
					let json = JSON(first)["onMessageInfo"].arrayValue
					
					guard let chatId = currentChatId else { return }
					
					let chat = WGChat.chat(id: chatId)
					chat?.add(items: json)
				}
			})
			
			socket?.on("onMessage", callback: { (data, _) in
				let json = JSON(data)["onMessageInfo"].arrayValue
				Log.info?.value(json[0]["message"].stringValue)
			})
		}
		
		socket?.connect()
	}
	
	static func disconnect() {
		socket?.disconnect()
	}

	static func isOpen(chatId: String) -> Bool {
		return currentChatId == chatId
	}
	
	static func open(friendIds: [String], type: WGChatType = .normal, complete: @escaping (String) -> Void) {
		guard let currentUser = WGUserService.current else { return }
		guard let data = try? JSONSerialization.data(withJSONObject: friendIds, options: .prettyPrinted) else { return }
		guard let ids = String(data: data, encoding: .utf8) else { return }
		
		socket?.emit("onCreateChatId", ids, currentUser.objectId, type.rawValue)
		socket?.once("onCreateChatId", callback: { (data, _) in
			if let first = data.first {
				let json = JSON(first)["onCreateChatId"]
				
				Log.info?.value(json)
				
				let chat = WGChat(value: json.object)
				chat.save()
				
				complete(chat.chatId)
			}
		})
	}
	
	static func invite(chatId: String, friendIds: [String]) {
		guard let currentUser = WGUserService.current else { return }
		guard let data = try? JSONSerialization.data(withJSONObject: friendIds, options: .prettyPrinted) else { return }
		guard let ids = String(data: data, encoding: .utf8) else { return }
		
		socket?.emit("onInviteMember", chatId, currentUser.objectId, ids)
	}
	
	static func enter(chatId: String, isSecret: Bool = false, complete: @escaping () -> Void) {
		guard let currentUser = WGUserService.current else { return }
		
		currentChatId = chatId
		
		socket?.emit("onChatInfo", chatId, currentUser.objectId, isSecret ? 1 : 0)
		socket?.once("onChatInfo", callback: { (data, _) in
			if let first = data.first {
				let json = JSON(first)["onChatInfo"]
				
				let chat = WGChat(value: json.object)
				chat.save(value: json.object)
				
				complete()
			}
		})
	}
	
	static func messageList(chatId: String, lastDate: String) {
		socket?.emit("onMessageList", chatId, lastDate)
	}
	
	static func send(chatId: String, chatItem: WGChatItem) {
		guard let currentUser = WGUserService.current else { return }
		
		socket?.emit("onMessage", chatId, chatItem.messageTempId, currentUser.objectId, chatItem.message, chatItem.extern, chatItem.externType, 0)
		
		messageList(chatId: chatId, lastDate: Date().timestamp)
	}
	
	// 채팅 이미지 전송
	static func imageMessage(chatId: String, messageTempId: String, image: UIImage,
	                         success: @escaping (JSON) -> Void,
	                         progress progressClosure: @escaping (Double) -> Void,
	                         failure: @escaping (NSError?) -> Void) {
		guard let imageData = UIImageJPEGRepresentation(image, 0.7) else { failure(nil); return }
		
		Alamofire.upload(multipartFormData: { (multipartFormData) in
			multipartFormData.append(chatId.data(using: .utf8)!, withName: "chatId")
			multipartFormData.append(messageTempId.data(using: .utf8)!, withName: "messageTempId")
			multipartFormData.append(imageData, withName: "image", fileName: "image.jpg", mimeType: "image/jpg")
		}, to: WGNetworkService.createAPIPath(api:"/imageMessage", isSecureScheme: true),
		   headers: WGNetworkService.authHeaders,
		   encodingCompletion: { (encodingResult) in
			switch encodingResult {
			case .success(let uploadRequest, _, _):
				uploadRequest.uploadProgress(closure: { (progress) in
					progressClosure(progress.fractionCompleted)
				})
				
				uploadRequest.responseJSON { response in
					switch response.result {
					case .success(let value):
						let json = JSON(value)
						
						Log.info?.value(json)
						
						if let statusCode = response.response?.statusCode {
							switch statusCode {
							case 200:
								guard let extern = json["extern"].rawString() else {
									failure(NSError(domain: "chatting.image", code: json["code"].intValue, userInfo: ["message": "이미지 전송 후 extern 취득 실패"]))
									
									return
								}
								
								let chatItem = WGChatItem()
								chatItem.messageTempId = messageTempId
//								chatItem.message = ""
								chatItem.externType = ChatExternType.image.rawValue
								chatItem.extern = extern
								
								send(chatId: chatId, chatItem: chatItem)
								
								success(json)
							default:
								failure(NSError(domain: "chatting.image", code: json["code"].intValue, userInfo: ["message": json["message"].stringValue]))
							}
						}
					case .failure(let error):
						Log.error?.value(error)
						failure(nil)
					}
				}
			case .failure(let error):
				Log.error?.value(error)
				failure(nil)
			}
		})
	}
	
	// 채팅 동영상 전송
	static func videoMessage(chatId: String, messageTempId: String, videoURL: URL,
	                         success: @escaping (JSON) -> Void,
	                         progress progressClosure: @escaping (Double) -> Void,
	                         failure: @escaping (NSError?) -> Void) {
		guard let movieData = try? Data.init(contentsOf: videoURL, options: .mappedIfSafe) else { failure(nil); return }
		
		Alamofire.upload(multipartFormData: { (multipartFormData) in
			multipartFormData.append(chatId.data(using: .utf8)!, withName: "chatId")
			multipartFormData.append(messageTempId.data(using: .utf8)!, withName: "messageTempId")
			multipartFormData.append(videoURL.lastPathComponent.data(using: .utf8)!, withName: "videoOriginName")
			multipartFormData.append(videoURL.absoluteString.data(using: .utf8)!, withName: "videoOriginPath")
			multipartFormData.append(movieData, withName: "video", fileName: videoURL.lastPathComponent, mimeType: "video/mov")
		}, to: WGNetworkService.createAPIPath(api:"/videoMessage", isSecureScheme: true),
		   headers: WGNetworkService.authHeaders,
		   encodingCompletion: { (encodingResult) in
			switch encodingResult {
			case .success(let uploadRequest, _, _):
				uploadRequest.uploadProgress(closure: { (progress) in
					progressClosure(progress.fractionCompleted)
				})
				
				uploadRequest.responseJSON { response in
					switch response.result {
					case .success(let value):
						let json = JSON(value)
						
						Log.info?.value(json)
						
						if let statusCode = response.response?.statusCode {
							switch statusCode {
							case 200:
								guard let extern = json["extern"].rawString() else {
									failure(NSError(domain: "chatting.video", code: json["code"].intValue, userInfo: ["message": "동영상 전송 후 extern 취득 실패"]))
									
									return
								}
								
								let chatItem = WGChatItem()
								chatItem.messageTempId = messageTempId
								//								chatItem.message = ""
								chatItem.externType = ChatExternType.video.rawValue
								chatItem.extern = extern
								
								send(chatId: chatId, chatItem: chatItem)
								
								success(json)
							default:
								failure(NSError(domain: "chatting.video", code: json["code"].intValue, userInfo: ["message": json["message"].stringValue]))
							}
						}
					case .failure(let error):
						Log.error?.value(error)
						failure(nil)
					}
				}
			case .failure(let error):
				Log.error?.value(error)
				failure(nil)
			}
		})
	}
	
	static func openLive(chatId: String, type: WGChatLiveType, success: @escaping (JSON) -> Void, failure: @escaping (NSError?) -> Void) {
		guard let current = WGUserService.current else { failure(nil); return }
		
		WGNetworkService.requestJSON(api: "/liveStateChecking",
		                             method: .post,
		                             parameters: ["chatId": chatId, "memberObjectId": current.objectId, "type": type.rawValue],
		                             needsAuth: true,
		                             success: { json in
										success(json["value"])
		}) { (error) in
			failure(error)
		}
	}
	
	static func imageList(chatId: String, success: @escaping (JSON) -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/chats/\(chatId)/messages/images",
		                             needsAuth: true,
		                             success: { json in
										success(json)
		}) { (error) in
			failure(error)
		}
	}
	
	static func change(title: String, chatId: String, success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
		guard let current = WGUserService.current else { failure(nil); return }
		
		WGNetworkService.requestJSON(api: "/chatTitleChange", method: .post, parameters: ["chatId": chatId, "myObjectId": current.objectId, "title": title], needsAuth: true, success: { (_) in
			success()
		}) { (error) in
			failure(error)
		}
	}
	
	static func leave(chatId: String) {
		guard let currentUser = WGUserService.current else { return }
		
		currentChatId = nil
		
		socket?.emit("onChatLeave", chatId, currentUser.objectId)
	}
	
	static func destroy(chatId: String) {
		guard let currentUser = WGUserService.current else { return }
		
		currentChatId = nil
		
		socket?.emit("onMessage", chatId, 0, currentUser.objectId, "", "", ChatExternType.leave.rawValue, 0)
	}
	
	static func leaveCurrent() {
		guard let chatId = currentChatId else { return }
		
		leave(chatId: chatId)
	}
}
