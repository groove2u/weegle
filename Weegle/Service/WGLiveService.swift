//
//  WGLiveService.swift
//  Weegle
//
//  Created by 이인재 on 11/09/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import Alamofire
import SocketIO
import CleanroomLogger

class WGLiveService {

	private static var socket: SocketIOClient? {
		return WGChatService.socket
	}
	
	private init() {}
	
	// 방송 생성
	static func open(categoryId: String, tags: [String], thumb: UIImage, inviteMember: [String], isSecret: Bool = false, isPublic: Bool = true,
	                 success: @escaping (Int64, JSON) -> Void, failure: @escaping (NSError?) -> Void) {
		guard let thumbData = UIImageJPEGRepresentation(thumb, 0.85) else { failure(nil); return }
		
		Alamofire.upload(multipartFormData: { (multipartFormData) in
			multipartFormData.append(categoryId.data(using: .utf8)!, withName: "category")
			multipartFormData.append(tags.joined(separator: ",").data(using: .utf8)!, withName: "tags")
			multipartFormData.append(inviteMember.joined(separator: ",").data(using: .utf8)!, withName: "invitedMember")
			multipartFormData.append((isSecret ? "1" : "0").data(using: .utf8)!, withName: "secret")
			multipartFormData.append((isPublic ? "1" : "0").data(using: .utf8)!, withName: "type")
			multipartFormData.append(thumbData, withName: "thumb", fileName: "thumb.jpg", mimeType: "image/jgp")
			
			Log.info?.value(multipartFormData)
		}, to: WGNetworkService.createAPIPath(api:"/chats/public", isSecureScheme: true),
		   headers: WGNetworkService.authHeaders,
		   encodingCompletion: { (encodingResult) in
			switch encodingResult {
			case .success(let uploadRequest, _, _):
				uploadRequest.responseJSON { response in
					switch response.result {
					case .success(let value):
						let json = JSON(value)
						
						Log.info?.value(json)
						
						if let statusCode = response.response?.statusCode {
							switch statusCode {
							case 200:
								var buffer = json["chat"]
								buffer["chatId"].stringValue = buffer["id"].stringValue
								buffer["lastDate"].stringValue = buffer["date"].stringValue
								buffer["title"].stringValue = buffer["publicTitle"].stringValue
								
								let chat = WGChat(value: buffer.object)
								chat.save(value: buffer.object)
								
								success(Int64(json["live"]["roomId"].intValue), json)
							default:
								failure(NSError(domain: "LiveDomain", code: json["code"].intValue, userInfo: ["message": json["message"].stringValue]))
							}
						}
					case .failure(let error):
						Log.error?.value(error)
						failure(nil)
					}
				}
			case .failure(let error):
				Log.error?.value(error)
				failure(nil)
			}
		})
	}
	
	static func category(success: @escaping (JSON) -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/category", method: .get, needsAuth: true, success: { (json) in
			Log.info?.value(json)
			
			success(json)
		}, failure: { (error) in
			Log.error?.value(error)
			
			failure(nil)
		})
	}
	
	static func open(chatId: String) {
		guard let currentUser = WGUserService.current else { return }
		
		let param = ["chatId": chatId, "memberObjectId": currentUser.objectId]
		
		if let jsonString = param.jsonString {
			socket?.emit("onOpenLiveInfo", jsonString)
			socket?.once("onOpenLiveInfo", callback: { (data, _) in
				if let first = data.first {
					let json = JSON(first)
					
					Log.info?.value(json)
				}
			})
			socket?.on("onOpenLiveMessage", callback: { (data, _) in
				if let first = data.first {
					let json = JSON(first)["onOpenLiveMessage"].arrayValue
					
					let chat = WGChat.chat(id: chatId)
					chat?.add(items: json)
				}
			})
		}
	}
	
	static func send(chatId: String, chatInfo: [String: Any]) {
		guard let jsonString = chatInfo.jsonString else { return }
		
		socket?.emit("onOpenLiveMessage", jsonString)
	}
	
	static func list(search: String = "", lastId: String = "", liveFlag: Int? = nil, success: @escaping (JSON) -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/chats/public", method: .get, needsAuth: true, success: { (json) in
			Log.info?.value(json)
			
			success(json)
		}, failure: { (error) in
			Log.error?.value(error)
			
			failure(nil)
		})
	}
	
	static func close(chatId: String, isBJ: Bool = false) {
		guard let currentUser = WGUserService.current else { return }
		
		let param: [String: Any] = ["chatId": chatId, "memberObjectId": currentUser.objectId, "userType": isBJ ? 1 : 0]
		
		if let jsonString = param.jsonString {
			socket?.emit("onOpenLiveLeave", jsonString)
			socket?.once("onOpenLiveLeave", callback: { (data, _) in
				if let first = data.first {
					let json = JSON(first)
					
					Log.info?.value(json)
				}
			})
		}
	}
}
