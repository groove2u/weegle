//
//  WGUserService.swift
//  Weegle
//
//  Created by 이인재 on 2017.07.15.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleSignIn
import FBSDKLoginKit
import CleanroomLogger
import RealmSwift
import SwiftKeychainWrapper
import SocketIO

enum WGLoginType: String {
	case facebook
	case google
	case `default`
}

enum WGLoginStatus {
	case logIn
	case logOut
}

protocol WGLoginDelegate: class {
	
	func didLoginStatusChanged(status: WGLoginStatus)
}

class WGUserService {

	static let loginCompleteNotification = Notification(name: "login.complete.notification")
	static let logoutCompleteNotification = Notification(name: "logout.complete.notification")
	
	static var current: WGUser? {
		didSet {
			current?.save()
		}
	}
	static var isLogin: Bool {
		guard token != nil else { Log.error?.message("not logged in"); return false }
		
		return true
	}
	static private(set) var token: String? {
		get {
			guard let validToken = KeychainWrapper.standard.string(forKey: "\(appName).login.token") else { return nil }
			guard let lastDate = lastLoginDate else { return nil }
			guard let expireDate = Calendar.current.date(byAdding: .day, value: 30, to: lastDate), expireDate > lastDate else { logout(); return nil }
			
			return validToken
		}
		
		set {
			if let newToken = newValue {
				lastLoginDate = Date()
				KeychainWrapper.standard.set(newToken, forKey: "\(appName).login.token")
			} else {
				lastLoginDate = nil
				KeychainWrapper.standard.removeObject(forKey: "\(appName).login.token")
			}
		}
	}
	static var configuration: Realm.Configuration? {
		guard let currentUser = WGUserService.current else { return nil }
		guard let supportDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
		guard let keyData = realmKeyData else { return nil }
		
//		Log.info?.value(keyData.hexString)
		
		let destinationURL = supportDirectoryURL.appendingPathComponent(".\(currentUser.objectId).weegle.realm")
		
		return Realm.Configuration(fileURL: destinationURL, encryptionKey: keyData)
	}
	static var realm: Realm? {
		guard let configuration = configuration else { Log.error?.message("error getting realm configuration"); return nil }
		
		do {
			return try Realm(configuration: configuration)
		} catch {
			Log.error?.value(error)
		}
		
		return nil
	}
	static var loginType: WGLoginType? {
		get {
			guard let type = UserDefaults.standard.string(forKey: "\(appName).login.type") else { return nil }
			return WGLoginType(rawValue: type)
		}
		
		set {
			UserDefaults.standard.set(newValue?.rawValue, forKey: "\(appName).login.type")
			UserDefaults.standard.synchronize()
		}
	}
	
	private static var realmKeyData: Data? {
		guard let currentUser = WGUserService.current else { return nil }
		
		if KeychainWrapper.standard.data(forKey: currentUser.objectId) == nil {
			var keyData = Data(count: 64)
			let result = keyData.withUnsafeMutableBytes { (mutableBytes: UnsafeMutablePointer<UInt8>) -> Int32 in
				SecRandomCopyBytes(kSecRandomDefault, keyData.count, mutableBytes)
			}
			
			guard result == errSecSuccess else { return nil }
			
			KeychainWrapper.standard.set(keyData, forKey: currentUser.objectId)
		}
		
		return KeychainWrapper.standard.data(forKey: currentUser.objectId)
	}
	private static var lastLoginDate: Date? {
		get {
			guard let since1970: TimeInterval = KeychainWrapper.standard.double(forKey: "\(appName).date.last.login") else { return nil }
			
			return Date(timeIntervalSince1970: since1970)
		}
		
		set {
			if let newDate = newValue {
				KeychainWrapper.standard.set(newDate.timeIntervalSince1970, forKey: "\(appName).date.last.login")
			} else {
				KeychainWrapper.standard.removeObject(forKey: "\(appName).date.last.login")
			}
		}
	}
	
	private init() {}
	
	// 인증번호 요청
	static func phoneNumberAuthCode(nationalCode: String, phoneNumber: String, success: @escaping (String) -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/public/sms/auth", method: .post, parameters: ["phone": phoneNumber, "nationalCode": nationalCode], success: { (json) in
			success(json["id"].stringValue)
		}) { (error) in
			failure(error)
		}
	}
	
	// 인증번호 확인
	static func verifyAuthCode(authId: String, code: String, success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/public/sms/auth/\(authId)", method: .post, parameters: ["authNumber": code], success: { (json) in
			switch json["code"].stringValue {
			case "1":
				success()
			default:
				failure(WGNetworkService.error(api: "phoneNumber.auth", json: json))
			}
		}) { (error) in
			failure(error)
		}
	}
	
	// 중복 확인
	static func checkDuplication(nickName: String, success: @escaping (Bool) -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/public/duplication/\(nickName)", success: { (json) in
			switch json["code"].intValue {
			case 200:
				success(true)
			default:
				success(false)
			}
		}) { (_) in
			failure(nil)
		}
	}
	
	// 회원 가입
	static func join(user: WGUser, success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
		guard let qrImageData = user.qrCodeImageData else { failure(nil); return }
		
		Alamofire.upload(multipartFormData: { (multipartFormData) in
			multipartFormData.append(user.memberId.data(using: .utf8)!, withName: "id")
			multipartFormData.append(user.memberName.data(using: .utf8)!, withName: "name")
			multipartFormData.append(user.password.data(using: .utf8)!, withName: "password")
			multipartFormData.append(user.nationalCode.data(using: .utf8)!, withName: "nationalCode")
			multipartFormData.append(user.phoneNumber.data(using: .utf8)!, withName: "phoneNumber")
			multipartFormData.append("ios".data(using: .utf8)!, withName: "device")
			multipartFormData.append(qrImageData, withName: "barcodeImage", fileName: "file.jpg", mimeType: "image/jgp")
			
			Log.info?.value(multipartFormData)
		}, to: WGNetworkService.createAPIPath(api:"/public/joinWithBarcode", isSecureScheme: true)) { (encodingResult) in
			switch encodingResult {
			case .success(let uploadRequest, _, _):
				uploadRequest.responseJSON { response in
					switch response.result {
					case .success(let value):
						let json = JSON(value)
						
						Log.info?.value(json)
						
						if let statusCode = response.response?.statusCode {
							switch statusCode {
							case 200:
								success()
							default:
								failure(NSError(domain: "JoinDomain", code: json["code"].intValue, userInfo: ["message": json["message"].stringValue]))
							}
						}
					case .failure(let error):
						Log.error?.value(error)
						failure(nil)
					}
				}
			case .failure(let error):
				Log.error?.value(error)
				failure(nil)
			}
		}
	}
	
	// 소셜 회원 가입
	static func join(user: WGUser, type: WGLoginType, token: String, authCode: String, success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
		guard type != .default else {
			join(user: user, success: success, failure: failure)
			
			return
		}
		
		guard let qrImage = WGUser.QRCodeImage(message: type == .facebook ? "facebook_\(user.id)" : "google_\(user.id)") else { failure(nil); return }
		guard let qrImageData = UIImageJPEGRepresentation(qrImage, 0.85) else { failure(nil); return }
		
		Alamofire.upload(multipartFormData: { (multipartFormData) in
			multipartFormData.append(token.data(using: .utf8)!, withName: "accessToken")
			multipartFormData.append(user.nationalCode.data(using: .utf8)!, withName: "nationalCode")
			multipartFormData.append(user.phoneNumber.data(using: .utf8)!, withName: "phoneNumber")
			multipartFormData.append(user.memberName.data(using: .utf8)!, withName: "memberName")
			multipartFormData.append(authCode.data(using: .utf8)!, withName: "authNumber")
			multipartFormData.append("ios".data(using: .utf8)!, withName: "device")
			multipartFormData.append(qrImageData, withName: "barcodeImage", fileName: "file.jpg", mimeType: "image/jgp")
			
			Log.info?.value(multipartFormData)
		}, to: WGNetworkService.createAPIPath(api: type == .facebook ? "/public/member/facebook" : "/public/member/google")) { (encodingResult) in
			switch encodingResult {
			case .success(let uploadRequest, _, _):
				uploadRequest.responseJSON { response in
					switch response.result {
					case .success(let value):
						let json = JSON(value)
						
						Log.info?.value(json)
						
						if json["accessToken"].exists() {
							success()
						} else {
							if json["code"].intValue == 4001 {
								login(type: .facebook, snsToken: token, success: {
									success()
								}, failure: { (error) in
									failure(error)
								})
							} else {
								failure(NSError(domain: "JoinDomain", code: json["code"].intValue, userInfo: ["message": json["message"].stringValue]))
							}
						}
					case .failure(let error):
						Log.error?.value(error)
						failure(nil)
					}
				}
			case .failure(let error):
				Log.error?.value(error)
				failure(nil)
			}
		}
	}
	
	// 프로필 이미지 변경
	static func uploadProfileImage(image: UIImage, success: @escaping () -> Void, progress progressClosure: @escaping (Double) -> Void, failure: @escaping (NSError?) -> Void) {
		guard let current = WGUserService.current else { failure(nil); return }
		guard let imageData = UIImageJPEGRepresentation(image, 0.85) else { failure(nil); return }
		
		let id = current.objectId
		
		Alamofire.upload(multipartFormData: { (multipartFormData) in
			multipartFormData.append(id.data(using: .utf8)!, withName: "id")
			multipartFormData.append(imageData, withName: "profileImage", fileName: "file.jpg", mimeType: "image/jgp")
		}, to: WGNetworkService.createAPIPath(api:"/profileImageModify", isSecureScheme: true),
		   headers: WGNetworkService.authHeaders,
		   encodingCompletion: { (encodingResult) in
			switch encodingResult {
			case .success(let uploadRequest, _, _):
				uploadRequest.uploadProgress(closure: { (progress) in
					progressClosure(progress.fractionCompleted)
				})
				
				uploadRequest.responseJSON { response in
					switch response.result {
					case .success(let value):
						let json = JSON(value)
						
						Log.info?.value(json)
						
						if let statusCode = response.response?.statusCode {
							switch statusCode {
							case 200:
								WGUserService.myInfo(success: { (json) in
									NotificationCenter.default.post(name: NSNotification.Name.UserProfileChanged, object: nil, userInfo: ["objectId": id])
									
									success()
								}, failure: { (_) in
									failure(WGNetworkService.error(api: "login", json: json))
								})
							default:
								failure(NSError(domain: "UpdateProfileDomain", code: json["code"].intValue, userInfo: ["message": json["message"].stringValue]))
							}
						}
					case .failure(let error):
						Log.error?.value(error)
						failure(nil)
					}
				}
			case .failure(let error):
				Log.error?.value(error)
				failure(nil)
			}
		})
	}
	
	// 프로필 수정
	static func updateProfileInfo(name: String, comment: String, success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
		guard let id = WGUserService.current?.objectId else { failure(nil); return }
		
		WGNetworkService.requestJSON(api: "/profileModify", method: .post, parameters: ["id": id, "profileName": name, "profileComment": comment], needsAuth: true, isSecureScheme: true, success: { (json) in
			if json["result"].stringValue == "success" {
				WGUserService.myInfo(success: { (json) in
					NotificationCenter.default.post(name: NSNotification.Name.UserProfileChanged, object: nil, userInfo: ["objectId": id])
					
					success()
				}, failure: { (_) in
					failure(WGNetworkService.error(api: "login", json: json))
				})
			} else {
				failure(nil)
			}
		}) { (error) in
			failure(error)
		}
	}
	
	// 소셜 로그인
	static func login(type: WGLoginType, snsToken: String, success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
		guard type != .default else { failure(nil); return }
		
		let target = type == .facebook ? "facebook" : "google"
		
		WGNetworkService.requestJSON(api: "/public/member/login/\(target)",
			method: .post,
			parameters: ["accessToken": snsToken, "regId": WGDeviceService.pushToken ?? "", "device": "ios"],
			isSecureScheme: true,
			success: { (json) in
				if let accessToken = json["accessToken"].string {
					token = accessToken
					loginType = type
					
					WGUserService.myInfo(success: { (json) in
						NotificationCenter.default.post(loginCompleteNotification)
						
						success()
					}, failure: { (_) in
						failure(WGNetworkService.error(api: "login", json: json))
					})
				} else {
					failure(WGNetworkService.error(api: "login", json: ["code": json["code"], "message": json["message"].stringValue]))
				}
		}) { (error) in
			failure(error)
		}
	}
	
	// 로그인
	static func login(id: String, password: String, success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/login", method: .post, parameters: ["id": id, "password": password, "regId": WGDeviceService.pushToken ?? ""], isSecureScheme: true, success: { (json) in
			let resultCode = json["result"].intValue
			switch resultCode {
			case 0:
				failure(WGNetworkService.error(api: "login", json: ["code": resultCode, "message": "가입되지 않은 아이디입니다."]))
			case 1:
				failure(WGNetworkService.error(api: "login", json: ["code": resultCode, "message": "비밀번호 오류입니다."]))
			case 2:
				token = json["token"].stringValue
				loginType = .default
				
				WGUserService.myInfo(success: { (json) in
					NotificationCenter.default.post(loginCompleteNotification)
					
					success()
				}, failure: { (_) in
					failure(WGNetworkService.error(api: "login", json: json))
				})
			default:
				failure(WGNetworkService.error(api: "login", json: json))
			}
		}) { (error) in
			failure(error)
		}
	}
	
	// 토큰 갱신
	static func refreshToken(success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
		guard let validToken = WGUserService.token else { failure(nil); return }
		
		Log.info?.message("Push token: \(WGDeviceService.pushToken ?? "")")
		
		WGNetworkService.requestJSON(api: "/auth/refresh", method: .post, parameters: ["token": validToken, "regId": WGDeviceService.pushToken ?? ""], isSecureScheme: true, success: { (json) in
			if let refreshToken = json["refreshToken"].string {
				token = refreshToken
				
				WGUserService.myInfo(success: { (json) in
					NotificationCenter.default.post(loginCompleteNotification)
					
					success()
				}, failure: { (_) in
					failure(WGNetworkService.error(api: "login", json: json))
				})
			} else {
				failure(WGNetworkService.error(api: "refresh token", json: json))
			}
		}) { (error) in
			failure(error)
		}
	}
	
	// 로그아웃
	static func logout() {
		current = nil
		token = nil
		loginType = nil
		
		if FBSDKAccessToken.current() != nil {
			FBSDKLoginManager().logOut()
			FBSDKAccessToken.setCurrent(nil)
		}
		
		if GIDSignIn.sharedInstance().currentUser != nil {
			GIDSignIn.sharedInstance().signOut()
			GIDSignIn.sharedInstance().disconnect()
		}
		
		NotificationCenter.default.post(logoutCompleteNotification)
	}
	
	// 회원 탈퇴
	static func withdraw(success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/member", method: .delete, needsAuth: true, success: { (json) in
			if let resultCode = json["status"].int, resultCode == 0 || resultCode == 5001 {
				logout()
					
				success()
			} else {
				failure(WGNetworkService.error(api: "withdraw", json: json))
			}
		}) { (error) in
			failure(error)
		}
	}
	
	// 본인 정보 조회
	static func myInfo(success: @escaping (JSON) -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/profile", needsAuth: true, isSecureScheme: true, success: { (json) in
			current = WGUser(value: json.object)
			
			success(json)
		}) { (error) in
			failure(error)
		}
	}
	
	// 회원 이미지 삭제
	static func deleteProfileImage(success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/profileImageModify", method: .delete, needsAuth: true, success: { (json) in
			success()
		}) { (error) in
			failure(error)
		}
	}
	
	static func updatePassword(current: String, to newPassword: String, success: @escaping () -> Void, failure: @escaping (String?) -> Void) {
		guard let currentUser = WGUserService.current else { failure(nil); return }
		
		WGNetworkService.requestJSON(api: "/passwordModify",
		                             method: .post,
		                             parameters: ["id": currentUser.memberId, "password": newPassword, "oldPassword": current],
		                             needsAuth: true, isSecureScheme: true, success: { (json) in
			if json["code"].exists() {
				failure(json["message"].string)
			} else if json["result"].stringValue == "success" {
				success()
			}
		}) { (error) in
			failure(error?.localizedDescription)
		}
	}
	
	// 문의하기
	static func ask(contents: String, email: String, image: UIImage?, success: @escaping () -> Void, progress progressClosure: @escaping (Double) -> Void, failure: @escaping (NSError?) -> Void) {
		guard WGUserService.current != nil else { failure(nil); return }
		
		Alamofire.upload(multipartFormData: { (multipartFormData) in
			multipartFormData.append(contents.data(using: .utf8)!, withName: "contents")
			multipartFormData.append(email.data(using: .utf8)!, withName: "email")
			
			if let image = image, let imageData = UIImageJPEGRepresentation(image, 0.85) {
				multipartFormData.append(imageData, withName: "attachFile", fileName: "file.jpg", mimeType: "image/jgp")
			}
		}, to: WGNetworkService.createAPIPath(api:"/inquiry", isSecureScheme: true),
		   headers: WGNetworkService.authHeaders,
		   encodingCompletion: { (encodingResult) in
			switch encodingResult {
			case .success(let uploadRequest, _, _):
				uploadRequest.uploadProgress(closure: { (progress) in
					progressClosure(progress.fractionCompleted)
				})
				
				uploadRequest.responseJSON { response in
					switch response.result {
					case .success:
						success()
					case .failure(let error):
						Log.error?.value(error)
						failure(nil)
					}
				}
			case .failure(let error):
				Log.error?.value(error)
				failure(nil)
			}
		})
	}
}
