//
//  WGAppService.swift
//  Weegle
//
//  Created by 이인재 on 04/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import UserNotifications
import SwiftyJSON
import CleanroomLogger

class WGAppService {

	private init() {}
	
	static func version(success: @escaping (JSON) -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/public/version", success: { (json) in
			success(json)
		}) { (error) in
			failure(error)
		}
	}
	
	static func help(success: @escaping ([JSON]) -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/help", needsAuth: true, success: { (json) in
			success(json["content"].arrayValue)
		}) { (error) in
			failure(error)
		}
	}
	
	static func notice(success: @escaping ([JSON]) -> Void, failure: @escaping (NSError?) -> Void) {
		WGNetworkService.requestJSON(api: "/notice", needsAuth: true, success: { (json) in
			success(json["content"].arrayValue)
		}) { (error) in
			failure(error)
		}
	}
}
