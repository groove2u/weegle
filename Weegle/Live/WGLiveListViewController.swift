//
//  WGLiveListViewController.swift
//  Weegle
//
//  Created by 이인재 on 15/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import SwiftyJSON
import MobileCoreServices
import Kingfisher
import CleanroomLogger

class WGLiveListViewController: WGBaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

	@IBOutlet var searchBar: UISearchBar!
	@IBOutlet weak private var tableView: UITableView!
	@IBOutlet weak private var noSearchResultView: UIView!
	@IBOutlet weak private var noLiveStackView: UIStackView!
	
	fileprivate var models: [JSON] = []
	
	private let refreshControl = UIRefreshControl()
	
	override func prepareUI() {
		super.prepareUI()
		
		refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)

		tableView.addSubview(refreshControl)
		
		fetch()
	}

	override func updateUI() {
		super.updateUI()
		
		navigationController?.setNavigationBarHidden(false, animated: true)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "SetupLiveSegue",
			let viewController = segue.destination as? WGLiveSetupViewController,
			let image = sender as? UIImage {
			viewController.backgroundImage = image
		}
	}

	@objc private func refreshData() {
		fetch()
	}
	
	override func fetch() {
		WGLiveService.list(success: { [weak self] (json) in
			self?.models = json.arrayValue.filter { $0["liveFlag"].intValue == 1 }
			
			self?.updateDataUI()
		}) { (_) in
			
		}
	}
	
	override func updateDataUI() {
		tableView.reloadData()
		
		refreshControl.endRefreshing()
	}
	
	// MARK: - Action
	
	@IBAction func createLive() {
		selectImage()
	}
	
	@IBAction func toggleSearch(_ sender: Any) {
		if searchBar.superview == nil {
			navigationItem.titleView = searchBar
		} else {
			navigationItem.titleView = nil
		}
	}
	
	// MARK: - Custom
	
	private func selectImage() {
		let picker = UIImagePickerController()
		picker.mediaTypes = [kUTTypeImage as String]
		picker.allowsEditing = true
		picker.delegate = self
		
		let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		
		sheet.addAction(UIAlertAction(title: "카메라", style: .default, handler: { (_) in
			picker.sourceType = .camera
			
			self.present(picker, animated: true)
		}))
		sheet.addAction(UIAlertAction(title: "앨범", style: .default, handler: { (_) in
			picker.sourceType = .photoLibrary
			
			self.present(picker, animated: true)
		}))
		sheet.addAction(UIAlertAction(title: "cancel".localized, style: .cancel))
		
		present(sheet, animated: true)
	}
	
	// MARK: - UIImagePickerControllerDelegate
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else { return }
		
		picker.dismiss(animated: false) {
			DispatchQueue.main.async {
				self.performSegue(withIdentifier: "SetupLiveSegue", sender: image)
			}
		}
	}
	
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		picker.dismiss(animated: true)
	}
}

extension WGLiveListViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		noLiveStackView.isHidden = models.isEmpty == false
		
		return models.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard indexPath.item < models.count,
		let cell = tableView.dequeueReusableCell(withIdentifier: WGLiveTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGLiveTableViewCell else { return UITableViewCell() }
		
		let model = models[indexPath.item]
		
		cell.coverImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: model["thumbPath"].stringValue))
		cell.categoryLabel.text = model["categoryName"].string
		cell.tagLabel.text = model["tags"].arrayValue.map { $0.stringValue }.joined(separator: ", ")
		cell.tagSeperator.isHidden = model["tags"].arrayValue.isEmpty
		cell.viewerCountLabel.text = model["livecnt"].stringValue
		cell.ownerProfileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: model["owner"]["profileImage"].stringValue), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
		cell.ownerNameLabel.text = model["owner"]["memberName"].string
		cell.secretView.isHidden = model["secret"].intValue == 0
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let model = models[indexPath.item]
		
		DispatchQueue.main.async {
			if let viewController = self.storyboard?.instantiateViewController(withIdentifier: String(describing: BroadcastViewController.self)) as? BroadcastViewController {
				viewController.mRoomId = model["roomId"].int64Value
				viewController.mUserId = WGUserService.current?.memberId
				viewController.mMainUserId = model["owner"]["memberId"].stringValue
				viewController.chatInfo = model
				
				self.present(viewController, animated: true)
			}
		}
	}
}
