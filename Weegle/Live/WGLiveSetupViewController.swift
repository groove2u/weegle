//
//  WGLiveSetupViewController.swift
//  Weegle
//
//  Created by 이인재 on 15/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import SwiftyJSON
import CleanroomLogger

enum LiveAccessLevel: String {
	case `public` = "전체공개 (누구나 방송시청 가능)"
	case `private` = "비공개 방송"
}

class WGLiveSetupViewController: WGBaseViewController {

	@IBOutlet weak private var backgroundImageView: UIImageView!
	@IBOutlet weak private var accessLevelButton: UIButton!
	@IBOutlet weak private var categoryButton: UIButton!
	@IBOutlet weak private var tagButton: UIButton!
	@IBOutlet weak private var optionStackView: UIStackView!
	@IBOutlet weak private var beginLiveButton: UIButton!
	@IBOutlet weak private var countNoticeLabel: UILabel!
	@IBOutlet weak private var countImageView: UIImageView!
	@IBOutlet weak private var countLabel: UILabel!
	
	var backgroundImage: UIImage?
	var invitedFriendObjectIds = [String]()
	var accessLevel = LiveAccessLevel.public {
		didSet {
			if isViewLoaded {
				accessLevelButton.setTitle(accessLevel.rawValue, for: .normal)
			}
		}
	}
	
	private let tagAlert: UIAlertController = {
		let alert = UIAlertController(title: "태그입력", message: "헤시태그를 입력해주세요.\n#으로 구분, 최대 5개 입력 가능합니다.", preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel))
		
		return alert
	}()
	
	private var tagConfirmAction: UIAlertAction?
	private var categories = [JSON]()
	private var categoryId: String?
	private var timer: Timer?
	
	override var prefersStatusBarHidden: Bool {
		return true
	}

	override func prepareUI() {
		super.prepareUI()
		
		navigationController?.setNavigationBarHidden(true, animated: true)
		
		NotificationCenter.default.addObserver(self, selector: #selector(tagTextChanged(notification:)), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
		
		backgroundImageView.image = backgroundImage
		accessLevelButton.setTitle(accessLevel.rawValue, for: .normal)
		
		updateStatus(isCount: false)
		
		WGLiveService.category(success: { [weak self] (json) in
			self?.categories = json.arrayValue
			
			if let category = self?.categories.first {
				self?.categoryButton.setTitle(category["categoryName"].stringValue, for: .normal)
				self?.categoryId = category["id"].string
			}
		}) { (_) in
			
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "AccessLevelSegue",
			let viewController = segue.destination as? WGLiveAccessLevelViewController {
			viewController.accessLevel = accessLevel
			viewController.invitedFriendObjectIds = invitedFriendObjectIds
		}
	}
	
	// MARK: - Action
	
	@IBAction func selectCategory(_ sender: UIButton) {
		let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

		for category in categories {
			sheet.addAction(UIAlertAction(title: category["categoryName"].stringValue, style: .default) { (_) in
				sender.setTitle(category["categoryName"].stringValue, for: .normal)
				self.categoryId = category["id"].stringValue
			})
		}
		
		sheet.addAction(UIAlertAction(title: "cancel".localized, style: .cancel))
		
		present(sheet, animated: true)
	}
	
	@IBAction func addTag(_ sender: UIButton) {
		if tagAlert.textFields?.isEmpty == true {
			tagAlert.addTextField { (textField) in
				textField.delegate = self
				textField.text = "#"
			}
		}

		if tagConfirmAction == nil {
			tagConfirmAction = UIAlertAction(title: "confirm".localized, style: .default, handler: { (_) in
				if let textField = self.tagAlert.textFields?.first,
					let text = textField.text,
					let tags = self.tags(text: text) {
					if textField.text?.hasPrefix("#") == false {
						textField.text?.insert("#", at: textField.text!.startIndex)
					}
					
					self.tagButton.setTitle(tags.joined(separator: ", "), for: .normal)
				}
			})
			tagAlert.addAction(tagConfirmAction!)
		}
		
		validateTags(text: tagAlert.textFields?.first?.text ?? "")
		
		present(tagAlert, animated: true)
	}
	
	private func updateStatus(isCount: Bool) {
		countNoticeLabel.isHidden = isCount == false
		countLabel.isHidden = isCount == false
		countImageView.isHidden = isCount == false

		optionStackView.isHidden = isCount
		beginLiveButton.isHidden = isCount
	}
	
	@IBAction func complete() {
		updateStatus(isCount: true)
		
		countLabel.text = "3"
		
		timer?.invalidate()
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
			self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer(timer:)), userInfo: nil, repeats: true)
		}
	}
	
	@objc private func updateTimer(timer: Timer) {
		guard let categoryId = categoryId else { return }
		guard let tagString = tagButton.title(for: .normal) else { return }
		guard let image = backgroundImage else { return }
		
		if var time = Int(countLabel.text!) {
			if time > 0 {
				time -= 1
				
				countLabel.text = "\(time)"
			} else {
				self.timer?.invalidate()
				
				WGLiveService.open(categoryId: categoryId,
				                   tags: tagString.trimmingCharacters(in: .whitespacesAndNewlines).split(separator: ",").map { String($0) },
				                   thumb: image,
				                   inviteMember: accessLevel == .private ? invitedFriendObjectIds : [],
				                   success: { (roomId, json) in
									if let viewController = self.storyboard?.instantiateViewController(withIdentifier: String(describing: BroadcastViewController.self)) as? BroadcastViewController {
										viewController.mRoomId = roomId
										viewController.mUserId = WGUserService.current?.memberId
										viewController.mMainUserId = WGUserService.current?.memberId
										viewController.chatInfo = json
										viewController.isBJ = true
										viewController.isPublic = true
										
										self.dismiss(animated: false, completion: { [weak presenter = self.presentingViewController] in
											DispatchQueue.main.async {
												self.updateStatus(isCount: false)
												self.countLabel.text = "3"
												
												presenter?.present(viewController, animated: true)
											}
										})
									}
				}) { (_) in
					
				}
			}
		}
	}
	
	override func dismiss(_ sender: AnyObject?) {
		timer?.invalidate()
		
		super.dismiss()
	}
	
	@objc private func tagTextChanged(notification: Notification) {
		tagConfirmAction?.isEnabled = false
		
		guard let textField = notification.object as? UITextField else { return }
		guard let text = textField.text else { return }
		
		validateTags(text: text)
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if string == " " || string == "," || string == "#" {
			if let text = textField.text {
				let result = (text as NSString).replacingCharacters(in: range, with: ", #")
				
				textField.text = result
				
				return false
			}
		}
		
		return true
	}
	
	private func validateTags(text: String) {
		if let tags = tags(text: text) {
			if tags.count <= 5 {
				tagConfirmAction?.isEnabled = true
			} else {
				tagConfirmAction?.isEnabled = false
				
				toast(message: "태그는 5개까지만 입력 가능합니다.", duration: 1, delay: 0, bottomOffsetPortrait: view.bounds.height - 50)
			}
		} else {
			tagConfirmAction?.isEnabled = false
		}
	}
	
	private func tags(text: String) -> [String]? {
		return text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).split(separator: ",").map { String($0) }
	}
}
