//
//  WGLiveTableViewCell.swift
//  Weegle
//
//  Created by 이인재 on 15/08/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGLiveTableViewCell: UITableViewCell {

	@IBOutlet weak var coverImageView: UIImageView!
	@IBOutlet weak var viewerCountLabel: UILabel!
	@IBOutlet weak var ownerProfileImageView: UIImageView!
	@IBOutlet weak var ownerNameLabel: UILabel!
	@IBOutlet weak var categoryLabel: UILabel!
	@IBOutlet weak var tagLabel: UILabel!
	@IBOutlet weak var tagSeperator: UILabel!
	@IBOutlet weak var secretView: UIView!
}
