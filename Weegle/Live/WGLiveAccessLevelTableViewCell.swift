//
//  WGLiveAccessLevelTableViewCell.swift
//  Weegle
//
//  Created by 이인재 on 15/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit

class WGLiveAccessLevelTableViewCell: UITableViewCell {

	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var profileImageView: UIImageView!
	
	weak var delegate: ListItemInteractionProtocol?
	
	@IBAction func delete() {
		delegate?.listItem(cell: self, didTouched: .delete)
	}
}
