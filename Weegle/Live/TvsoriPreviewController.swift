//
//  TvsoriPreviewController.swift
//  TvsoriSdkSample
//
//  Created by Tvsori Internet on 2/9/17.
//  Copyright © 2017 Tvsori Internet. All rights reserved.
//

import Foundation

import GLKit
import UIKit
import AVFoundation
import TvsLib

class TvsoriPreviewController : GLKViewController, ILayerInfo {
    
    static var sPreviewController : TvsoriPreviewController?
	static var sTvsClient : ITvsClient?
	
	private let defaultImage = #imageLiteral(resourceName: "img_loading")
	
    public static func handleDidBecomeActive() {
        sPreviewController?.onResume()
    }
	
    public static func handleDidEnterBackground() {
        sPreviewController?.onPause()
    }
	
    public static func initTvsoriSDK(_ stream_ip: String, _ stream_port: Int, _ sendBitrateVideo: Int, _ sendBitrateAudio: Int) {
        setLocalShareInt("send_bitrate_video", sendBitrateVideo)
        setLocalShareInt("send_bitrate_audio", sendBitrateAudio)
        setLocalShareString("stream_ip", stream_ip)
        setLocalShareInt("stream_port", stream_port)
        if(sTvsClient != nil) {
            sTvsClient = nil
        }
    }
    
    public static func destroyTvsoriSDK() {
        if(sTvsClient != nil) {
            sTvsClient = nil
        }
    }
    
    public static func getTvsClient() -> ITvsClient? {
        if(sTvsClient == nil) {
            sTvsClient = TvsFactory.createEngine("", 0, getLocalShareString("stream_ip"), getLocalShareInt("stream_port"), "")
        }
        return sTvsClient
    }
    
    /*********************************************
     Public Method
     *********************************************/
    public func setUserPlayType(_ isPlayCamera: Bool, _ isPlayAudio: Bool, _ isFrontCamera: Bool) {
        TvsoriPreviewController.setLocalShareBool("is_camera", isPlayCamera)
        TvsoriPreviewController.setLocalShareBool("is_audio", isPlayAudio)
        TvsoriPreviewController.setLocalShareBool("is_front_camera", isFrontCamera)
        onChangeLocalType()
    }
    
    public func getUserPlayType() -> [Bool] {
        var playtype = [Bool]()
        playtype.append(TvsoriPreviewController.getLocalShareBool("is_camera", true))
        playtype.append(TvsoriPreviewController.getLocalShareBool("is_audio", true))
        playtype.append(TvsoriPreviewController.getLocalShareBool("is_front_camera", true))
        return playtype
    }
    
    public func setCameraSize(_ isFrontCamera: Bool, _ width: Int, _ height: Int) {
        TvsoriPreviewController.setLocalShareInt(isFrontCamera ? "front_camera_width" : "back_camera_width", width)
        TvsoriPreviewController.setLocalShareInt(isFrontCamera ? "front_camera_height" : "back_camera_height", height)
        
    }
    
    public func getCameraSize(_ isFrontCamera: Bool) -> Size {
        let width = TvsoriPreviewController.getLocalShareInt(isFrontCamera ? "front_camera_width" : "back_camera_width")
        let height = TvsoriPreviewController.getLocalShareInt(isFrontCamera ? "front_camera_height" : "back_camera_height")
        return Size(width, height)
    }
    
    public func getAvailableCameraSizes(_ isFrontCamera: Bool) -> [Size] {
        if let client = TvsoriPreviewController.getTvsClient() {
            var cam = client.getCam(isFrontCamera ? CamOrientation.eFrontOrientation : CamOrientation.eBackOrientation)
            if(cam == nil) {
                cam = mTvsClient?.getCam(CamOrientation.eAnyOrientation)
            }
            if(cam == nil) {
                return [Size]()
            }
            if let size_list = cam?.getAvailablePreviewSizes() {
                var real_list = [Size]()
                for size in size_list {
                    if(size.width < 1000 && size.height < 1000) {
                        real_list.append(size)
                    }
                }
                return real_list
            }
        }
        return [Size]()
    }
    
    public func doChangeRoomMainUser(_ mainUserId: String) {
        mMainUserId = mainUserId
        setMainUserLayout()
    }
    
    public func doChangeRoomEmitorUser(_ emitorUserId: String) {
        mEmitorUserId = emitorUserId
        setEmitorUserLayout()
    }
    
    public func doExchangeRoomMainEmitorUser() {
        let emitor = mEmitorUserId != nil ? String.init(mEmitorUserId!) : nil
        mEmitorUserId = mMainUserId
        mMainUserId = emitor
        setMainUserLayout()
        setEmitorUserLayout()
    }
    
    var mTvsClient : ITvsClient?
    
    var mRoomId : Int64?
    var mUserId : String?
    var mMainUserId : String?
    var mEmitorUserId : String?
    
    var mCam : ICam?
    
    var mLayer : [Int:CGSize] = [0:CGSize(width:320,height:240),1:CGSize(width:320,height:240)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, with: AVAudioSessionCategoryOptions.defaultToSpeaker)
        try! AVAudioSession.sharedInstance().setActive(true)
        
        mTvsClient = TvsoriPreviewController.getTvsClient()
        mTvsClient?.setLayerNotificationListener(self)
        mTvsClient?.setSpeakerVolume(1.0)
        mTvsClient?.setMicVolume(0.5)
        mTvsClient?.noreSetHearMike(false)
        mTvsClient?.setEchoVolume(0)
        mTvsClient?.setPitchTempo(0, tempo: 1)
        mTvsClient?.setSendBitrate(videoBitrate: getSendBitrateVideo(), videoMode: 0, audioBitrate: getSendBitrateAudio(), audioMode: 0)
        mTvsClient?.setGlSurface(self.view as? GLKView)
        mTvsClient?.setGlBackgroundColor(convertHexToColor(0x32363F))
        mTvsClient?.setRemoteLayer(0, x: 0, y: 0, z: 0, width: 0, height: 0, rotation: 0)
        mTvsClient?.setRemoteLayer(1, x: 0, y: 0, z: 0, width: 0, height: 0, rotation: 0)
        mTvsClient?.setRemoteLayer(2, x: 0, y: 0, z: 0, width: 0, height: 0, rotation: 0)
        mTvsClient?.setRemoteLayer(3, x: 0, y: 0, z: 0, width: 0, height: 0, rotation: 0)
        mTvsClient?.setLocalLayer(x: 0, y: 0, z: 0, width: 0, height: 0, rotation: 0)
		
		if let mainUserId = mMainUserId {
			doChangeRoomMainUser(mainUserId)
		}
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        TvsoriPreviewController.sPreviewController = self
        onResume()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        onPause()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        TvsoriPreviewController.sPreviewController = nil
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        stopCamera()
        if(mMainUserId == mUserId) {
            setDjLocalLayer()
            if(isPlayCamera()) {
                startCamera(true)
            }
        } else if(!isEmpty(mMainUserId)) {
            setDjRemoteLayer(mLayer[0]!)
        }
        if(mEmitorUserId == mUserId) {
            setEmitorLocalLayer()
            if(isPlayCamera()) {
                startCamera(false)
            }
            if(isPlayAudio()) {
                mTvsClient?.startAudioCapture()
            } else {
                mTvsClient?.stopAudioCapture()
            }
        } else if(!isEmpty(mEmitorUserId)) {
            setEmitorRemoteLayer(mLayer[1]!)
        }
        mTvsClient?.setGlDraw(true, isVod: false)
    }
    
    // MARK: - ILayerInfo
    func onLayerContentSizeChanged(_ layer: Int, width: Int, height: Int) {
        if(layer == 0) {
            mLayer[layer] = CGSize(width: width, height: height)
            if(!isEmpty(mMainUserId) && mUserId != mMainUserId) {
                setDjRemoteLayer(mLayer[0]!)
            }
        } else {
            if(!isEmpty(mEmitorUserId) && mUserId != mEmitorUserId) {
                setEmitorRemoteLayer(mLayer[1]!)
            }
        }
    }
    
    func onResume() {
        if let userid = mUserId {
            mTvsClient?.setGlSurface(self.view as? GLKView)
            mTvsClient?.loginSS(userId: userid)
            mTvsClient?.setSendBitrate(videoBitrate: getSendBitrateVideo(), videoMode: 0, audioBitrate: getSendBitrateAudio(), audioMode: 0)
            mTvsClient?.joinchannelSS(roomId: UInt64(mRoomId!),
                                      roomMask: StreamMask.eDJAudio.rawValue | StreamMask.eEmitorAudio.rawValue | StreamMask.eDJVideo.rawValue |
										StreamMask.eEmitorVideo.rawValue | StreamMask.eLoStream.rawValue | StreamMask.eHiStream.rawValue,
                                      userIds: nil, userMasks: nil)
            setMainUserLayout()
            setEmitorUserLayout()
        }
    }
    
    func onPause() {
        stopCamera()
        mTvsClient?.stopAudioCapture()
        mTvsClient?.clearChannels()
        mTvsClient?.logoutSS()
        mTvsClient?.setGlDraw(false, isVod: false)
        
    }
    
    func setMainUserLayout() {
        mTvsClient?.clearChannelPos(layer: 0)
        mTvsClient?.stopAudioCapture()
        stopCamera()
        
        if(isEmpty(mMainUserId)) {
            mTvsClient?.setLayerImage(0, image: defaultImage)
            setDjRemoteLayer(CGSize(width:640, height:480))
        } else if(mMainUserId == mUserId) {
            self.mTvsClient?.setSendMask(videoMask: [StreamMask.eDJVideo, StreamMask.eLoStream, StreamMask.eSendClient], audioMask: [StreamMask.eDJAudio, StreamMask.eLoStream, StreamMask.eSendClient])
            self.mTvsClient?.setLayerImage(CamLayerPos, image: defaultImage)
            setDjLocalLayer()
            self.mTvsClient?.setStretchMode(CamLayerPos, mode: StretchMode.eStretchMax)
            var mode : SendMode = []
            if(isPlayCamera()) {
                self.startCamera(true)
                mode.insert(SendMode.eSendVideo)
            }
            if(isPlayAudio()) {
                self.mTvsClient?.startAudioCapture()
                mode.insert(SendMode.eSendAudio)
            }
            self.mTvsClient?.doSend(sendMode: mode)
        } else {
            self.mTvsClient?.setLayerImage(0, image: defaultImage)
            self.mTvsClient?.assignChannelMask(userId: self.hash_uid(mMainUserId!), userMask: StreamMask.eHiStream.rawValue | StreamMask.eLoStream.rawValue, layer: 0)
            self.mTvsClient?.setLocalLayer(x: 0, y: 0, z: 0, width: 0, height: 0, rotation: 0)
            setDjRemoteLayer(mLayer[0]!)
        }
    }
    
    func setEmitorUserLayout() {
        mTvsClient?.clearChannelPos(layer: 1)
        if(mUserId != mMainUserId) {
            mTvsClient?.stopAudioCapture()
            stopCamera()
        }
        
        if(isEmpty(mEmitorUserId)) {
            if(mUserId != mMainUserId) {
                mTvsClient?.setLocalLayer(x: 0, y: 0, z: 0, width: 0, height: 0, rotation: 0)
            }
            mTvsClient?.setRemoteLayer(1, x: 0, y: 0, z: 0, width: 0, height: 0, rotation: 0)
        } else if(mEmitorUserId == mUserId) {
            self.mTvsClient?.setSendMask(videoMask: [StreamMask.eDJVideo, StreamMask.eLoStream, StreamMask.eSendClient], audioMask: [StreamMask.eDJAudio, StreamMask.eLoStream, StreamMask.eSendClient])
            mTvsClient?.setLayerImage(CamLayerPos, image: defaultImage)
            setEmitorLocalLayer()
            self.mTvsClient?.setStretchMode(CamLayerPos, mode: StretchMode.eStretchMax)
            var mode : SendMode = []
            if(isPlayCamera()) {
                self.startCamera(false)
                mode.insert(SendMode.eSendVideo)
            }
            if(isPlayAudio()) {
                self.mTvsClient?.startAudioCapture()
                mode.insert(SendMode.eSendAudio)
            }
            self.mTvsClient?.doSend(sendMode: mode)
        } else {
            self.mTvsClient?.setLayerImage(1, image: defaultImage)
            self.mTvsClient?.assignChannelMask(userId: self.hash_uid(mEmitorUserId!), userMask: StreamMask.eHiStream.rawValue | StreamMask.eLoStream.rawValue, layer: 1)
            if(self.mUserId != self.mMainUserId) {
                self.mTvsClient?.setLocalLayer(x: 0, y: 0, z: 0, width: 0, height: 0, rotation: 0)
            }
            setEmitorRemoteLayer(mLayer[1]!)
        }
        self.mTvsClient?.setGlDraw(true, isVod: false)
    }
    
    func onChangeLocalType() {
        var mode : SendMode = []
        stopCamera()
        if(mUserId == mMainUserId) {
            if(isPlayCamera()) {
                mode.insert(SendMode.eSendVideo)
                startCamera(true)
            }
            setDjLocalLayer()
        } else if(mUserId == mEmitorUserId) {
            if(isPlayCamera()) {
                mode.insert(SendMode.eSendVideo)
                startCamera(false)
            }
            setEmitorLocalLayer()
        }
        if(mUserId == mMainUserId || mUserId == mEmitorUserId) {
            if(isPlayAudio()) {
                mode.insert(SendMode.eSendAudio)
                mTvsClient?.startAudioCapture()
            } else {
                mTvsClient?.stopAudioCapture()
            }
        } else {
            mTvsClient?.stopAudioCapture()
        }
        mTvsClient?.doSend(sendMode: mode)
    }
    
    func setDjLocalLayer() {
        if(isPlayCamera()) {
            mTvsClient?.setLocalLayer(x: -1, y: -1, z: 0, width: 2, height: 2, rotation: 0)
        } else {
            let sw = Float(UIScreen.main.bounds.width)
            let sh = Float(UIScreen.main.bounds.height)
            let w : Float = Float(getCameraSize(isFrontCamera()).width)
            let h : Float = Float(getCameraSize(isFrontCamera()).height)
            if(isLandscapeMode()) {
                let width = sh * w / h / sw * 2
                mTvsClient?.setLocalLayer(x: -1 + ((2-width)/2), y: -1, z: 0, width: width, height: 2, rotation: 0)
            } else {
                let height = sw * h / w / sh * 2
                mTvsClient?.setLocalLayer(x: -1, y: 1 - height, z: 0, width: 2, height: height, rotation: 0)
            }
        }
        mTvsClient?.setRemoteLayer(0, x: 0, y: 1, z: 0, width: 0, height: 0, rotation: 0)
    }
    
    func setEmitorLocalLayer() {
        let sw = Float(UIScreen.main.bounds.width)
        let sh = Float(UIScreen.main.bounds.height)
        if(isLandscapeMode()) {
            let w : Float = 320
            let h : Float = 240
            let height = 0.4 * sw * h / w / sh
            mTvsClient?.setLocalLayer(x: 0.6, y: 1-height, z: -0.1, width: 0.4, height: height, rotation: 0)
        } else {
            let w : Float = 240
            let h : Float = 320
            let height = 0.6 * sw * h / w / sh
            mTvsClient?.setLocalLayer(x: 0.4, y: 1-height, z: -0.1, width: 0.6, height: height, rotation: 0)
        }
    }
    
    func setDjRemoteLayer(_ size : CGSize) {
        let sw = Float(UIScreen.main.bounds.width)
        let sh = Float(UIScreen.main.bounds.height)
        let w : Float = Float(size.width)
        let h : Float = Float(size.height)
        if(isLandscapeMode()) {
            let width = sh * w / h / sw * 2
            mTvsClient?.setRemoteLayer(0, x: -1 + ((2-width)/2), y: -1, z: 0, width: width, height: 2, rotation: 0)
        } else {
            let height = sw * h / w / sh * 2
            mTvsClient?.setRemoteLayer(0, x: -1, y: 1 - height, z: 0, width: 2, height: height, rotation: 0)
        }
    }
    
    func setEmitorRemoteLayer(_ size : CGSize) {
        let sw = Float(UIScreen.main.bounds.width)
        let sh = Float(UIScreen.main.bounds.height)
        if(isLandscapeMode()) {
            let w : Float = Float(size.width)
            let h : Float = Float(size.height)
            let height = 0.4 * sw * h / w / sh
            mTvsClient?.setRemoteLayer(1, x: 0.6, y: 1-height, z: -0.1, width: 0.4, height: height, rotation: 0)
        } else {
            let w : Float = Float(size.width)
            let h : Float = Float(size.height)
            let height = 0.6 * sw * h / w / sh
            mTvsClient?.setRemoteLayer(1, x: 0.4, y: 1-height, z: -0.1, width: 0.6, height: height, rotation: 0)
        }
    }
    
	func hash_uid(_ uid : String) -> UInt64 {
		var h : UInt64 = 0
		if let list = uid.cString(using: String.Encoding.utf8) {
			for i in 0 ..< list.count - 1 {
				h = (19 &* h) &+ UInt64(list[i])
			}
		}
		
		return h
	}
    
    func startCamera(_ isDj : Bool) {
        stopCamera()
        
        mCam = mTvsClient?.getCam(isFrontCamera() ? CamOrientation.eFrontOrientation : CamOrientation.eBackOrientation)
        if(mCam == nil) {
            mCam = mTvsClient?.getCam(CamOrientation.eAnyOrientation)
        }
        if(mCam == nil) {
            return
        }
        mCam?.setOrientation(getOrientation())
        mCam?.setPreviewSize(isDj ? getCameraSize(isFrontCamera()) : Size(320, 240))
        mCam?.start()
    }
    
    func stopCamera() {
        mCam?.stop()
        mCam = nil
    }
    
    func convertHexToColor(_ color: UInt64) -> UIColor {
        return UIColor(red:CGFloat((color & 0xFF0000) >> 16) / 255, green: CGFloat((color & 0x00FF00) >> 8) / 255, blue:CGFloat((color & 0x0000FF)) / 255, alpha:1.0)
    }
    
    func isEmpty(_ value : String?) -> Bool {
        return value == nil || value?.isEmpty == true
    }
    
    func getOrientation() -> AVCaptureVideoOrientation {
        var orientation: AVCaptureVideoOrientation = AVCaptureVideoOrientation.portrait
        
        switch UIApplication.shared.statusBarOrientation {
        case .landscapeLeft:
            orientation = .landscapeLeft
        case .landscapeRight:
            orientation = .landscapeRight
        case .portrait:
            orientation = .portrait
        case .portraitUpsideDown:
            orientation = .portraitUpsideDown
        default:
            orientation = .portrait
        }
        return orientation
    }
    
    func isLandscapeMode() -> Bool {
        switch UIApplication.shared.statusBarOrientation {
        case .landscapeLeft:
            return true
        case .landscapeRight:
            return true
        case .portrait:
            return false
        case .portraitUpsideDown:
            return false
        default:
            return false
        }
    }
	
    func isPlayCamera() -> Bool {
        return TvsoriPreviewController.getLocalShareBool("is_camera", true)
    }
    
    func isPlayAudio() -> Bool {
        return TvsoriPreviewController.getLocalShareBool("is_audio", true)
    }
    
    func isFrontCamera() -> Bool {
        return TvsoriPreviewController.getLocalShareBool("is_front_camera", true)
    }
    
    func getSendBitrateVideo() -> Int {
        return TvsoriPreviewController.getLocalShareInt("send_bitrate_video")
    }
    
    func getSendBitrateAudio() -> Int {
        return TvsoriPreviewController.getLocalShareInt("send_bitrate_audio")
    }
    
    /***********************************
     Local Share
     ***********************************/
    static func getLocalShareString(_ key : String) -> String {
        if let value = UserDefaults.standard.string(forKey: "tvsori_" + key) {
            return value
        } else {
            return ""
        }
    }
    
    static func setLocalShareString(_ key: String, _ value: String) {
        UserDefaults.standard.set(value, forKey: "tvsori_" + key)
    }
    
    static func getLocalShareLong(_ key : String) -> Int64 {
        if let value = UserDefaults.standard.object(forKey: "tvsori_" + key) as? NSNumber {
            return value.int64Value
        }
        return 0
    }
    
    static func setLocalShareLong(_ key: String, _ value: Int64) {
        UserDefaults.standard.set(NSNumber(value: value as Int64), forKey: "tvsori_" + key)
    }
    
    static func getLocalShareInt(_ key : String) -> Int {
        if let value = UserDefaults.standard.object(forKey: "tvsori_" + key) as? NSNumber {
            return value.intValue
        }
        return 0
    }
    
    static func setLocalShareInt(_ key: String, _ value: Int) {
        UserDefaults.standard.set(NSNumber(value: value as Int), forKey: "tvsori_" + key)
    }
    
    static func getLocalShareBool(_ key: String, _ defaultValue: Bool) -> Bool {
        if let value = UserDefaults.standard.object(forKey: key) as? Bool {
            return value
        } else {
            return defaultValue
        }
    }
    
    static func setLocalShareBool(_ key: String, _ value: Bool) {
        UserDefaults.standard.set(value, forKey: key)
    }
}
