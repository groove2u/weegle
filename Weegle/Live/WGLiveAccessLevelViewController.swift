//
//  WGLiveAccessLevelViewController.swift
//  Weegle
//
//  Created by 이인재 on 15/10/2017.
//  Copyright © 2017 OJ World. All rights reserved.
//

import UIKit
import CleanroomLogger
import RealmSwift

class WGLiveAccessLevelViewController: WGBaseViewController {

	@IBOutlet weak private var accessLevelPublicButton: UIButton!
	@IBOutlet weak private var accessLevelPrivateButton: UIButton!
	@IBOutlet weak private var bottomContraint: NSLayoutConstraint!
	@IBOutlet weak private var invitedFriendCountLabel: UILabel!
	@IBOutlet weak private var tableView: UITableView!
	
	var accessLevel: LiveAccessLevel = .public
	var invitedFriendObjectIds = [String]()
	
	override func prepareUI() {
		super.prepareUI()
		
		accessLevelSelected(accessLevel == .public ? accessLevelPublicButton : accessLevelPrivateButton)
		tableView.tableFooterView = UIView()
		
		invitedFriendCountLabel.text = "(\(invitedFriendObjectIds.count)명)"
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "SelectFriendSegue",
			let viewController = segue.destination as? WGChatInviteViewController {
			viewController.selectedFriendIds = invitedFriendObjectIds
		}
	}
	
	// MARK: - Action
	
	@IBAction private func accessLevelSelected(_ sender: UIButton) {
		switch sender {
		case accessLevelPublicButton:
			accessLevelPublicButton.isSelected = true
			accessLevelPrivateButton.isSelected = false
		default:
			accessLevelPublicButton.isSelected = false
			accessLevelPrivateButton.isSelected = true
		}
		
		accessLevel = accessLevelPublicButton.isSelected ? .public : .private
		
		UIView.animate(withDuration: 0.3,
		               delay: 0,
		               options: [.beginFromCurrentState, .curveEaseInOut],
		               animations: {
						self.bottomContraint.constant = self.accessLevel == .public ? 0 : 236
						self.view.layoutIfNeeded()
		})
	}
	
	// MARK: - Segue
	
	@IBAction func friendSelected(segue: UIStoryboardSegue) {
		Log.info?.value(invitedFriendObjectIds)
		
		invitedFriendCountLabel.text = "(\(invitedFriendObjectIds.count)명)"
		tableView.reloadData()
	}
	
	@IBAction func confirm() {
		guard let viewController = presentingViewController as? WGLiveSetupViewController else { return }
		
		viewController.invitedFriendObjectIds = invitedFriendObjectIds
		viewController.accessLevel = accessLevel
		
		dismissWithoutAnimation()
	}
}

// MARK: - TableView

extension WGLiveAccessLevelViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return invitedFriendObjectIds.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let model = WGUser.user(objectId: invitedFriendObjectIds[indexPath.item]) else { return UITableViewCell() }
		guard let cell = tableView.dequeueReusableCell(withIdentifier: WGLiveAccessLevelTableViewCell.defaultReuseIdentifier, for: indexPath) as? WGLiveAccessLevelTableViewCell else {
				return UITableViewCell()
		}
		
		cell.delegate = self
		cell.nameLabel.text = model.memberName
		cell.profileImageView.kf.setImage(with: WGNetworkService.resourceURL(uri: model.profileImage), placeholder: #imageLiteral(resourceName: "commonProfileImage"))
		
		return cell
	}
}

// MARK: -

extension WGLiveAccessLevelViewController: ListItemInteractionProtocol {
	
	func listItem(cell: UITableViewCell, didTouched event: ListItemInteractionEvent) {
		guard let indexPath = tableView.indexPath(for: cell) else { return }
		
		tableView.beginUpdates()
		invitedFriendObjectIds.remove(at: indexPath.item)
		tableView.deleteRows(at: [indexPath], with: .none)
		tableView.endUpdates()
	}
}
