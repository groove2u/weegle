//
//  BroadcastViewController.swift
//  TvsoriSdkSample
//
//  Created by Tvsori Internet on 2/9/17.
//  Copyright © 2017 Tvsori Internet. All rights reserved.
//

import UIKit
import RealmSwift
import MobileCoreServices
import SnapKit
import SwiftyJSON
import ContactsUI
import CleanroomLogger

class BroadcastViewController : WGBaseViewController, UITableViewDelegate, UITableViewDataSource,
CNContactViewControllerDelegate, DisplayContentProtocol {

	@IBOutlet weak private var tableView: UITableView!
	@IBOutlet weak private var contentInputBottomConstraint: NSLayoutConstraint!
	@IBOutlet weak private var conversationTextView: UITextView!
	@IBOutlet weak private var conversationHeightConstraint: NSLayoutConstraint!
	@IBOutlet weak private var viewerCountLabel: UILabel!
	@IBOutlet weak private var viewerCollectionView: UICollectionView!
	@IBOutlet weak private var recordingTimeLabel: UILabel!
	@IBOutlet weak private var chatInputView: UIView!
	
    var mRoomId : Int64?
    var mUserId : String?
    var mMainUserId : String?
	var chatInfo: JSON? {
		didSet {
			chatId = chatInfo?["chat"]["id"].stringValue
		}
	}
	var isBJ = false
	var isPublic = false

	private var chatId: String!
	private var chat: WGChat?
	private var mTvsoriController : TvsoriPreviewController?
	private var isFrontCamera = true
	private var isCameraOn = true
	private var isAudioOn = true
	private var notificationToken: NotificationToken?
	
	override func prepareUI() {
		super.prepareUI()
		
		NotificationCenter.default.addObserver(self, selector: #selector(appWillTerminate),
		                                       name: NSNotification.Name.UIApplicationWillTerminate, object: nil)
	}
	
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? TvsoriPreviewController {
			mTvsoriController = controller
			mTvsoriController?.mRoomId = self.mRoomId
			mTvsoriController?.mUserId = self.mUserId
			mTvsoriController?.mMainUserId = self.mMainUserId
			
			// TEST CODE START (CAMERA ON, MIC ON, CAMERA FRONT)
			mTvsoriController?.setUserPlayType(true, true, true)
			// TEST CODE END
			
			conversationTextView.textContainerInset = UIEdgeInsets(top: 8, left: 5, bottom: 8, right: 5)
			
			tableView.rowHeight = UITableViewAutomaticDimension
			tableView.estimatedRowHeight = 32
			tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
			
			let style = NSMutableParagraphStyle()
			conversationTextView.attributedText = NSAttributedString(string: "", attributes: [NSAttributedStringKey.paragraphStyle: style])
			
			chat = WGChat.chat(id: chatId)
			
			notificationToken = chat?.addNotificationBlock({ [weak self] (changes) in
				Log.info?.message("change: \(changes)")
				
				self?.updateDataUI()
			})
			
			WGLiveService.open(chatId: chatId)
        }
    }
	
	deinit {
		notificationToken?.stop()
	}

	override func updateDataUI() {
		tableView.reloadData()
		
		if let chat = chat, chat.items.isEmpty == false {
			tableView.scrollToRow(at: IndexPath(item: chat.items.count - 1, section: 0), at: .bottom, animated: false)
		}
	}
	
	// MARK: - Notification
	
	@objc private func appWillTerminate() {
		WGLiveService.close(chatId: chatId, isBJ: self.isBJ)
	}
	
	// MARK: - DisplayContentProtocol
	
	func showContent(_ image: UIImage) {
		if let viewController = storyboard?.instantiateViewController(withIdentifier: String(describing: WGImageViewController.self)) as? WGImageViewController {
			viewController.image = image
			
			navigationController?.pushViewController(viewController, animated: true)
		}
	}
	
	func showContent(_ videoURL: URL) {
		
	}
	
	// MARK: - UITableViewDelegate, dataSource
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return chat?.items.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard indexPath.item < chat?.items.count ?? 0 else { return UITableViewCell() }
		guard let chatItem = chat?.items[indexPath.item] else { return UITableViewCell() }
		
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "WGChatOtherTextTableViewCell", for: indexPath) as? WGChatTextTableViewCell else {
				return UITableViewCell()
		}
		
		cell.present(chatItem: chatItem)
		
		return cell
	}
	
	// MARK: - Action
	
	@IBAction func send() {
		guard let message = conversationTextView.text, message.isEmpty == false else { return }
		guard let currentUser = WGUserService.current else { return }
		
		conversationTextView.text = nil
		conversationHeightConstraint.constant = 36
		
		view.layoutIfNeeded()
		
		var chatInfo = [String: Any]()
		chatInfo["chatId"] = chatId
		chatInfo["message"] = message
		chatInfo["memberObjectId"] = currentUser.objectId
		chatInfo["messageTempId"] = UUID().uuidString
		chatInfo["extern"] = ""
		chatInfo["externType"] = 1
		chatInfo["lastDate"] = ""
		
		WGLiveService.send(chatId: chatId, chatInfo: chatInfo)
	}
	@IBAction func showKeyboard() {
		conversationTextView.becomeFirstResponder()
	}
	
    @IBAction func toggleAudio(sender: UIButton) {
		sender.isSelected = !sender.isSelected
		
		isAudioOn = sender.isSelected == false
		
        mTvsoriController?.setUserPlayType(isCameraOn, isAudioOn, isFrontCamera)
    }
    
    @IBAction func toggleFrontCamera(sender: UIButton) {
		isFrontCamera = !isFrontCamera
		
        mTvsoriController?.setUserPlayType(isCameraOn, isAudioOn, isFrontCamera)
    }
    
	@IBAction func showAD() {
		toast(message: "서비스 준비중입니다.", duration: 1, delay: 0, bottomOffsetPortrait: 60)
	}
	
	@IBAction func moreTouched() {
		guard let chatInfo = chatInfo else { return }
		
		let chatId: String
		
		if let id = chatInfo["chat"]["id"].string {
			chatId = id
		} else {
			chatId = chatInfo["chatId"].stringValue
		}
		
		let sheet = UIAlertController(title: nil, message: "더보기", preferredStyle: .actionSheet)
		
		sheet.addAction(UIAlertAction(title: "친구초대", style: .default, handler: { (_) in
			
		}))
		sheet.addAction(UIAlertAction(title: "채팅 안보기", style: .default, handler: { (_) in
			self.tableView.isHidden = true
		}))
		
		if isPublic {
			sheet.addAction(UIAlertAction(title: "화면정리(채팅내용 삭제)", style: .default, handler: { (_) in
				
			}))
		}
		
		sheet.addAction(UIAlertAction(title: "방송종료", style: .destructive, handler: { (_) in
			let alert = UIAlertController(title: "방송종료", message: "방송을 종료 하시겠습니까?", preferredStyle: .alert)
			
			alert.addAction(UIAlertAction(title: "예", style: .destructive, handler: { (_) in
				WGLiveService.close(chatId: chatId, isBJ: self.isBJ)
				self.dismiss(animated: false)
			}))
			alert.addAction(UIAlertAction(title: "아니오", style: .cancel))
			
			self.present(alert, animated: true)
		}))
		
		sheet.addAction(UIAlertAction(title: "cancel".localized, style: .cancel))
		
		present(sheet, animated: true)
	}
	
	// MARK: - UITextView
	
	override func textViewDidBeginEditing(_ textView: UITextView) {
		UIView.setAnimationsEnabled(true)
		
		super.textViewDidBeginEditing(textView)
	}
	
	func textViewDidChange(_ textView: UITextView) {
		if let text = textView.text {
			let height = floor(text.height(withConstrainedWidth: textView.bounds.width - (textView.textContainerInset.left + textView.textContainerInset.right), font: textView.font!))
				+ textView.textContainerInset.top + textView.textContainerInset.bottom
			
			if height <= 97 {
				conversationHeightConstraint.constant = height
				
				view.layoutIfNeeded()
				
				//				Log.info?.value(conversationHeightConstraint.constant)
				conversationTextView.setContentOffset(.zero, animated: true)
			}
		}
	}
	
	// MARK: - Keyboard
	
	override func keyboardWillShown(_ notification: Notification) {
		guard let info = (notification as NSNotification).userInfo else { return }
		guard let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
		guard let curve = (info[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber)?.intValue else { return }
		guard let duration = (info[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue else { return }
		
		chatInputView.isHidden = false
		tableView.isHidden = false
		
		UIView.animate(withDuration: duration,
		               delay: 0.0,
		               options: UIViewAnimationOptions(rawValue: UInt(curve) << 16),
		               animations: { [weak self] in
						self?.contentInputBottomConstraint.constant = keyboardFrame.size.height
						self?.view.layoutIfNeeded()
			},
		               completion: nil)
	}
	
	open override func keyboardWillBeHidden(_ notification: Notification) {
		guard let info = (notification as NSNotification).userInfo else { return }
		guard let curve = (info[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber)?.intValue else { return }
		guard let duration = (info[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue else { return }
		
		UIView.animate(withDuration: duration,
		               delay: 0.0,
		               options: UIViewAnimationOptions(rawValue: UInt(curve) << 16),
		               animations: { [weak self] in
						self?.contentInputBottomConstraint.constant = 0
						self?.view.layoutIfNeeded()
			},
		               completion: { (_) in
						self.chatInputView.isHidden = true
		})
	}
}

// MARK: - UICollectionView

extension BroadcastViewController: UICollectionViewDelegate, UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 0
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		return UICollectionViewCell()
	}
}
