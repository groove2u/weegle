//
//  BroadcastViewController.swift
//  TvsoriSdkSample
//
//  Created by Tvsori Internet on 2/9/17.
//  Copyright © 2017 Tvsori Internet. All rights reserved.
//

import Foundation
import UIKit

class BroadcastViewController : UIViewController {
    let TEST_USER_LIST : [String] = ["없음", "userA", "userB", "userC", "userD"]
    
    @IBOutlet weak var switchCamera: UISwitch!
    @IBOutlet weak var switchAudio: UISwitch!
    @IBOutlet weak var switchCameraFront: UISwitch!
    
    var mRoomId : Int64?
    var mUserId : String?
    var mMainUserId : String?
    var mEmitorUsrId : String?
    var mTvsoriController : TvsoriPreviewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? TvsoriPreviewController {
            mMainUserId = mUserId
            
            mTvsoriController = controller
            mTvsoriController?.mRoomId = self.mRoomId
            mTvsoriController?.mUserId = self.mUserId
            mTvsoriController?.mMainUserId = self.mMainUserId
            mTvsoriController?.mEmitorUserId = self.mEmitorUsrId
            
            // TEST CODE START (CAMERA ON, MIC ON, CAMERA FRONT)
            mTvsoriController?.setUserPlayType(true, true, true)
            // TEST CODE END
        }
    }

    @IBAction func onValueChangeCamera(_ sender: UISwitch) {
        mTvsoriController?.setUserPlayType(switchCamera.isOn, switchAudio.isOn, switchCameraFront.isOn)
    }
    
    @IBAction func onValueChangeAudio(_ sender: UISwitch) {
        mTvsoriController?.setUserPlayType(switchCamera.isOn, switchAudio.isOn, switchCameraFront.isOn)
    }
    
    @IBAction func onValueChangeCameraFront(_ sender: UISwitch) {
        mTvsoriController?.setUserPlayType(switchCamera.isOn, switchAudio.isOn, switchCameraFront.isOn)
    }
    
    @IBAction func onClickSelectCameraSize(_ sender: Any) {
        if let cam_size = mTvsoriController?.getAvailableCameraSizes(switchCameraFront.isOn) {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            for i in 0..<cam_size.count {
                let title = String.init(format: "%d X %d", cam_size[i].width, cam_size[i].height)
                alert.addAction(UIAlertAction(title:title, style:UIAlertActionStyle.default) { (action) -> Void in
                    self.mTvsoriController?.setCameraSize(self.switchCameraFront.isOn, cam_size[i].width, cam_size[i].height)
                })
            }
            alert.popoverPresentationController?.sourceView = sender as! UIButton
            alert.popoverPresentationController?.sourceRect = (sender as! UIButton).bounds
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func onClickSelectMainUser(_ sender: Any) {
        let alert = UIAlertController(title: "SELECT MAIN USER", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        for i in 0..<TEST_USER_LIST.count {
            alert.addAction(UIAlertAction(title:TEST_USER_LIST[i], style:UIAlertActionStyle.default) { (action) -> Void in
                let user = i == 0 ? "" : self.TEST_USER_LIST[i]
                self.mTvsoriController?.doChangeRoomMainUser(user)
            })
        }
        alert.popoverPresentationController?.sourceView = sender as! UIButton
        alert.popoverPresentationController?.sourceRect = (sender as! UIButton).bounds
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onClickSelectEmitorUser(_ sender: Any) {
        let alert = UIAlertController(title: "SELECT EMITOR USER", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        for i in 0..<TEST_USER_LIST.count {
            alert.addAction(UIAlertAction(title:TEST_USER_LIST[i], style:UIAlertActionStyle.default) { (action) -> Void in
                let user = i == 0 ? "" : self.TEST_USER_LIST[i]
                self.mTvsoriController?.doChangeRoomEmitorUser(user)
            })
        }
        alert.popoverPresentationController?.sourceView = sender as! UIButton
        alert.popoverPresentationController?.sourceRect = (sender as! UIButton).bounds
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onClickExchangeMainEmitor(_ sender: Any) {
        self.mTvsoriController?.doExchangeRoomMainEmitorUser()
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
