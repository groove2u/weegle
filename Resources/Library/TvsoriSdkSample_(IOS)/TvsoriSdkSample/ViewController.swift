//
//  ViewController.swift
//  TvsoriSdkSample
//
//  Created by Tvsori Internet on 2/9/17.
//  Copyright © 2017 Tvsori Internet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickUserA(_ sender: Any) {
        let uvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BroadcastViewController") as! BroadcastViewController
        uvc.mRoomId = 20000
        uvc.mUserId = "userA"
        self.present(uvc, animated: true, completion: nil)
    }
    
    @IBAction func onClickUserB(_ sender: Any) {
        let uvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BroadcastViewController") as! BroadcastViewController
        uvc.mRoomId = 20000
        uvc.mUserId = "userB"
		uvc.mMainUserId = "userA"
        self.present(uvc, animated: true, completion: nil)
    }
    
    @IBAction func onClickUserC(_ sender: Any) {
        let uvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BroadcastViewController") as! BroadcastViewController
        uvc.mRoomId = 20000
        uvc.mUserId = "userC"
        self.present(uvc, animated: true, completion: nil)
    }
    
    @IBAction func onClickUserD(_ sender: Any) {
        let uvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BroadcastViewController") as! BroadcastViewController
        uvc.mRoomId = 20000
        uvc.mUserId = "userD"
        self.present(uvc, animated: true, completion: nil)
    }
}

